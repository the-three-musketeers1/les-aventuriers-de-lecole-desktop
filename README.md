# Les aventuriers de l'école desktop app
Desktop application specially for teachers<br />
This application work on java 11 and apache maven, those tools have to be installed.<br />
Install Java 11 : [https://www.oracle.com/java/technologies/javase-jdk11-downloads.html](https://www.oracle.com/java/technologies/javase-jdk11-downloads.html)<br />
Install Apache Maven : [https://maven.apache.org/install.html](https://maven.apache.org/install.html)<br />

#### Clone the project
    git clone https://gitlab.com/the-three-musketeers1/les-aventuriers-de-lecole-desktop.git

#### Launch command line application development version
	cd desktopapp

	mvn clean package

	java -jar cli/target/desktopapp.cli-1.0-SNAPSHOT-shaded.jar
#### Launch user interface application development version
	cd desktopapp

	mvn clean package

	java -jar ui/target/desktopapp.ui-1.0-SNAPSHOT-shaded.jar