package fr.sa.desktopapp.ui.utils;

import org.junit.Before;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDate;
import java.util.ArrayList;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

class ValidatorUtilsTest {

    @Mock
    private DialogUtils dialogUtils;

    ValidatorUtils sut;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @BeforeEach
    void setUp() {
        dialogUtils = mock(DialogUtils.class);
        sut = new ValidatorUtils(dialogUtils);
    }

    @Test
    public void isGreterThan_shouldReturnTrue_whenNumberGreaterThanBoundary() {
        assertTrue(sut.isGreatherThan("4", 1));
    }

    @Test
    public void isGreterThan_shouldReturnFalse_whenNumberLowerThanBoundary() {
        assertFalse(sut.isGreatherThan("4", 5));
    }

    @Test
    public void isGreterThan_shouldReturnFalse_whenNumberIsNotNumberFormat() {
        assertFalse(sut.isGreatherThan("puissance 4", 5));
    }

    @Test
    public void isGreterThan_shouldReturnFalse_whenNumberIsFloat() {
        assertFalse(sut.isGreatherThan("4.4", 5));
    }

    @Test
    public void isGreterThan_shouldReturnFalse_whenNumberParamIsNull() {
        assertFalse(sut.isGreatherThan(null, 5));
    }

    @Test
    public void isInteger_shouldReturnTrue_whenToParseParamIsInteger() {
        assertTrue(sut.isInteger("5"));
    }

    @Test
    public void isInteger_shouldReturnFalse_whenToParseParamIsNull() {
        assertFalse(sut.isInteger(null));
    }

    @Test
    public void isInteger_shouldReturnFalse_whenToParseParamIsNotNumber() {
        assertFalse(sut.isInteger("not number"));
    }

    @Test
    public void isInteger_shouldReturnFalse_whenToParseParamIsFloat() {
        assertFalse(sut.isInteger("5.2"));
    }

    @Test
    public void hasLengthBetween_shouldReturnTrue_whenStringLengthIsBetweenMinAndMax() {
        assertTrue(sut.hasLengthBetween("test", 3, 5));
    }

    @Test
    public void hasLengthBetween_shouldReturnTrue_whenStringLengthIsEqualToMinAndMax() {
        assertTrue(sut.hasLengthBetween("test", 4, 4));
    }

    @Test
    public void hasLengthBetween_shouldReturnFalse_whenStringLengthLowerThanMin() {
        assertFalse(sut.hasLengthBetween("test", 5, 6));
    }

    @Test
    public void hasLengthBetween_shouldReturnFalse_whenStringLengthGreaterThanMax() {
        assertFalse(sut.hasLengthBetween("test", 2, 3));
    }

    @Test
    public void showPotentialErrors_shouldReturnTrue_whenNoErrors() {
        var errors = new ArrayList<String>();

        assertEquals(errors.size(), 0);
        assertTrue(sut.showPotentialErrors("title", "description", errors));
    }

    @Test
    public void showPotentialErrors_shouldCallDialogUtilsErrorPopup() {
        var errors = new ArrayList<String>();
        errors.add("ERROR !!!");

        doNothing().when(dialogUtils).showErrorPopUp(isA(String.class), isA(String.class), isA(String.class));

        sut.showPotentialErrors("title", "description", errors);

        verify(dialogUtils, times(1)).showErrorPopUp(any(String.class), any(String.class), any(String.class));
    }

    @Test
    public void showPotentialErrors_shouldReturnFalse_whenHasAtLeastOneError() {
        var errors = new ArrayList<String>();
        errors.add("ERROR !!!");

        doNothing().when(dialogUtils).showErrorPopUp(isA(String.class), isA(String.class), isA(String.class));

        assertEquals(errors.size(), 1);
        assertFalse(sut.showPotentialErrors("title", "description", errors));
    }

    @Test
    public void isDateLowerThan_shouldReturnTrue_whenCheckedDateIsLowerThanBoundaryDate() {
        LocalDate boundaryDate = LocalDate.now();
        LocalDate checkedDate = boundaryDate.plusDays(1);


        assertTrue(sut.isLocalDateGreaterThan(checkedDate, boundaryDate));
    }

    @Test
    public void isDateLowerThan_shouldReturnFalse_whenCheckedDateIsLowerThanBoundaryDate() {
        LocalDate boundaryDate = LocalDate.now();
        LocalDate checkedDate = boundaryDate.minusYears(1);

        assertFalse(sut.isLocalDateGreaterThan(checkedDate, boundaryDate));
    }

    @Test
    public void isDateLowerThan_shouldReturnFalse_whenCheckedDateIsEqualToBoundaryDate() {
        LocalDate boundaryDate = LocalDate.now();
        LocalDate checkedDate = LocalDate.of(boundaryDate.getYear(), boundaryDate.getMonth(), boundaryDate.getDayOfMonth());

        assertFalse(sut.isLocalDateGreaterThan(checkedDate, boundaryDate));
    }
}
