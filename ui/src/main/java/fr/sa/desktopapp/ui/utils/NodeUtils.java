package fr.sa.desktopapp.ui.utils;

import javafx.beans.value.ChangeListener;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Integer.MAX_VALUE;

public class NodeUtils {

    public Button createListActionButton(String var1) {
        Button button = new Button(var1);
        button.setMaxWidth(MAX_VALUE);
        button.setPrefHeight(20);
        button.setId("action-btn");
        return button;
    }

    public Button createButtonWithEventAndStyle(String text, EventHandler<ActionEvent> event, String style) {
        Button button = new Button(text);

        if(event != null) {
            button.setOnAction(event);
        }

        if(style != null) {
            button.setStyle(style);
        }

        return button;
    }

    public ImageView createIconWithEventAndStyle(String path, EventHandler<MouseEvent> event) {
        ImageView image = this.createImageView(path, 32, 32);
        image.setStyle("-fx-cursor: hand");

        if(event != null) {
            image.setOnMouseClicked(event);
        }
        HBox.setMargin(image, new Insets(0, 5, 0, 0));
        return image;
    }

    public ImageView createImageView(String path, int width, int height) {
        if (StringUtils.isEmpty(path)) {
            return new ImageView();
        }
        ImageView image = new ImageView(new Image(getClass().getResourceAsStream(path)));
        image.setFitHeight(height);
        image.setFitWidth(width);
        return image;
    }

    public <T> boolean addDefaultItemToEmptyMenuButton(List<T> list, MenuButton menu, String defaultText) {
        if(list != null && list.size() == 0) {
            menu.getItems().add(new MenuItem(defaultText));
            return true;
        }
        return false;
    }

    public List<Pane> createPanes(int number) {
        List<Pane> panes = new ArrayList<>();
        for (int i = 0; i < number; i++) {
            Pane pane = new Pane();
            HBox.setHgrow(pane, Priority.ALWAYS);
            panes.add(pane);
        }
        return panes;
    }

    public Label createLabelWithId(String content, String id) {
        if (StringUtils.isEmpty(id)) {
            return new Label();
        }
        Label label = new Label(content);
        label.setId(id);
        return label;
    }

    public MenuItem createMenuItemWithAction(String title, EventHandler<ActionEvent> event) {
        MenuItem menuItem = new MenuItem(title);
        menuItem.setOnAction(event);
        return menuItem;
    }

    public TextField createTextFieldWithBinding(String value, ChangeListener<String> listener) {
        TextField textField = new TextField(value);
        textField.textProperty().addListener(listener);
        return textField;
    }
}
