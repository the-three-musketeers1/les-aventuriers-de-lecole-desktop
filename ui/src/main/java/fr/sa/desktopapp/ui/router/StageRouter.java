package fr.sa.desktopapp.ui.router;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.function.Consumer;

public class StageRouter {
    private final Stage stage;

    public StageRouter(Stage stage) {
        this.stage = stage;
    }

    public void goTo(final Class<?> controllerClass) {
        goTo(controllerClass, __ -> {});
    }

    public <T> void goTo(final Class<T> controllerClass, final Consumer<T> controllerConsumer) {
        final var viewName = controllerClass.getSimpleName().replace("Controller", "");
        final var view = loadView(viewName, controllerConsumer);
        final var scene = new Scene(view);
        stage.setMaximized(false);
        stage.setScene(scene);
    }

    private<T> Parent loadView(final String viewName, final Consumer<T> controllerConsumer) {
        final var viewPath = String.format("/views/%sView.fxml", viewName);

        try {
            final var fxmlLoader = new FXMLLoader(this.getClass().getResource(viewPath));
            final Parent view = fxmlLoader.load();
            controllerConsumer.accept(fxmlLoader.getController());

            return view;
        } catch (final IOException e) {
            throw new IllegalStateException(String.format("Cannot load view: %s", viewPath, e));
        }
    }
}
