package fr.sa.desktopapp.ui.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.sa.desktop.core.models.CharacterInformations;
import fr.sa.desktop.core.models.Guild;
import fr.sa.desktop.core.plugin.MemberPluginTemplate;
import fr.sa.desktop.core.plugin.models.CharacterWithStatistics;
import fr.sa.desktop.core.plugin.models.StatisticsExercise;
import fr.sa.desktop.core.services.CharacterExerciseService;
import fr.sa.desktop.core.services.GuildService;
import fr.sa.desktop.core.utils.parsers.JsonParser;
import fr.sa.desktop.core.utils.requests.SyncRequest;
import fr.sa.desktopapp.ui.utils.NodeUtils;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextAlignment;

import java.net.http.HttpClient;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MemberPluginController {

    @FXML
    Label pluginTitle;

    @FXML
    ScrollPane pluginContent;

    @FXML
    VBox memberList;

    @FXML
    Button allMemberButton;

    private CharacterExerciseService characterExerciseService;
    private GuildService guildService;

    List<CharacterInformations> members;
    Guild guild;

    MemberPluginTemplate memberPlugin;

    private NodeUtils nodeUtils;

    @FXML
    public void initialize() {
        nodeUtils = new NodeUtils();
        characterExerciseService = new CharacterExerciseService(new JsonParser(new ObjectMapper()),
                new SyncRequest(HttpClient.newHttpClient()));
        guildService = new GuildService(new JsonParser(new ObjectMapper()),
                new SyncRequest(HttpClient.newHttpClient()));


    }

    public void setPluginInstance(MemberPluginTemplate memberPluginTemplate) {
        memberPlugin = memberPluginTemplate;

        pluginTitle.setText(memberPluginTemplate.setTitle());
        allMemberButton.setText(memberPluginTemplate.setButtonName());

        allMemberButton.setOnMouseClicked(mouseEvent -> {
            List<CharacterWithStatistics> charactersStatistics = this.characterExerciseService.findStatisticsByCharactersByGuildId(guild.getId());
            this.memberPlugin.setAllMemberActionButtonAction(charactersStatistics);
        });

    }

    public void setMemberList() {

        memberList.getChildren().clear();

        this.members.forEach(member -> {
            Button button = nodeUtils.createListActionButton(member.getLast_name() + " " + member.getFirst_name());

            button.setOnMouseClicked(mouseEvent -> {
                List<StatisticsExercise> statisticsExerciseByCharacter = characterExerciseService.findStatisticsExerciseByCharacter(member.getId());
                HashMap<String, String> memberInfos = new HashMap<String, String>();
                memberInfos.put("pseudo", member.getPseudo());
                memberInfos.put("firstname", member.getFirst_name());
                memberInfos.put("lastname", member.getLast_name());
                pluginContent.setContent(memberPlugin.setPluginContent(statisticsExerciseByCharacter, memberInfos));
            });

            memberList.getChildren().add(button);
        });
    }

    public void setGuild(Guild guild) {
        this.members = this.guildService.getGuildMemberByGuildId(guild.getId());
        this.guild = guild;
        setMemberList();
    }
}
