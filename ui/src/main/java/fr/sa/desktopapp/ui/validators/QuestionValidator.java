package fr.sa.desktopapp.ui.validators;

import fr.sa.desktopapp.ui.utils.DialogUtils;
import fr.sa.desktopapp.ui.utils.ValidatorUtils;
import javafx.scene.control.DatePicker;
import javafx.scene.control.MenuButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import org.apache.commons.lang3.StringUtils;

import java.time.LocalDate;

public class QuestionValidator {
    private ValidatorUtils validatorUtils;

    public QuestionValidator(ValidatorUtils validatorUtils) {
        this.validatorUtils = validatorUtils;
    }

    public boolean isWordingValid(String title) {
        return validatorUtils.hasLengthBetween(title, 1, 60000);
    }

    public boolean isDamageValid(String life) {
        return validatorUtils.isGreatherThan(life, -1);
    }

    public boolean isTimeValid(String winLevel) {
        return validatorUtils.isGreatherThan(winLevel, 0);
    }

}
