package fr.sa.desktopapp.ui.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.sa.desktop.core.models.Message;
import fr.sa.desktop.core.models.Ticket;
import fr.sa.desktop.core.services.TicketService;
import fr.sa.desktop.core.utils.parsers.JsonParser;
import fr.sa.desktop.core.utils.requests.SyncRequest;
import fr.sa.desktopapp.ui.router.Router;
import fr.sa.desktopapp.ui.utils.DialogUtils;
import fr.sa.desktopapp.ui.utils.ValidatorUtils;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.util.Callback;

import java.net.http.HttpClient;
import java.text.DateFormat;
import java.util.*;

public class MessageController {

    private Router router;
    private Ticket ticket;
    private TicketService ticketService;
    private ValidatorUtils validatorUtils;
    private DialogUtils dialogUtils;
    private ObservableList<Message> messages = FXCollections.observableArrayList();

    @FXML
    private Label ticketTitle;
    @FXML
    private Label ticketCreatedDate;
    @FXML
    private Label ticketUpdatedDate;
    @FXML
    private Label ticketNbMessages;
    @FXML
    private Label characterName;
    @FXML
    private Label characterLevel;
    @FXML
    private Label characterActionPoints;
    @FXML
    private Label userFirstName;
    @FXML
    private Label userLastName;
    @FXML
    private Label stateStatus;
    @FXML
    private TableView<Message> messagesTable;
    @FXML
    private TableColumn<Message, String> nameSenderColumn;
    @FXML
    private TableColumn<Message, String> creationDateColumn;
    @FXML
    private TableColumn<Message, String> contentColumn;
    @FXML
    private TextField contentField;
    private List<Message> messageList;

    @FXML
    public void initialize() {
        this.ticketService = new TicketService(new JsonParser(new ObjectMapper()),
                new SyncRequest(HttpClient.newHttpClient()));
        dialogUtils = new DialogUtils();
        validatorUtils = new ValidatorUtils(dialogUtils);
    }

    public void setRouter(Router router) {
        this.router = router;
    }

    public void setTicket(Ticket concernedTicket) {
        ticket = concernedTicket;
        messageList = Arrays.asList(ticket.getMessages());
        messagesTable.setItems(messages);
        setMessagesTable();
        setTicketInformation();
    }

    private void setMessagesTable() {
        nameSenderColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Message, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Message, String> param) {
                final var rowMessage = param.getValue();
                String sender = rowMessage.getSentByCharacter()
                        ? String.format("%s %s", ticket.getCharacter().getUser().getFirstname(), ticket.getCharacter().getUser().getLastname())
                        : "Vous";
                return new SimpleStringProperty(sender);
            }
        });
        contentColumn.setCellValueFactory(new PropertyValueFactory<>("content"));
        creationDateColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Message, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Message, String> param) {
                return createDateStringProperty(new Locale("FR", "fr"), param.getValue().getSentDate());
            }
        });
        messages.addAll(messageList);
    }

    private void setTicketInformation() {
        userFirstName.setText(ticket.getCharacter().getUser().getFirstname());
        userLastName.setText(ticket.getCharacter().getUser().getLastname());

        characterName.setText(ticket.getCharacter().getName());
        characterActionPoints.setText(ticket.getCharacter().getAction_points().toString());
        characterLevel.setText(ticket.getCharacter().getLevel().toString());

        ticketTitle.setText(ticket.getTitle());
        ticketCreatedDate.setText(createDateStringFormat(new Locale("FR", "fr"), ticket.getDate()));
        ticketUpdatedDate.setText(createDateStringFormat(new Locale("FR", "fr"), ticket.getUpdatedAt()));

        if (ticket.getMessages() != null) {
            ticketNbMessages.setText(String.valueOf(ticket.getMessages().length));
        }
        stateStatus.setText(ticket.getStatus().getState());
    }

    private SimpleStringProperty createDateStringProperty(Locale locale, Date date) {
        return new SimpleStringProperty(createDateStringFormat(locale, date));
    }

    private String createDateStringFormat(Locale locale, Date date) {
        return DateFormat.getDateTimeInstance(
                DateFormat.SHORT,
                DateFormat.SHORT,
                locale)
                .format(date);
    }

    public void onReturnTicketList(MouseEvent mouseEvent) {
        router.goTo(TicketController.class, controller -> {
            controller.setRouter(router);
        });
    }

    public void onCreateMessage(MouseEvent mouseEvent) {
        if (!validatorUtils.showPotentialErrors("Création d'un message",
                "Votre message n'a pas pu être créée", checkMessageField())) {
            return;
        }

        final var message = Message.builder()
                .content(contentField.getText().trim())
                .build();
        final var newMessage = ticketService.createMessage(message, ticket);
        if (newMessage == null) {
            final var errorMessage = "Votre message n'a pas pu être créé";
            dialogUtils.showErrorPopUp("Création d'un message", errorMessage, "");
            return;
        }
        this.messages.add(newMessage);
        contentField.clear();
    }

    private List<String> checkMessageField() {
        List<String> errors = new ArrayList<>();

        if (!validatorUtils.hasLengthBetween(this.contentField.getText().trim(), 1, 255)) {
            errors.add("Le contenu doit être renseigné");
        }
        return errors;
    }
}
