package fr.sa.desktopapp.ui.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.sa.desktop.core.models.Adventure;
import fr.sa.desktop.core.models.Exercise;
import fr.sa.desktop.core.models.Subject;
import fr.sa.desktop.core.services.ExerciseService;
import fr.sa.desktop.core.services.LeaderService;
import fr.sa.desktop.core.utils.parsers.JsonParser;
import fr.sa.desktop.core.utils.requests.SyncRequest;
import fr.sa.desktopapp.ui.router.Router;
import fr.sa.desktopapp.ui.utils.DialogUtils;
import fr.sa.desktopapp.ui.utils.NodeUtils;
import fr.sa.desktopapp.ui.utils.ValidatorUtils;
import fr.sa.desktopapp.ui.validators.ExerciseValidator;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;

import java.io.IOException;
import java.net.http.HttpClient;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ExerciseController {
    //dependances
    private LeaderService leaderService;
    private ExerciseService exerciseService;
    private DialogUtils dialogUtils;
    private NodeUtils nodeUtils;
    private Router router;
    private ExerciseValidator validator;
    private ValidatorUtils validatorUtils;

    private List<Exercise> leaderExercises;
    private List<Adventure> leaderExpeditions;
    private List<Subject> leaderSubjects;

    private Subject exerciseChapterToCreate;

    private Subject subjectFilter;
    private Adventure expeditionFilter;

    @FXML
    private VBox exerciseViewVbox;

    @FXML
    private VBox tilePane;

    @FXML
    private MenuButton expeditionList;

    @FXML
    private MenuButton subjectList;

    @FXML
    private TextField titleInput;

    @FXML
    private TextField lifeInput;

    @FXML
    private TextField expInput;

    @FXML
    private TextField nbValidQuestionInput;

    @FXML
    private TextArea descriptionInput;

    @FXML
    private Button resetFiltersBtn;

    @FXML
    private MenuButton subjectCreateList;

    @FXML
    private MenuButton chapterCreateList;

    @FXML
    private Button createExerciseBtn;

    public void setRouter(Router router) {
        this.router = router;
    }

    @FXML
    public void initialize() {
        leaderService = new LeaderService(new JsonParser(new ObjectMapper()),
                new SyncRequest(HttpClient.newHttpClient()));
        exerciseService = new ExerciseService(new JsonParser(new ObjectMapper()),
                new SyncRequest(HttpClient.newHttpClient()));
        dialogUtils = new DialogUtils();
        nodeUtils = new NodeUtils();
        validatorUtils = new ValidatorUtils(dialogUtils);
        validator = new ExerciseValidator(validatorUtils);

        var currentLeaderId = leaderService.getCurrentLeaderId();
        leaderSubjects = leaderService.findLeaderSubjects(currentLeaderId);
        leaderExpeditions = leaderService.findLeaderExpeditions(currentLeaderId);
        displayExpeditionsInMenu();
        displaySubjectsInMenu();
        loadExercises();
    }

    private void displayAllExercises() {
        emptyExercises();

        for (Exercise leaderExercise : leaderExercises) {
            tilePane.getChildren().add(createExercisePane(leaderExercise));
        }
    }

    private HBox createExercisePane(Exercise exercise) {
        HBox hBox = new HBox();
        hBox.setSpacing(10);
        hBox.setPadding(new Insets(5, 10, 10, 10));

        hBox.getChildren().add(nodeUtils.createLabelWithId(exercise.getName(), "label-input-size"));

        hBox.getChildren().add(nodeUtils.createImageView("/images/book.png", 32, 32));
        hBox.getChildren().add(nodeUtils.createLabelWithId(exercise.getSubject().getTitle(), "label-input-size"));

        hBox.getChildren().add(nodeUtils.createImageView("/images/heart.png", 32, 32));
        hBox.getChildren().add(nodeUtils.createLabelWithId(exercise.getLife().toString(), "label-input-size"));

        hBox.getChildren().add(nodeUtils.createImageView("/images/add.png", 32, 32));
        hBox.getChildren().add(nodeUtils.createLabelWithId(exercise.getExperience().toString(), "label-input-size"));
        hBox.getChildren().add(nodeUtils.createPanes(1).get(0));
        hBox.getChildren().add(nodeUtils.createIconWithEventAndStyle("/images/pencil.png", mouseEvent -> setQuestionView(exercise)));
        hBox.getChildren().add(nodeUtils.createIconWithEventAndStyle("/images/trash.png", mouseEvent -> deleteExercise(exercise)));

        return hBox;
    }

    private void deleteExercise(Exercise exercise) {
        Alert confirmationDialog = dialogUtils.showConfirmationPopUp("Supprimer un exercice", "Voulez-vous vraiment supprimer cet exercice ?");
        Optional<ButtonType> result = confirmationDialog.showAndWait();
        if (result.isPresent() && result.get() == ButtonType.OK){
            if(exerciseService.deleteExercise(exercise.getId())) {
                dialogUtils.showInformationPopUp("Supprimer un exercice", "Votre exercice a bien été supprimé");
                loadExercises();
            } else {
                dialogUtils.showErrorPopUp("Supprimer un exercice", "Votre exercice n'a pas pu être supprimé", null);
            }
        }
    }

    private void displayExpeditionsInMenu() {
        if(!nodeUtils.addDefaultItemToEmptyMenuButton(leaderExpeditions, expeditionList, "Aucune expédition")) {
            for (Adventure expedition : leaderExpeditions) {
                fillExpeditionMenuItemsWithEvent(expeditionList, expedition, actionEvent -> {
                    expeditionList.setText(expedition.getTitle());
                    expeditionFilter = expedition;
                    loadExercises();
                });
            }
        }
    }

    private void fillChapterCreateMenu(Subject subject) {
        chapterCreateList.getItems().clear();
        chapterCreateList.setText("Aucun");
        exerciseChapterToCreate = null;
        for (Subject childSubject : subject.getChildSubjects()) {
            fillSubjectMenuItemsWithEvent(chapterCreateList, childSubject, actionEvent -> {
                exerciseChapterToCreate = childSubject;
                chapterCreateList.setText(childSubject.getTitle());
            });
        }
    }

    private void displaySubjectsInMenu() {
        if(!nodeUtils.addDefaultItemToEmptyMenuButton(leaderSubjects, subjectList, "Aucune matière")) {
            for (Subject subject : leaderSubjects) {
                if(subject.getParentSubject() == null && subject.getChildSubjects() != null && subject.getChildSubjects().length > 0) {
                    fillSubjectMenuItemsWithEvent(subjectCreateList, subject, actionEvent -> {
                        subjectCreateList.setText(subject.getTitle());
                        fillChapterCreateMenu(subject);
                    });

                    fillSubjectMenuItemsWithEvent(subjectList, subject, actionEvent -> {
                        subjectList.setText(subject.getTitle());
                        subjectFilter = subject;
                        loadExercises();
                    });
                }
            }
        }
    }

    private void fillSubjectMenuItemsWithEvent(MenuButton menu, Subject subject, EventHandler<ActionEvent> event) {
            MenuItem menuItem = new MenuItem(subject.getTitle());
            menuItem.setOnAction(event);
            menu.getItems().add(menuItem);
    }

    private void fillExpeditionMenuItemsWithEvent(MenuButton menu, Adventure expedition, EventHandler<ActionEvent> event) {
            MenuItem menuItem = new MenuItem(expedition.getTitle());
            menuItem.setOnAction(event);
            menu.getItems().add(menuItem);
    }

    private void emptyExercises() {
        tilePane.getChildren().clear();
    }

    private void loadExercises() {
        leaderExercises = leaderService.findLeaderExercisesWithFilters(leaderService.getCurrentLeaderId(), subjectFilter, expeditionFilter);
        displayAllExercises();
    }

    @FXML
    private void resetFilters() {
        subjectList.setText("Matière");
        expeditionList.setText("Expédition");
        subjectFilter = null;
        expeditionFilter = null;
        loadExercises();
    }

    private List<String> checkCreateForm() {
        List<String> errors = new ArrayList<>();

        if(!validator.isTitleValid(titleInput.getText())) {
            errors.add("Le titre doit être renseigné");
        }

        if(!validator.isDescriptionValid(descriptionInput.getText())) {
            errors.add("La description doit être renseignée");
        }

        if(!validator.isLifeValid(lifeInput.getText())) {
            errors.add("La vie doit être supérieure à 0");
        }

        if(!validator.isExperienceValid(expInput.getText())) {
            errors.add("L'expérience doit être renseignée");
        }

        if(exerciseChapterToCreate == null) {
            errors.add("Le chapitre doit être renseignée");
        }

        return errors;
    }

    @FXML
    private void createExercise() {

        if(!validatorUtils.showPotentialErrors("Création d'exercice",
                "Votre exercice n'a pas pu être créée", checkCreateForm())) {
            return;
        }

        Exercise exercise = Exercise.builder()
                .name(titleInput.getText())
                .description(descriptionInput.getText())
                .life(Integer.parseInt(lifeInput.getText()))
                .experience(Integer.parseInt(expInput.getText()))
                .subject(exerciseChapterToCreate)
                .build();

        if(!exerciseService.createExercise(exercise)) {
            dialogUtils.showErrorPopUp("Création d'exercice", "Votre exercice n'a pas pu être créé", "");
            return;
        }

        dialogUtils.showInformationPopUp("Création d'un exercice", "Votre exercice a bien été créé !");
        loadExercises();
        cleanInputs();
    }

    private void cleanInputs() {
        titleInput.clear();
        descriptionInput.clear();
        lifeInput.clear();
        chapterCreateList.getItems().clear();
        exerciseChapterToCreate = null;
        subjectCreateList.setText("Aucune");
        chapterCreateList.setText("Aucune");
    }

    public void setQuestionView(Exercise exercise) {

        router.goTo(QuestionController.class, controller -> {
            exercise.setLeader(leaderService.findOneLeader(leaderService.getCurrentLeaderId()));
            controller.setExercise(exercise);
            controller.setLeaderSubjects(leaderSubjects);
            controller.setExerciseService(exerciseService);
            controller.setPrecedentView(exerciseViewVbox);
            controller.setRouter(router);
            controller.setUp();
        });
    }

    @FXML
    public void setSubjectView(MouseEvent mouseEvent) throws IOException {
        this.router.goTo(SubjectController.class, controller -> {
            controller.setRouter(this.router);
        });
    }
}
