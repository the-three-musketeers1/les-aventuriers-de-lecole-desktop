package fr.sa.desktopapp.ui.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.sa.desktop.core.models.Status;
import fr.sa.desktop.core.models.Ticket;
import fr.sa.desktop.core.services.StatusService;
import fr.sa.desktop.core.services.TicketService;
import fr.sa.desktop.core.utils.parsers.JsonParser;
import fr.sa.desktop.core.utils.requests.SyncRequest;
import fr.sa.desktopapp.ui.router.Router;
import fr.sa.desktopapp.ui.utils.DialogUtils;
import fr.sa.desktopapp.ui.utils.ValidatorUtils;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.util.Callback;
import javafx.util.StringConverter;

import java.net.http.HttpClient;
import java.text.DateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class TicketController {

    private Router router;
    private ObservableList<Ticket> tickets = FXCollections.observableArrayList();
    private ObservableList<Status> statuses = FXCollections.observableArrayList();
    private DialogUtils dialogUtils;
    private TicketService ticketService;
    private StatusService statusService;

    public TableView<Ticket> ticketsTable;
    public TableColumn<Ticket, String> titleColumn;
    public TableColumn<Ticket, String> characterUserColumn;
    public TableColumn<Ticket, String> createdAtColumn;
    public TableColumn<Ticket, String> updatedAtColumn;
    public TableColumn<Ticket, String> statusColumn;
    public Label userFirstName;
    public Label userLastName;
    public Label characterActionPoints;
    public Label characterLevel;
    public Label characterName;
    public Label ticketTitle;
    public Label ticketCreatedDate;
    public Label ticketUpdatedDate;
    public Label ticketNbMessages;
    public ComboBox<Status> statusesComboBox;
    private List<Ticket> ticketList;
    private List<Status> statusList;
    private Ticket selectedTicket;

    @FXML
    public void initialize() {
        this.ticketService = new TicketService(new JsonParser(new ObjectMapper()),
                new SyncRequest(HttpClient.newHttpClient()));
        this.statusService = new StatusService(new JsonParser(new ObjectMapper()),
                new SyncRequest(HttpClient.newHttpClient()));
        this.dialogUtils = new DialogUtils();
        this.ticketList = ticketService.findTickets();
        this.statusList = statusService.findStatus();
        this.statuses.addAll(statusList);
        this.clearTicketInformations();
        this.setTicketsTable();
    }

    private void setTicketsTable() {
        titleColumn.setCellValueFactory(new PropertyValueFactory<>("title"));
        characterUserColumn.setCellValueFactory(param -> new SimpleStringProperty(param.getValue().getCharacter().getName()));
        createdAtColumn.setCellValueFactory(param -> {
            return createDateStringProperty(new Locale("FR", "fr"), param.getValue().getDate());
        });
        updatedAtColumn.setCellValueFactory(param -> {
            return createDateStringProperty(new Locale("FR", "fr"), param.getValue().getUpdatedAt());
        });
        statusColumn.setCellValueFactory(param -> new SimpleStringProperty(param.getValue().getStatus().getState()));

        tickets.addAll(ticketList);
        ticketsTable.setItems(tickets);
        ticketsTable.getSelectionModel().selectedItemProperty().addListener(observable -> {
            selectedTicket = ticketsTable.getSelectionModel().getSelectedItem();
            setTicketInformation();
        });
    }

    private void setTicketInformation() {
        if (selectedTicket == null) {
            return;
        }
        userFirstName.setText(selectedTicket.getCharacter().getUser().getFirstname());
        userLastName.setText(selectedTicket.getCharacter().getUser().getLastname());

        characterName.setText(selectedTicket.getCharacter().getName());
        characterActionPoints.setText(selectedTicket.getCharacter().getAction_points().toString());
        characterLevel.setText(selectedTicket.getCharacter().getLevel().toString());

        ticketTitle.setText(selectedTicket.getTitle());
        ticketCreatedDate.setText(createDateStringFormat(new Locale("FR", "fr"), selectedTicket.getDate()));
        ticketUpdatedDate.setText(createDateStringFormat(new Locale("FR", "fr"), selectedTicket.getUpdatedAt()));

        if (selectedTicket.getMessages() != null) {
            ticketNbMessages.setText(String.valueOf(selectedTicket.getMessages().length));
        }
        setStatusComboBox();
    }

    private void setStatusComboBox() {
        statusesComboBox.setItems(statuses);
        statusesComboBox.setConverter(new StringConverter<Status>() {
            @Override
            public String toString(Status object) {
                if (object != null) {
                    return object.getState();
                }
                return null;
            }

            @Override
            public Status fromString(String string) {
                return statusesComboBox.getItems()
                        .stream()
                        .filter(status -> status.getState().equals(string))
                        .findFirst()
                        .orElse(null);
            }
        });
        statusesComboBox.getSelectionModel().select(selectedTicket.getStatus());
    }

    private void clearTicketInformations() {
        userFirstName.setText("");
        userLastName.setText("");
        characterActionPoints.setText("");
        characterLevel.setText("");
        characterName.setText("");
        ticketTitle.setText("");
        ticketCreatedDate.setText("");
        ticketUpdatedDate.setText("");
        ticketNbMessages.setText("");
        statusesComboBox.getSelectionModel().clearSelection();
        statusesComboBox.setItems(null);
    }

    public void setRouter(Router router) {
        this.router = router;
    }

    public void onShowMessages(MouseEvent mouseEvent) {
        if (selectedTicket != null) {
            final var concernedTicket = ticketService.findTicketById(selectedTicket);
            router.goTo(MessageController.class, controller -> {
                controller.setRouter(router);
                controller.setTicket(concernedTicket);
            });
        }
    }

    private SimpleStringProperty createDateStringProperty(Locale locale, Date date) {
        return new SimpleStringProperty(createDateStringFormat(locale, date));
    }

    private String createDateStringFormat(Locale locale, Date date) {
        return DateFormat.getDateTimeInstance(
                DateFormat.SHORT,
                DateFormat.SHORT,
                locale)
                .format(date);
    }

    public void onUpdateTicketStatus(MouseEvent mouseEvent) {
        final var selectedStatus = statusesComboBox.getSelectionModel().getSelectedItem();
        if (ticketService.updateStatus(selectedTicket, selectedStatus)) {
            final var indexOfTicket = tickets.indexOf(selectedTicket);
            selectedTicket.setStatus(selectedStatus);
            tickets.set(indexOfTicket, selectedTicket);
            clearTicketInformations();
            ticketsTable.getSelectionModel().clearSelection();
        } else {
            dialogUtils.showErrorPopUp("Mise à jour du statut d'un ticket",
                    "Le statut du ticket n'a pas été mise à jour", "");
        }
    }
}
