package fr.sa.desktopapp.ui.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.sa.desktop.core.models.Guild;
import fr.sa.desktop.core.plugin.GuildPluginTemplate;
import fr.sa.desktop.core.plugin.models.ExerciseInformation;
import fr.sa.desktop.core.plugin.models.Discipline;
import fr.sa.desktop.core.plugin.models.StatisticsExercise;
import fr.sa.desktop.core.services.CharacterExerciseService;
import fr.sa.desktop.core.services.ExerciseService;
import fr.sa.desktop.core.services.SubjectService;
import fr.sa.desktop.core.utils.parsers.JsonParser;
import fr.sa.desktop.core.utils.requests.SyncRequest;
import fr.sa.desktopapp.ui.router.Router;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.BorderPane;

import java.net.http.HttpClient;
import java.util.List;

public class GuildPluginController {

    private GuildPluginTemplate guildPluginTemplate;

    private BorderPane mainWindow;

    private Router router;

    private Guild guild;

    private CharacterExerciseService characterExerciseService;
    private ExerciseService exerciseService;
    private SubjectService subjectService;

    @FXML
    ScrollPane mainPanel;

    @FXML
    ScrollPane secondaryPanel;

    @FXML
    Label pluginName;

    @FXML
    public void initialize() {
        characterExerciseService = new CharacterExerciseService(new JsonParser(new ObjectMapper()),
                new SyncRequest(HttpClient.newHttpClient()));
        exerciseService = new ExerciseService(new JsonParser(new ObjectMapper()),
                new SyncRequest(HttpClient.newHttpClient()));
        subjectService = new SubjectService(new JsonParser(new ObjectMapper()),
                new SyncRequest(HttpClient.newHttpClient()));
    }

    public void setPluginInstance(GuildPluginTemplate guildPluginTemplate) {
        this.guildPluginTemplate = guildPluginTemplate;
        List<StatisticsExercise> statisticsExerciseByGuild = characterExerciseService.findStatisticsExerciseByGuild(guild);
        List<ExerciseInformation> exerciseList = exerciseService.findExercisesByGuild(guild);
        List<Discipline> pSubjectList = subjectService.findSubjectsByGuild(guild);
        mainPanel.setContent(guildPluginTemplate.getMainPanel(statisticsExerciseByGuild));
        secondaryPanel.setContent(guildPluginTemplate.getSecondaryPanel(pSubjectList, exerciseList));
        pluginName.setText(guildPluginTemplate.getTitle());
    }

    public void setGuild(Guild guild) {
        this.guild = guild;
    }

    public void setMainWindow(BorderPane mainWindow) {
        this.mainWindow = mainWindow;
    }

    public void setRouter(Router router) {
        this.router = router;
    }
}
