package fr.sa.desktopapp.ui.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.sa.desktop.core.models.Leader;
import fr.sa.desktop.core.services.AuthService;
import fr.sa.desktop.core.services.LeaderService;
import fr.sa.desktop.core.utils.parsers.JsonParser;
import fr.sa.desktop.core.utils.requests.SyncRequest;
import fr.sa.desktopapp.ui.router.Router;
import fr.sa.desktopapp.ui.router.StageRouter;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;

import java.io.FileReader;
import java.io.IOException;
import java.net.http.HttpClient;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

public class MainController {

    private Router router;
    private StageRouter stageRouter;
    private AuthService authService;
    private LeaderService leaderService;
    private int leaderId;

    @FXML
    private BorderPane mainWindow;

    @FXML
    private Label welcomeLabel;

    @FXML
    private ImageView appLogo;

    @FXML
    private MenuButton settingsButton;

    @FXML
    private MenuItem logOut;

    @FXML
    private MenuItem pluginsBtn;

    @FXML
    private MenuItem exitBtn;

    @FXML
    private Hyperlink guildLink;

    @FXML
    private Hyperlink expeditionLink;

    @FXML
    private Hyperlink exerciseLink;

    @FXML
    private Hyperlink ticketLink;

    @FXML
    public void initialize() {
        this.router = new Router(this.mainWindow);
        this.authService = new AuthService(new JsonParser(new ObjectMapper()),
                new SyncRequest(HttpClient.newHttpClient()));
        this.leaderService = new LeaderService(new JsonParser(new ObjectMapper()),
                new SyncRequest(HttpClient.newHttpClient()));
        this.leaderId = this.leaderService.getCurrentLeaderId();
        Leader connectedLeader = this.leaderService.findOneLeader(leaderId);
        welcomeLabel.setText("Bienvenue " + connectedLeader.getFirstname() + " " + connectedLeader.getLastname());
        this.pluginsBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                router.goTo(PluginsGestionController.class, pluginsGestionController -> pluginsGestionController.setRouter(router));
            }
        });
    }

    @FXML
    public void setExerciseView() {
        router.goTo(ExerciseController.class, controller -> controller.setRouter(router));
    }

    @FXML
    public void setGuildView() { router.goTo(GuildListController.class, controller -> controller.setRouter(router)); }

    @FXML
    public void setExpeditionView() {
        router.goTo(ExpeditionController.class, controller -> controller.setRouter(router));
    }

    @FXML
    public void setTicketView(MouseEvent mouseEvent) {
        router.goTo(TicketController.class, controller -> controller.setRouter(router));
    }

    public void setStageRouter(StageRouter stageRouter) {
        this.stageRouter = stageRouter;
    }

    public void onLogout(ActionEvent actionEvent) {
        if (authLogoutAndDeleteToken()) {
            this.stageRouter.goTo(LoginController.class, loginController -> {
                loginController.setStageRouter(this.stageRouter);
            });
            return;
        }
        throw new RuntimeException("MainController onLogout problem");
    }

    @FXML
    public void goHomePage() {
        this.stageRouter.goTo(MainController.class, mainController -> {
            mainController.setStageRouter(this.stageRouter);
        });
    }


    private boolean authLogoutAndDeleteToken() {
        try {
            if (this.authService.logout()) {
                return this.authService.deleteAuthTokenFile();
            }
            throw new RuntimeException("MainController, fail to logout with authService");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    public void onExit(ActionEvent actionEvent) {
        Platform.exit();
    }
}
