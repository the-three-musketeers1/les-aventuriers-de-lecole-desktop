package fr.sa.desktopapp.ui.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.sa.desktop.core.models.Leader;
import fr.sa.desktop.core.services.AuthService;
import fr.sa.desktop.core.utils.parsers.JsonParser;
import fr.sa.desktop.core.utils.requests.SyncRequest;
import fr.sa.desktopapp.ui.router.Router;
import fr.sa.desktopapp.ui.router.StageRouter;
import fr.sa.desktopapp.ui.utils.DialogUtils;
import fr.sa.desktopapp.ui.utils.ValidatorUtils;
import fr.sa.desktopapp.ui.validators.UserValidator;
import javafx.fxml.FXML;
import javafx.scene.control.DatePicker;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import org.apache.commons.validator.routines.EmailValidator;

import java.net.http.HttpClient;
import java.net.http.HttpResponse;
import java.time.Instant;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SubscribeController {

    private StageRouter stageRouter;
    private ValidatorUtils validatorUtils;
    private UserValidator userValidator;
    private AuthService authService;
    private DialogUtils dialogUtils;

    @FXML
    private TextField firstnameInput;

    @FXML
    private TextField lastnameInput;

    @FXML
    private TextField emailInput;

    @FXML
    private PasswordField passwordInput;

    public void setStageRouter(StageRouter stageRouter) {
        this.stageRouter = stageRouter;
    }

    @FXML
    public void initialize() {
        this.validatorUtils = new ValidatorUtils(new DialogUtils());
        this.userValidator = new UserValidator(validatorUtils, EmailValidator.getInstance());
        this.authService = new AuthService(new JsonParser(new ObjectMapper()),
                new SyncRequest(HttpClient.newHttpClient(), false));
        this.dialogUtils = new DialogUtils();
    }

    public void onLoginRoute(MouseEvent mouseEvent) {
        this.stageRouter.goTo(LoginController.class, loginController -> {
            loginController.setStageRouter(this.stageRouter);
        });
    }

    private List<String> checkSubscribeForm() {
        List<String> errors = new ArrayList<>();
        if (!userValidator.isTextRequiredValid(this.firstnameInput)) {
            errors.add("Le champ 'Prénom' est requis");
        }
        if (!userValidator.isTextRequiredValid(this.lastnameInput)) {
            errors.add("Le champ 'Nom' est requis");
        }
        if (!userValidator.isEmailValid(this.emailInput)) {
            errors.add("Le champ 'Email' n'est pas valide");
        }
        if (!userValidator.isPasswordValid(this.passwordInput)) {
            errors.add("Le champ 'Mot de passe' doit contenir entre 4 et 255 charactères");
        }
        return errors;
    }

    private void displayErrorByResponse(HttpResponse<String> response) {
        if (response.statusCode() == 403) {
            this.dialogUtils.showErrorPopUp("Inscription", "Vous êtes probablement déjà inscrit ou vous avez transmis des données non conformes.", "");
        } else {
            this.dialogUtils.showErrorPopUp("Connexion", "Problème de connexion, veuillez réessayer plus tard.", "");
        }
    }

    @FXML
    private void onSubscribe(MouseEvent mouseEvent) {
        if (!this.validatorUtils.showPotentialErrors("Inscription",
                "L'inscription n'a pa pu être établie", checkSubscribeForm())) {
            return;
        }
        final var leader = Leader.builder()
                .firstname(this.firstnameInput.getText())
                .lastname(this.lastnameInput.getText())
                .email(this.emailInput.getText())
                .password(this.passwordInput.getText())
                .is_teacher(true)
                .build();
        final var response = this.authService.subscribe(leader);
        if (response == null || response.statusCode() != 201) {
            assert response != null;
            displayErrorByResponse(response);
            return;
        }


        this.dialogUtils.showInformationPopUp("Inscription réussie !", "Cher/Chère Leader ! Vous êtes enfin inscrit(e) !");
        this.stageRouter.goTo(LoginController.class, loginController -> {
            loginController.setStageRouter(this.stageRouter);
        });
    }
}
