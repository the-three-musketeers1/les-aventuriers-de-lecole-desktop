package fr.sa.desktopapp.ui.validators;

import fr.sa.desktopapp.ui.utils.ValidatorUtils;
import javafx.scene.control.DatePicker;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import org.apache.commons.validator.routines.EmailValidator;

import java.time.LocalDate;
import java.time.ZoneId;

public class UserValidator {

    private ValidatorUtils validatorUtils;
    private EmailValidator emailValidator;

    public UserValidator(ValidatorUtils validatorUtils, EmailValidator emailValidator) {
        this.validatorUtils = validatorUtils;
        this.emailValidator = emailValidator;
    }

    public boolean isTextRequiredValid(TextField textField) {
        return this.validatorUtils.hasLengthBetween(textField.getText(), 1, 255);
    }

    public boolean isEmailValid(TextField emailField) {
        return this.emailValidator.isValid(emailField.getText());
    }

    public boolean isPasswordValid(PasswordField passwordField) {
        return this.validatorUtils.hasLengthBetween(passwordField.getText(), 4, 255);
    }
}
