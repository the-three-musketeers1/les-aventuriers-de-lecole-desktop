package fr.sa.desktopapp.ui.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.sa.desktop.core.models.Adventure;
import fr.sa.desktop.core.models.Guild;
import fr.sa.desktop.core.services.AdventureService;
import fr.sa.desktop.core.services.GuildService;
import fr.sa.desktop.core.services.LeaderService;
import fr.sa.desktop.core.utils.parsers.JsonParser;
import fr.sa.desktop.core.utils.requests.SyncRequest;
import fr.sa.desktopapp.ui.router.Router;
import fr.sa.desktopapp.ui.utils.DialogUtils;
import fr.sa.desktopapp.ui.utils.NodeUtils;
import fr.sa.desktopapp.ui.utils.ValidatorUtils;
import fr.sa.desktopapp.ui.validators.ExpeditionValidator;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Text;

import java.net.http.HttpClient;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public class ExpeditionController {
    //dependances
    private LeaderService leaderService;
    private GuildService guildService;
    private AdventureService adventureService;
    private DialogUtils dialogUtils;
    private NodeUtils nodeUtils;
    private Router router;
    private ExpeditionValidator validator;
    private ValidatorUtils validatorUtils;

    private List<Adventure> leaderExpeditions;

    @FXML
    private VBox expeditionViewVbox;

    @FXML
    private VBox expeditionList;

    @FXML
    private TextField titleInput;

    @FXML
    private TextField bonusInput;

    @FXML
    private TextArea descriptionInput;

    @FXML
    private Button createExpeditionBtn;
    private int leaderId;


    public void setRouter(Router router) {
        this.router = router;
    }

    @FXML
    public void initialize() {
        leaderService = new LeaderService(new JsonParser(new ObjectMapper()),
                new SyncRequest(HttpClient.newHttpClient()));
        adventureService = new AdventureService(new JsonParser(new ObjectMapper()),
                new SyncRequest(HttpClient.newHttpClient()));
        dialogUtils = new DialogUtils();
        nodeUtils = new NodeUtils();
        validatorUtils = new ValidatorUtils(dialogUtils);
        validator = new ExpeditionValidator(validatorUtils);
        leaderId = leaderService.getCurrentLeaderId();

        loadExpeditions();
    }

    private List<String> checkCreateForm() {
        List<String> errors = new ArrayList<>();

        if(!validator.isTitleValid(titleInput.getText())) {
            errors.add("Le titre doit être renseigné");
        }

        if(!validator.isDescriptionValid(descriptionInput.getText())) {
            errors.add("La description doit être renseignée");
        }

        if(!validator.isBonusValid(bonusInput.getText())) {
            errors.add("Le bonus d'expérience doit être renseigné");
        }

        return errors;
    }

    @FXML
    public void resetFilters() {

    }

    private void emptyExpeditions() {
        expeditionList.getChildren().clear();
    }


    private void deleteExpedition(Adventure expedition) {
        Alert confirmationDialog = dialogUtils.showConfirmationPopUp("Supprimer une expédition", "Voulez-vous vraiment supprimer cette expédition ?");
        Optional<ButtonType> result = confirmationDialog.showAndWait();
        if (result.isPresent() && result.get() == ButtonType.OK){
            if(adventureService.deleteExpedition(expedition.getId())) {
                dialogUtils.showInformationPopUp("Supprimer une expédition", "Votre expédition a bien été supprimée");
                loadExpeditions();
            } else {
                dialogUtils.showErrorPopUp("Supprimer une expédition", "Votre expédition n'a pas pu être supprimée", null);
            }
        }
    }

    public void updateExpedition(Adventure expedition) {
        if(adventureService.updateExpedition(expedition)) {
            dialogUtils.showInformationPopUp("Modifier une expédition",
                    "Votre expédition a bien été mise à jour !");
            loadExpeditions();
        } else {
            dialogUtils.showErrorPopUp("Modifier une expédition",
                    "Votre expédition n'a pas pu être mise à jour",
                    null);
        }
    }


    private void loadExpeditions() {
        leaderExpeditions = leaderService.findLeaderExpeditions(this.leaderId);
        displayExpeditions();
    }

    @FXML
    private void createExpedition() {

        if(!validatorUtils.showPotentialErrors("Création d'expédition",
                "Votre expédition n'a pas pu être créée", checkCreateForm())) {
            return;
        }

        Adventure expedition = Adventure.builder()
                .title(titleInput.getText())
                .description(descriptionInput.getText())
                .experienceBonus(Integer.parseInt(bonusInput.getText()))
                .leader(leaderService.findOneLeader(this.leaderId))
                .isExpedition(true)
                .build();

        if(!adventureService.createExpedition(expedition)) {
            dialogUtils.showErrorPopUp("Création d'expédition", "Votre expédition n'a pas pu être créée", "");
            return;
        }

        dialogUtils.showInformationPopUp("Création d'une expédition", "Votre expédition a bien été créée !");
        loadExpeditions();
        cleanInputs();
    }

    private void cleanInputs() {
        titleInput.clear();
        descriptionInput.clear();
        bonusInput.clear();
    }

    public void displayExpeditions() {
        emptyExpeditions();
        leaderExpeditions.forEach((this::displayOneExpedition));
    }

    public void displayOneExpedition(Adventure expedition) {
        HBox hBox = new HBox();
        hBox.setAlignment(Pos.CENTER_LEFT);

        TextField tf = nodeUtils.createTextFieldWithBinding(expedition.getTitle(),
                (obs, old, value) -> {
                    if(validator.isTitleValid(value)) {
                        expedition.setTitle(value);
                    }
                });
        tf.setPrefWidth(200);
        HBox.setMargin(tf, new Insets(0, 20, 0, 0));
        hBox.getChildren().add(tf);
        TextField df = nodeUtils.createTextFieldWithBinding(expedition.getDescription(),
                (obs, old, value) -> {
                    if(validator.isDescriptionValid(value)) {
                        expedition.setDescription(value);
                    }
                });
        df.setPrefWidth(400);
        HBox.setMargin(df, new Insets(0, 20, 0, 0));
        hBox.getChildren().add(df);

        TextField ef = nodeUtils.createTextFieldWithBinding(expedition.getExperienceBonus().toString(),
                (obs, old, value) -> {
                    if(validator.isBonusValid(value)) {
                        expedition.setExperienceBonus(Integer.parseInt(value));
                    }
                });
        ef.setPrefWidth(200);
        HBox.setMargin(ef, new Insets(0, 20, 0, 0));
        hBox.getChildren().add(ef);
        hBox.getChildren().add(nodeUtils.createPanes(1).get(0));
        hBox.getChildren().add(nodeUtils.createIconWithEventAndStyle("/images/check.png", mouseEvent -> updateExpedition(expedition)));
        hBox.getChildren().add(nodeUtils.createIconWithEventAndStyle("/images/trash.png", mouseEvent -> deleteExpedition(expedition)));
        hBox.setPadding(new Insets(0, 0, 10, 0));
        expeditionList.getChildren().add(hBox);
    }

    @FXML
    private void setExpeditionGuildView() {
        router.goTo(ExpeditionGuildController.class, controller -> {
            controller.setLeaderExpeditions(leaderExpeditions);
            controller.setNodeUtils(nodeUtils);
            controller.setRouter(router);
            controller.setUp();
        });
    }

    @FXML
    private void setExpeditionExerciseView() {
        router.goTo(ExpeditionExerciseController.class, controller -> {
            controller.setLeaderExpeditions(leaderExpeditions);
            controller.setNodeUtils(nodeUtils);
            controller.setRouter(router);
            controller.setUp();
        });
    }



}
