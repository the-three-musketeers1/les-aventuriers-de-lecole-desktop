package fr.sa.desktopapp.ui.router;

import fr.sa.desktop.core.models.Exercise;
import fr.sa.desktopapp.ui.controllers.QuestionController;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.function.Consumer;

public class Router {
    private final BorderPane mainWindow;

    public Router(BorderPane mainWindow) {
        this.mainWindow = mainWindow;
    }

    public BorderPane getMainWindow() {
        return mainWindow;
    }

    public void goTo(final Class<?> controllerClass) {
        goTo(controllerClass, __ -> {});
    }

    public <T> void goTo(final Class<T> controllerClass, final Consumer<T> controllerConsumer) {
        final var viewName = controllerClass.getSimpleName().replace("Controller", "");
        final var view = loadView(viewName, controllerConsumer);
        mainWindow.setCenter(view);
    }

    /*public Parent loadView(final String viewName) {
        final var viewPath = String.format("/views/%sView.fxml", viewName);

        try {
            return FXMLLoader.load(this.getClass().getResource(viewPath));
        } catch (final IOException e) {
            throw new IllegalStateException(String.format("Cannot load view: %s", viewPath, e));
        }
    }*/

    private<T> Parent loadView(final String viewName, final Consumer<T> controllerConsumer) {
        final var viewPath = String.format("/views/%sView.fxml", viewName);
        try {
            final var fxmlLoader = new FXMLLoader(this.getClass().getResource(viewPath));
            final Parent view = fxmlLoader.load();
            controllerConsumer.accept(fxmlLoader.getController());

            return view;
        } catch (final IOException e) {
            throw new IllegalStateException(String.format("Cannot load view: %s", viewPath, e));
        }
    }
}
