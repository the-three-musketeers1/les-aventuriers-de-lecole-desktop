package fr.sa.desktopapp.ui;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.sa.desktop.core.models.Question;
import fr.sa.desktop.core.services.AuthService;
import fr.sa.desktop.core.utils.parsers.JsonParser;
import fr.sa.desktop.core.utils.requests.SyncRequest;
import fr.sa.desktopapp.ui.controllers.AnswerController;
import fr.sa.desktopapp.ui.controllers.LoginController;
import fr.sa.desktopapp.ui.router.Router;
import fr.sa.desktopapp.ui.router.StageRouter;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Screen;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.http.HttpClient;

public class SchoolAdventurersApplication extends Application {

    private Stage primaryStage;
    private StageRouter stageRouter;

    @Override
    public void init() {
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        this.stageRouter = new StageRouter(primaryStage);
        this.primaryStage = primaryStage;
        this.primaryStage.setMaximized(false);
        this.primaryStage.setFullScreen(false);
        this.primaryStage.setTitle("Les aventuriers de l'école");
        Image applicationIcon = new Image(getClass().getResourceAsStream("/images/logo.png"));
        this.primaryStage.getIcons().add(applicationIcon);

        this.stageRouter.goTo(LoginController.class, loginController -> {
            loginController.setStageRouter(stageRouter);
        });

        this.primaryStage.setX(1440);
        this.primaryStage.setY(900);

        this.primaryStage.centerOnScreen();

        // to get maximized window for all views
        /*Screen screen = Screen.getPrimary();
        Rectangle2D bounds = screen.getVisualBounds();
        this.primaryStage.setX(bounds.getMinX());
        this.primaryStage.setY(bounds.getMinY());
        this.primaryStage.setWidth(bounds.getWidth());
        this.primaryStage.setHeight(bounds.getHeight());*/

        this.primaryStage.show();
    }

    private Parent loadView(final String viewName) {
        final String viewPath = String.format("/views/%sView.fxml", viewName);

        try {
            return FXMLLoader.load(this.getClass().getResource(viewPath));
        } catch (IOException e) {
            e.printStackTrace();
            throw new IllegalStateException(String.format("Can not load view with this path : %s", viewPath));
        }
    }

    @Override
    public void stop() {
        final var authService = new AuthService(new JsonParser(new ObjectMapper()),
                new SyncRequest(HttpClient.newHttpClient(), false));
        if (authService.hasAuthTokenFile()) {
            try {
                if (authService.logout()) {
                    authService.deleteAuthTokenFile();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
