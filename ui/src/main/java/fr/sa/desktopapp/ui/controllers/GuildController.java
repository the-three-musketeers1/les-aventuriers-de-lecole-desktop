package fr.sa.desktopapp.ui.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.sa.desktop.core.plugin.GuildPluginTemplate;
import fr.sa.desktop.core.models.CharacterInformations;
import fr.sa.desktop.core.models.Guild;
import fr.sa.desktop.core.plugin.MemberPluginTemplate;
import fr.sa.desktop.core.plugin.PluginScanner;
import fr.sa.desktop.core.services.GuildService;
import fr.sa.desktop.core.utils.parsers.JsonParser;
import fr.sa.desktop.core.utils.requests.SyncRequest;
import fr.sa.desktopapp.ui.PluginsTemplate.GuildPluginTemplateImpl.GuildStats;
import fr.sa.desktopapp.ui.router.Router;
import fr.sa.desktopapp.ui.utils.DialogUtils;
import fr.sa.desktopapp.ui.utils.NodeUtils;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.text.TextAlignment;

import java.net.http.HttpClient;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class GuildController {

    @FXML
    VBox memberList;

    @FXML
    VBox memberActionList;

    @FXML
    VBox guildActionList;

    @FXML
    Label guildName;
    private Guild guild;
    private BorderPane mainWindow;
    private GuildService guildService;
    private NodeUtils nodeUtils;
    private DialogUtils dialogUtils;

    private List<GuildPluginTemplate> listGuildPlugin;
    private List<CharacterInformations> members;
    private Router router;


    @FXML
    public void initialize() {
        guildService = new GuildService(new JsonParser(new ObjectMapper()),
                new SyncRequest(HttpClient.newHttpClient()));
        nodeUtils = new NodeUtils();
        dialogUtils = new DialogUtils();
        listGuildPlugin = new ArrayList<>();
        List<GuildPluginTemplate> guildPluginTemplates = PluginScanner.scanPlugin(GuildPluginTemplate.class);
        List<MemberPluginTemplate> memberPlugins = new ArrayList<>();
        memberPlugins.addAll(PluginScanner.scanPlugin(MemberPluginTemplate.class));
        listGuildPlugin.add(new GuildStats());
        listGuildPlugin.addAll(guildPluginTemplates);
        listGuildPlugin.forEach(guildPlugin -> {

            Button button = nodeUtils.createListActionButton(guildPlugin.getButtonName());
            button.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent mouseEvent) {
                    router.goTo(GuildPluginController.class, controller -> {
                        controller.setRouter(router);
                        controller.setGuild(guild);
                        controller.setPluginInstance(guildPlugin);
                        controller.setMainWindow(mainWindow);
                    });
                }
            });
            guildActionList.getChildren().add(button);
        });
        memberPlugins.forEach(memberPlugin -> {
            Button buttonMember = nodeUtils.createListActionButton(memberPlugin.setButtonName());
            buttonMember.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent mouseEvent) {
                    router.goTo(MemberPluginController.class, controller -> {
                        controller.setGuild(guild);
                        controller.setPluginInstance(memberPlugin);
                    });
                }
            });
            memberActionList.getChildren().add(buttonMember);
        });

    }
    public void setMainWindow(BorderPane mainWindow) {
        this.mainWindow = mainWindow;
    }

    public void setGuild(Guild guild) {
        this.guild = guild;
        guildName.setText(this.guild.getName());
        loadMembers();
    }

    private void loadMembers() {
        this.members = this.guildService.getGuildMemberByGuildId(guild.getId());
        setMemberList();
    }

    private void deleteCharacter(CharacterInformations character) {
        Alert confirmationDialog = dialogUtils.showConfirmationPopUp("Supprimer un membre", "Voulez-vous vraiment supprimer ce membre ?");
        Optional<ButtonType> result = confirmationDialog.showAndWait();
        if (result.isPresent() && result.get() == ButtonType.OK){
            if(guildService.deleteMember(character.getId())) {
                dialogUtils.showInformationPopUp("Supprimer un membre",
                        character.getPseudo() +" ne fait plus parti de votre guilde");
                loadMembers();
            } else {
                dialogUtils.showErrorPopUp("Supprimer un membre", "Ce membre n'a pas pu être supprimé", null);
            }
        }
    }

    private void setMemberList() {
        memberList.getChildren().clear();
        this.members.forEach(member -> {
            ImageView button = nodeUtils.createIconWithEventAndStyle("/images/trash.png", mouseEvent -> deleteCharacter(member));
            Label pseudo = new Label(member.getPseudo());
            setTextProperties(pseudo);
            Label name = new Label(member.getLast_name() + " " + member.getFirst_name());
            setTextProperties(name);
            Label level = new Label(member.getLevel() + "");
            setTextProperties(level);
            Label experience = new Label(member.getExperience() + "");
            setTextProperties(experience);
            Label actionPoint = new Label(member.getAction_points() + "");
            setTextProperties(actionPoint);
            Label role = new Label(member.getRole());
            setTextProperties(role);

            final Pane spacer = nodeUtils.createPanes(1).get(0);

            HBox memberBox = new HBox(pseudo, name, role, level, experience, actionPoint, spacer, button);
            memberBox.setPadding(new Insets(10, 0, 0, 5));

            memberList.getChildren().add(memberBox);
        });
    }
    private void setTextProperties(Label label) {
        label.setPrefWidth(150);
        label.setTextAlignment(TextAlignment.CENTER);
    }

    public void setRouter(Router router) {
        this.router = router;
    }

}
