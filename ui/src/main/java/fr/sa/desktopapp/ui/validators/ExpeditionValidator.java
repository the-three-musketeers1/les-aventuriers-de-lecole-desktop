package fr.sa.desktopapp.ui.validators;

import fr.sa.desktopapp.ui.utils.ValidatorUtils;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import org.apache.commons.lang3.StringUtils;

import java.time.LocalDate;

public class ExpeditionValidator {

    private ValidatorUtils validatorUtils;

    public ExpeditionValidator(ValidatorUtils validatorUtils) {
        this.validatorUtils = validatorUtils;
    }

    public boolean isTitleValid(String title) {
        return validatorUtils.hasLengthBetween(title, 1, 60);
    }

    public boolean isDescriptionValid(String description) {
        return validatorUtils.hasLengthBetween(description, 1, 60000);
    }

    public boolean isBonusValid(String bonus) {
        return validatorUtils.isInteger(bonus);
    }

}
