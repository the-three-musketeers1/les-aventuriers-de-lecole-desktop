package fr.sa.desktopapp.ui.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.sa.desktop.core.models.Subject;
import fr.sa.desktop.core.services.LeaderService;
import fr.sa.desktop.core.services.SubjectService;
import fr.sa.desktop.core.utils.parsers.JsonParser;
import fr.sa.desktop.core.utils.requests.SyncRequest;
import fr.sa.desktopapp.ui.router.Router;
import fr.sa.desktopapp.ui.utils.DialogUtils;
import fr.sa.desktopapp.ui.utils.ValidatorUtils;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import org.jetbrains.annotations.NotNull;

import java.net.http.HttpClient;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class SubjectController {

    private Router router;
    private ObservableList<Subject> subjects = FXCollections.observableArrayList();
    private SubjectService subjectService;
    private DialogUtils dialogUtils;
    private ValidatorUtils validatorUtils;
    private Subject subjectToEdit;
    private boolean isEdit;

    @FXML
    private TableView<Subject> subjectTable;

    @FXML
    private TableColumn<Subject, String> titleColumn;

    @FXML
    private TableColumn<Subject, String> descriptionColumn;

    @FXML
    private Label formTitle;

    @FXML
    private TextField titleField;

    @FXML
    private TextArea descriptionField;

    @FXML
    private Button subjectButtonForm;

    @FXML
    private Button buttonChangeForm;

    @FXML
    public void initialize() {
        LeaderService leaderService = new LeaderService(new JsonParser(new ObjectMapper()),
                new SyncRequest(HttpClient.newHttpClient()));
        List<Subject> leaderSubjects = leaderService.findLeaderSubjects(leaderService.getCurrentLeaderId())
                .stream()
                .filter(sub -> sub.getParentSubject() == null)
                .collect(Collectors.toList());
        subjectService = new SubjectService(new JsonParser(new ObjectMapper()),
                new SyncRequest(HttpClient.newHttpClient()));
        dialogUtils = new DialogUtils();
        validatorUtils = new ValidatorUtils(dialogUtils);

        titleColumn.setCellValueFactory(new PropertyValueFactory<>("title"));
        descriptionColumn.setCellValueFactory(new PropertyValueFactory<>("description"));
        subjects.addAll(leaderSubjects);
        subjectTable.setItems(subjects);
        subjectTable.getSelectionModel().selectedItemProperty().addListener(observable -> {
            if (this.isEdit) {
                this.changeToCreateForm();
            }
        });

        subjectButtonForm.setOnMouseClicked(event -> this.createSubject());
        buttonChangeForm.setOnMouseClicked(event -> this.changeToEditForm());
        this.isEdit = false;
    }

    private void createSubject() {
        if (!validatorUtils.showPotentialErrors("Création de matière",
                "Votre matière n'a pas pu être créée", checkSubjectForm())) {
            return;
        }

        final var subject = Subject.builder()
                .title(this.titleField.getText().trim())
                .description(this.descriptionField.getText().trim())
                .build();

        final var newSubject = this.subjectService.createSubject(subject);
        if (newSubject == null) {
            final var errorMessage = String.format("Votre matière n'a pas pu être créée, vérifier bien si la matière %s n'est pas déjà créée", titleField.getText());
            dialogUtils.showErrorPopUp("Création d'une matière", errorMessage, "");
            return;
        }
        dialogUtils.showInformationPopUp("Création d'une matière", "Votre matière a bien été créée !");
        subjects.add(newSubject);
        cleanInputs();
    }

    private void updateSubject(@NotNull Subject subjectToEdit) {
        final var subject = Subject.builder()
                .id(subjectToEdit.getId())
                .title(this.titleField.getText().trim())
                .description(this.descriptionField.getText().trim())
                .build();
        final var subjectTitle = subject.getTitle();
        final var subjectDescription = subject.getDescription();
        if (!validatorUtils.showPotentialErrors("Edition d'une matière",
                "Votre matière n'a pas pu être éditée", checkSubjectForm())) {
            return;
        }
        if (subjectTitle.equals(subjectToEdit.getTitle()) && subjectDescription.equals(subjectToEdit.getDescription())) {
            dialogUtils.showErrorPopUp("Edition d'une matière",
                    "Votre matière éditée doit avoir des valeurs différents de sa valeur d'origine. Changer au moins une valeur", ""
            );
            return;
        }
        if (!subjectService.updateSubject(subject)) {
            dialogUtils.showErrorPopUp("Edition d'une matière",
                    "Votre matière n'a pas pu être éditée", "");
            return;
        }
        this.dialogUtils.showInformationPopUp(
                "Edition d'une matière",
                "Votre matière a bien été édité"
        );
        this.subjects.set(this.subjects.indexOf(subjectToEdit), subject);
        this.changeToCreateForm();
    }

    private void cleanInputs() {
        titleField.clear();
        descriptionField.clear();
    }

    @NotNull
    private List<String> checkSubjectForm() {
        List<String> errors = new ArrayList<>();

        if (!validatorUtils.hasLengthBetween(this.titleField.getText().trim(), 1, 255)) {
            errors.add("Le titre doit être renseigné");
        }

        if (!validatorUtils.hasLengthBetween(this.descriptionField.getText().trim(), 1, 255)) {
            errors.add("La description doit être renseignée");
        }

        return errors;
    }

    private void changeToEditForm() {
        subjectToEdit = this.subjectTable.getSelectionModel().getSelectedItem();
        if (subjectToEdit == null) {
            return;
        }

        this.buttonChangeForm.setText("Créer");
        this.buttonChangeForm.setStyle("-fx-background-color:  #377aff");
        this.buttonChangeForm.setOnMouseClicked(event -> this.changeToCreateForm());

        this.formTitle.setText(String.format("Edition de la matière '%s'", subjectToEdit.getTitle()));

        this.titleField.setText(subjectToEdit.getTitle());
        this.descriptionField.setText(subjectToEdit.getDescription());
        this.subjectButtonForm.setText("Editer");
        this.subjectButtonForm.setStyle("-fx-background-color: #03ac13");

        this.subjectButtonForm.setOnMouseClicked(event -> this.updateSubject(subjectToEdit));
        this.isEdit = true;
    }

    private void changeToCreateForm() {

        this.cleanInputs();
        this.buttonChangeForm.setText("Editer");
        this.buttonChangeForm.setStyle("-fx-background-color: #03ac13");
        this.buttonChangeForm.setOnMouseClicked(event -> this.changeToEditForm());

        this.formTitle.setText("Création d'une matière");

        this.subjectButtonForm.setText("Créer");
        this.subjectButtonForm.setStyle("-fx-background-color:  #377aff");

        this.subjectButtonForm.setOnMouseClicked(event -> this.createSubject());
        this.isEdit = false;
    }

    public void onDeleteSubject(MouseEvent mouseEvent) {
        final var subjectToDelete = subjectTable.getSelectionModel().getSelectedItem();
        if (subjectToDelete == null) {
            return;
        }
        Alert confirmDeleteDialog = dialogUtils.showConfirmationPopUp("Suppression d'une matière", "Voulez-vous supprimer cette matière ?");
        Optional<ButtonType> result = confirmDeleteDialog.showAndWait();
        if (result.isPresent() && result.get() == ButtonType.OK) {

            if (subjectService.deleteSubject(subjectToDelete.getId())) {
                dialogUtils.showInformationPopUp("Suppression d'une matière", "Votre matière a bien été supprimée");
                subjects.remove(subjectToDelete);
            } else {
                dialogUtils.showErrorPopUp("Suppression d'une matière", "Votre matière n'a pas été supprimée", null);
            }
        }
    }

    public void setRouter(Router router) {
        this.router = router;
    }

    public void onShowSubject(MouseEvent mouseEvent) {
        final var selectedSubject = this.subjectTable.getSelectionModel().getSelectedItem();
        if (selectedSubject != null) {
            this.router.goTo(ChapterController.class, chapterController -> {
                chapterController.setRouter(router);
                chapterController.setDiscipline(selectedSubject);
            });
        }
    }
}
