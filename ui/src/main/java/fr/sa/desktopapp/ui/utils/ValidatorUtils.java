package fr.sa.desktopapp.ui.utils;

import org.apache.commons.lang3.StringUtils;

import java.time.LocalDate;
import java.util.List;

public class ValidatorUtils {

    private DialogUtils dialogUtils;

    public ValidatorUtils(DialogUtils dialogUtils) {
        this.dialogUtils = dialogUtils;
    }

    public boolean isGreatherThan(String number, int boundary) {
        try{
            return Integer.parseInt(number) > boundary;
        } catch (NumberFormatException | NullPointerException e) {
            return false;
        }
    }

    public boolean isInteger(String toParse) {
        try {
            Integer.parseInt(toParse);
            return true;
        } catch (NumberFormatException | NullPointerException e) {
            return false;
        }
    }

    public boolean hasLengthGreatherThan(String text, int boundary) {
        return !StringUtils.isEmpty(text) && text.length() > boundary;
    }

    public boolean hasLengthBetween(String text, int min, int max) {
        return !StringUtils.isEmpty(text) && text.length() >= min && text.length() <= max;
    }

    public boolean isLocalDateGreaterThan(LocalDate checkedDate, LocalDate boundaryDate) {

        return checkedDate.compareTo(boundaryDate) > 0;
    }

    public boolean showPotentialErrors(String dialogTitle, String dialogDescription, List<String> errors) {
        if(errors.size() > 0) {
            dialogUtils.showErrorPopUp(dialogTitle,
                    dialogDescription,
                    String.join("\n", errors));
            return false;
        }

        return true;
    }
}
