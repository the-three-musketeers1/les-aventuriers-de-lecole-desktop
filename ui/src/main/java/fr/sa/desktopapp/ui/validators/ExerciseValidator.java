package fr.sa.desktopapp.ui.validators;

import fr.sa.desktopapp.ui.utils.DialogUtils;
import fr.sa.desktopapp.ui.utils.ValidatorUtils;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import org.apache.commons.lang3.StringUtils;

import java.time.LocalDate;

public class ExerciseValidator {

    private ValidatorUtils validatorUtils;

    public ExerciseValidator(ValidatorUtils validatorUtils) {
        this.validatorUtils = validatorUtils;
    }

    public boolean isTitleValid(String title) {
        return validatorUtils.hasLengthBetween(title, 1, 60);
    }

    public boolean isDescriptionValid(String description) {
        return validatorUtils.hasLengthBetween(description, 1, 60000);
    }

    public boolean isLifeValid(String life) {
        return validatorUtils.isGreatherThan(life, 0);
    }

    public boolean isExperienceValid(String life) {
        return validatorUtils.isGreatherThan(life, -1);
    }

}
