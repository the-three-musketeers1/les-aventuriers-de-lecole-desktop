package fr.sa.desktopapp.ui.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.sa.desktop.core.models.*;
import fr.sa.desktop.core.services.ExerciseService;
import fr.sa.desktop.core.services.LeaderService;
import fr.sa.desktop.core.services.QuestionService;
import fr.sa.desktop.core.services.QuestionTypeService;
import fr.sa.desktop.core.utils.parsers.JsonParser;
import fr.sa.desktop.core.utils.requests.SyncRequest;
import fr.sa.desktopapp.ui.router.Router;
import fr.sa.desktopapp.ui.utils.DialogUtils;
import fr.sa.desktopapp.ui.utils.NodeUtils;
import fr.sa.desktopapp.ui.utils.ValidatorUtils;
import fr.sa.desktopapp.ui.validators.ExerciseValidator;
import fr.sa.desktopapp.ui.validators.QuestionValidator;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

import java.io.IOException;
import java.net.http.HttpClient;
import java.util.*;

public class QuestionController {

    private Exercise exercise;
    private List<Subject> leaderSubjects;
    private List<Question> questions;
    private ExerciseService exerciseService;
    private QuestionService questionService;
    private QuestionTypeService questionTypeService;
    private List<QuestionType> questionTypes;
    private QuestionType questionTypeCreate;
    private DialogUtils dialogUtils;
    private NodeUtils nodeUtils;
    private Router router;
    private QuestionValidator validator;
    private ExerciseValidator exerciseValidator;
    private ValidatorUtils validatorUtils;

    private VBox precedentView;

    @FXML
    private TextField nameInput;

    @FXML
    private VBox questionViewVbox;

    @FXML
    private TextField lifeInput;

    @FXML
    private TextField experienceInput;

    @FXML
    private MenuButton subjectMenu;

    @FXML
    private MenuButton chapterMenu;

    @FXML
    private MenuButton seqInput;

    @FXML
    private TextArea descriptionArea;

    @FXML
    private Button saveUpdateBtn;

    @FXML
    private Button createQuestionBtn;

    @FXML
    private TextArea wordingInput;

    @FXML
    private TextField timeInput;

    @FXML
    private MenuButton questionTypeMenu;

    @FXML
    private TextField damageInput;

    @FXML
    private HBox createQuestionHbox;

    @FXML
    private Button goBackBtn;
    @FXML
    public void goBack() {
        router.goTo(ExerciseController.class, controller -> controller.setRouter(router));
    }

    public void setRouter(Router router) {
        this.router = router;
    }

    @FXML
    private VBox questionList;

    public void setPrecedentView(VBox precedentView) {
        this.precedentView = precedentView;
    }

    public void setExercise(Exercise exercise) {
        this.exercise = exercise;
    }

    public void setLeaderSubjects(List<Subject> leaderSubjects) {
        this.leaderSubjects = leaderSubjects;
    }

    public void setExerciseService(ExerciseService exerciseService) {
        this.exerciseService = exerciseService;
    }

    @FXML
    public void initialize() {
        questionService = new QuestionService(new JsonParser(new ObjectMapper()),
                new SyncRequest(HttpClient.newHttpClient()));
        dialogUtils = new DialogUtils();
        nodeUtils = new NodeUtils();
        questionTypeService = new QuestionTypeService(new JsonParser(new ObjectMapper()),
                new SyncRequest(HttpClient.newHttpClient()));
        questionTypes = questionTypeService.findQuestionTypes();
        validatorUtils = new ValidatorUtils(dialogUtils);
        validator = new QuestionValidator(validatorUtils);
        exerciseValidator = new ExerciseValidator(validatorUtils);
    }

    public void setUp() {
        displayQuestions();
        setExerciseInputs();
        createQuestionTypesMenuCreate();
        displaySubjectMenu();
        displayChaptersMenu(exercise.getSubject().getParentSubject());
        chapterMenu.setText(exercise.getSubject().getTitle());
    }

    private void checkUpdateExercise() {
        if(exerciseValidator.isTitleValid(nameInput.getText())) {
            exercise.setName(nameInput.getText());
        }

        if(exerciseValidator.isDescriptionValid(descriptionArea.getText())) {
            exercise.setDescription(descriptionArea.getText());
        }

        if(exerciseValidator.isLifeValid(lifeInput.getText())) {
            exercise.setLife(Integer.parseInt(lifeInput.getText()));
        }

        if(exerciseValidator.isExperienceValid(experienceInput.getText())) {
            exercise.setExperience(Integer.parseInt(experienceInput.getText()));
        }
    }

    @FXML
    public void updateExercise() {
        checkUpdateExercise();

        if(exerciseService.updateExercise(exercise)) {
            dialogUtils.showInformationPopUp("Modifier un exercice", "Votre exercice a bien été modifié !");
            setExerciseInputs();
        } else {
            dialogUtils.showErrorPopUp("Modifier un exercice", "Votre exercice n'a pas pu être modifié", null);
        }
    }

    private void setExerciseInputs() {
        exercise = exerciseService.findOneExercise(exercise.getId());
        subjectMenu.setText(exercise.getSubject().getParentSubject().getTitle());
        displayChaptersMenu(exercise.getSubject().getParentSubject());
        chapterMenu.setText(exercise.getSubject().getTitle());
        nameInput.setText(exercise.getName());
        descriptionArea.setText(exercise.getDescription());
        lifeInput.setText(exercise.getLife().toString());
        experienceInput.setText(exercise.getExperience().toString());
    }

    private void displaySubjectMenu() {
        leaderSubjects.forEach(subject -> {
            if(subject.getParentSubject() == null && subject.getChildSubjects() != null && subject.getChildSubjects().length > 0) {
                subjectMenu.getItems().add(nodeUtils.createMenuItemWithAction(subject.getTitle(), actionEvent -> {
                    subjectMenu.setText(subject.getTitle());
                    displayChaptersMenu(subject);
                }));
            }
        });
    }

    private void displayChaptersMenu(Subject subject) {
        chapterMenu.getItems().clear();
        chapterMenu.setText("Aucun");
        for (Subject childSubject : subject.getChildSubjects()) {
            chapterMenu.getItems().add(nodeUtils.createMenuItemWithAction(childSubject.getTitle(), actionEvent -> {
                exercise.setSubject(childSubject);
                chapterMenu.setText(childSubject.getTitle());
            }));
        }
    }

    private void emptyQuestionList() {
        questionList.getChildren().clear();
    }

    public void displayQuestions() {
        emptyQuestionList();

        questions = exerciseService.findExerciseQuestions(exercise.getId());
        createQuestionsSequenceMenu();
        sortQuestionsBySequenceNumber();
        questions.forEach((this::displayOneQuestion));
    }

    private void sortQuestionsBySequenceNumber() {
        Comparator<Question> compareBySeq = Comparator.comparing(Question::getSequence_number);
        Collections.sort(questions, compareBySeq);
    }

    private int getSequenceNumber() {
        int number;
        try {
            number = Integer.parseInt(seqInput.getText().trim());
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return 0;
        }
        return number;
    }

    private List<String> checkCreateForm() {
        List<String> errors = new ArrayList<>();

        if(!validator.isWordingValid(wordingInput.getText())) {
            errors.add("L'énoncé doit être renseigné");
        }

        if(!validator.isDamageValid(damageInput.getText())) {
            errors.add("Les dégâts doivent être un nombre");
        }

        if(!validator.isTimeValid(timeInput.getText())) {
            errors.add("Le temps doit être supérieur à 0 seconde");
        }

        if(questionTypeCreate == null) {
            errors.add("Le type de la question doit être renseigné");
        }

        if(getSequenceNumber() <= 0 || getSequenceNumber() > (getMaxQuestionSequenceNumber() + 1)) {
            errors.add("L'ordre de la question n'est pas bon");
        }

        return errors;
    }

    private boolean updateAllSequenceNumbers() {
        int number = Integer.parseInt(seqInput.getText());
        int limit = number;
        if(number != (getMaxQuestionSequenceNumber() + 1)) {
            for (Question question : questions) {
                int sequenceNumber = question.getSequence_number();
                if (sequenceNumber == limit) {
                    limit = sequenceNumber + 1;
                    if(!incrementSequenceNumber(question, 1)) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    private boolean decrementAllSequences() {
        for (Question question : questions) {
            if(!incrementSequenceNumber(question, -1)) {
                return false;
            }
        }
        return true;
    }

    private boolean updateAllSequenceNumbersAfterDelete() {
        questions = exerciseService.findExerciseQuestions(exercise.getId());
        sortQuestionsBySequenceNumber();
        if(questions.get(0).getSequence_number() != 1) {
            if(!decrementAllSequences()) {
                return false;
            }
        }
        int previous = 1;
        for (Question question : questions) {
            if ((question.getSequence_number() - previous >= 2)) {
                if(!incrementSequenceNumber(question, -1)) {
                    return false;
                }
            }
            previous = question.getSequence_number();
        }
        return true;
    }

    private boolean incrementSequenceNumber(Question question, int value) {
        question.setSequence_number(question.getSequence_number() + value);
        return questionService.updateQuestion(question);
    }

    @FXML
    public void createQuestion() {
        if(!validatorUtils.showPotentialErrors("Ajouter une question",
                "Votre question n'a pas pu être ajoutée", checkCreateForm())) {
            return;
        }

        if(!updateAllSequenceNumbers()) {
            dialogUtils.showErrorPopUp("Ajouter une question", "Votre question n'a pas pu être ajoutée", "Problème dans les ordres de vos questions");
        }

        Question newQuestion = Question.builder()
                .wording(wordingInput.getText())
                .damage(Integer.parseInt(damageInput.getText()))
                .time(Integer.parseInt(timeInput.getText()))
                .sequence_number(Integer.parseInt(seqInput.getText()))
                .questionType(questionTypeCreate)
                .exercise(exercise)
                .build();


        if(questionService.createQuestion(newQuestion) != null) {
            dialogUtils.showInformationPopUp("Ajouter une question", "Votre question a bien été ajoutée !");
            displayQuestions();
        } else {
            dialogUtils.showErrorPopUp("Ajouter une question", "Votre question n'a pas pu être ajoutée", null);
        }
    }

    public void deleteQuestion(Question question) {
        if(questionService.deleteQuestion(question.getId())) {
            dialogUtils.showInformationPopUp("Supprimer une question",
                    "Votre question a bien été supprimée !");
            updateAllSequenceNumbersAfterDelete();
            displayQuestions();
        } else {
            dialogUtils.showErrorPopUp("Supprimer une question",
                    "Votre question n'a pas pu être supprimée",
                    null);
        }
    }

    public void updateQuestion(Question question) {
        if(questionService.updateQuestion(question)) {
            dialogUtils.showInformationPopUp("Modifier une question",
                    "Votre question a bien été mise à jour !");
            displayQuestions();
        } else {
            dialogUtils.showErrorPopUp("Modifier une question",
                    "Votre question n'a pas pu être mise à jour",
                    null);
        }
    }

    public void setAnswerView(Question question) {
        router.goTo(AnswerController.class, controller -> {
            controller.setRouter(router);
            controller.setPrecedentView(questionViewVbox);
            controller.setQuestion(question);
            controller.setQuestionService(questionService);
            controller.setQuestionTypeQCM(question.getQuestionType().getName().toUpperCase().equals("QCM"));
            controller.setup();
        });
    }

    private void createQuestionTypesMenuCreate() {
        questionTypes.forEach((questionType) -> {
            questionTypeMenu.getItems().add(nodeUtils.createMenuItemWithAction(questionType.getName(), actionEvent -> {
                questionTypeCreate = questionType;
                questionTypeMenu.setText(questionType.getName());
            }));
        });
    }

    private MenuButton createQuestionTypesMenuUpdate(Question question) {
        MenuButton menuButton = new MenuButton(question.getQuestionType().getName());

        questionTypes.forEach((questionType) -> {
            menuButton.getItems().add(nodeUtils.createMenuItemWithAction(questionType.getName(), actionEvent -> {
                question.setQuestionType(questionType);
                menuButton.setText(questionType.getName());
            }));
        });

        return menuButton;
    }

    private void createQuestionsSequenceMenu() {
        seqInput.getItems().clear();
        questions.forEach((q) -> {
            seqInput.getItems().add(nodeUtils.createMenuItemWithAction(q.getSequence_number().toString(), actionEvent -> {
                seqInput.setText(q.getSequence_number().toString());
            }));
        });
        int max = getMaxQuestionSequenceNumber() + 1;
        seqInput.getItems().add(nodeUtils.createMenuItemWithAction(String.valueOf(max), actionEvent -> {
            seqInput.setText(String.valueOf(max));
        }));
    }

    private Integer getMaxQuestionSequenceNumber() {
        Integer max = 0;

        try {
            return questions
                    .stream()
                    .max(Comparator.comparing(Question::getSequence_number))
                    .orElseThrow(NoSuchElementException::new)
                    .getSequence_number();
        } catch (NoSuchElementException e) {
            return 0;
        }
    }

    public void displayOneQuestion(Question question) {
        HBox hBox = new HBox();
        hBox.setAlignment(Pos.CENTER_LEFT);
        hBox.setPadding(new Insets(10, 0, 0, 0));

        TextField wordingField = nodeUtils.createTextFieldWithBinding(question.getWording(), ((obs, old, value) -> {
            if(validator.isWordingValid(value)) {
                question.setWording(value);
            }
        }));
        wordingField.setPrefWidth(280);
        hBox.getChildren().add(wordingField);

        TextField timeField = nodeUtils.createTextFieldWithBinding(question.getTime().toString(), ((obs, old, value) -> {
            if(validator.isTimeValid(value)) {
                question.setTime(Integer.parseInt(value));
            }
        }));
        timeField.setPrefWidth(70);
        HBox.setMargin(timeField, new Insets(0, 0, 0, 40));

        hBox.getChildren().add(timeField);

        TextField damageField = nodeUtils.createTextFieldWithBinding(question.getDamage().toString(), ((obs, old, value) -> {
            if(validator.isDamageValid(value)) {
                question.setDamage(Integer.parseInt(value));
            }
        }));

        damageField.setPrefWidth(70);
        HBox.setMargin(damageField, new Insets(0, 0, 0, 105));
        hBox.getChildren().add(damageField);
        MenuButton qtmMenu = createQuestionTypesMenuUpdate(question);

        HBox.setMargin(qtmMenu, new Insets(0, 90, 0, 50));
        hBox.getChildren().add(qtmMenu);

        hBox.getChildren().add(nodeUtils.createImageView("/images/hashtag.png", 16, 16));
        hBox.getChildren().add(new Label(question.getSequence_number().toString()));
        List<Pane> pane = nodeUtils.createPanes(1);
        hBox.getChildren().add(pane.get(0));
        HBox hBox1 = new HBox();
        hBox1.setSpacing(20);


        hBox1.getChildren().add(nodeUtils.createIconWithEventAndStyle("/images/check.png", mouseEvent -> updateQuestion(question)));
        hBox1.getChildren().add(nodeUtils.createIconWithEventAndStyle("/images/pencil.png", mouseEvent -> setAnswerView(question)));
        hBox1.getChildren().add(nodeUtils.createIconWithEventAndStyle("/images/trash.png", mouseEvent -> deleteQuestion(question)));
        hBox.getChildren().add(hBox1);

        questionList.getChildren().add(hBox);
    }
}
