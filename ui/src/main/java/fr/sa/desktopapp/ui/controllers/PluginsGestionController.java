package fr.sa.desktopapp.ui.controllers;

import fr.sa.desktop.core.services.PluginsGestionService;
import fr.sa.desktop.core.utils.FileUtils;
import fr.sa.desktopapp.ui.router.Router;
import fr.sa.desktopapp.ui.utils.NodeUtils;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class PluginsGestionController {

    private Router router;
    private PluginsGestionService pluginsGestionService;
    private NodeUtils nodeUtils;
    private FileUtils fileUtils;

    @FXML
    private VBox enabledPlugins;

    @FXML
    private VBox disabledPlugins;

    @FXML
    public void initialize() {
        fileUtils = new FileUtils();
        pluginsGestionService = new PluginsGestionService(fileUtils);
        nodeUtils = new NodeUtils();
        setPluginsList();
    }

    public void setRouter(Router router) {
        this.router = router;
    }

    public void setPluginsList() {
        enabledPlugins.getChildren().clear();
        disabledPlugins.getChildren().clear();

        List<String> enabledPluginStr = pluginsGestionService.findPluginsByState("enable");
        List<String> disabledPluginStr = pluginsGestionService.findPluginsByState("disable");
        setEnablePluginsList(enabledPluginStr);
        setDisablePluginsList(disabledPluginStr);
    }

    private void setEnablePluginsList(List<String> enabledPluginsStr) {
        enabledPluginsStr.forEach(enabledPluginStr -> {
            Label pluginName = new Label();
            pluginName.setText(pluginsGestionService.getPluginName(enabledPluginStr));
            ImageView disableIcon = nodeUtils.createIconWithEventAndStyle("/images/redCross.png", mouseEvent -> updatePlugin(enabledPluginStr, false));
            List<Pane> panes = nodeUtils.createPanes(1);
            HBox pluginBox = new HBox(pluginName, panes.get(0), disableIcon);
            pluginBox.setStyle("-fx-border-style: solid; -fx-border-color: black; -fx-border-width: 2px;");
            enabledPlugins.getChildren().add(pluginBox);
        });
    }

    private void setDisablePluginsList(List<String> disabledPluginsStr) {
        disabledPluginsStr.forEach(disabledPluginStr -> {
            Label pluginName = new Label();
            pluginName.setText(pluginsGestionService.getPluginName(disabledPluginStr));
            ImageView enableIcon = nodeUtils.createIconWithEventAndStyle("/images/check.png", mouseEvent -> updatePlugin(disabledPluginStr, true));
            List<Pane> panes = nodeUtils.createPanes(1);
            HBox pluginBox = new HBox(pluginName,panes.get(0), enableIcon);
            pluginBox.setStyle("-fx-border-style: solid; -fx-border-color: black; -fx-border-width: 2px;");
            disabledPlugins.getChildren().add(pluginBox);
        });
    }
    private void updatePlugin(String disablePlugin,Boolean enable) {
        Boolean isMoved = pluginsGestionService.movePlugin(disablePlugin, enable);
        setPluginsList();
    }

    @FXML
    private void addPlugin() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Ajouter un plugin");
        fileChooser.getExtensionFilters().add(//
                new FileChooser.ExtensionFilter("JAR Files", "*.jar"));
        File selectedJar = fileChooser.showOpenDialog(new Stage());
        if(selectedJar != null) {
            fileUtils.createPluginDirs();
            Path selectedJarPath = Paths.get(selectedJar.toURI());
            try {
                Files.move(selectedJarPath, Paths.get("plugin/enable/" + selectedJar.getName()));
                setPluginsList();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
