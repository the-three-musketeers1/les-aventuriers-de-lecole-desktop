package fr.sa.desktopapp.ui.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.sa.desktop.core.models.*;
import fr.sa.desktop.core.services.AdventureExerciseService;
import fr.sa.desktop.core.services.LeaderService;
import fr.sa.desktop.core.utils.parsers.JsonParser;
import fr.sa.desktop.core.utils.requests.SyncRequest;
import fr.sa.desktopapp.ui.router.Router;
import fr.sa.desktopapp.ui.utils.DialogUtils;
import fr.sa.desktopapp.ui.utils.NodeUtils;
import fr.sa.desktopapp.ui.utils.ValidatorUtils;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

import java.net.http.HttpClient;
import java.util.*;
import java.util.stream.Collectors;

import static java.lang.Integer.MAX_VALUE;

public class ExpeditionExerciseController {

    private NodeUtils nodeUtils;
    private DialogUtils dialogUtils;
    private ValidatorUtils validator;
    private AdventureExerciseService adventureExerciseService;
    private List<Adventure> leaderExpeditions;

    private List<Exercise> leaderExercises;
    private List<AdventureExercise> leaderExpeditionExercises;
    private int leaderId;
    private Adventure expeditionFilter;
    private Router router;
    private Adventure selectedExpedition;
    private List<Exercise> selectedExercises;

    @FXML
    private MenuButton expeditionMenu;

    @FXML
    private VBox exerciseList;

    @FXML
    private VBox expeditionList;

    @FXML
    private VBox expeditionExerciseList;
    private LeaderService leaderService;

    public void setRouter(Router router) {
        this.router = router;
    }

    public void setNodeUtils(NodeUtils nodeUtils) {
        this.nodeUtils = nodeUtils;
    }

    public void setLeaderExpeditions(List<Adventure> leaderExpeditions) {
        this.leaderExpeditions = leaderExpeditions;
    }

    @FXML
    public void initialize() {
        adventureExerciseService = new AdventureExerciseService(new JsonParser(new ObjectMapper()),
               new SyncRequest(HttpClient.newHttpClient()));
        dialogUtils = new DialogUtils();
        validator = new ValidatorUtils(dialogUtils);
        leaderService = new LeaderService(new JsonParser(new ObjectMapper()),
                new SyncRequest(HttpClient.newHttpClient()));
        leaderId = leaderService.getCurrentLeaderId();
        leaderExercises = leaderService.findLeaderExercises(leaderId);
        leaderExpeditionExercises = adventureExerciseService.findExpeditionExercisesByLeader(leaderId);
    }

    public void setUp() {
        leaderExpeditionExercises = adventureExerciseService.findExpeditionExercisesByLeader(leaderId);
        loadExpeditionsMenu();
        loadExpeditionExercises();
        selectedExpedition = null;
        selectedExercises = new ArrayList<>();
        exerciseList.getChildren().clear();
        displayExpeditionsToSelect();
    }

    private void displayExpeditionsToSelect() {
        expeditionList.getChildren().clear();
        for (Adventure expedition : leaderExpeditions) {
            expeditionList.getChildren().add(getSelectedExpedition(expedition));
        }
    }

    private void resetExpeditionsColor() {
        for (Node button : expeditionList.getChildren()) {
            button.setStyle(null);
        }
    }

    private Button getSelectedExpedition(Adventure expedition) {
        Button button = nodeUtils.createListActionButton(expedition.getTitle());

        button.setOnAction(actionEvent -> {
            if(selectedExpedition != null) {
                resetExpeditionsColor();
            }
            button.setStyle("-fx-background-color: #377aff;");
            selectedExpedition = expedition;
            loadUnassignedExercises(expedition);
        });
        return button;
    }

    private void loadUnassignedExercises(Adventure expedition) {
        exerciseList.getChildren().clear();
        selectedExercises = new ArrayList<>();
        List<Exercise> unassignedExercises = getUnassignedExercises(expedition);

        for (Exercise exercise : unassignedExercises) {
            Button button = nodeUtils.createListActionButton(exercise.getName());

            button.setOnAction(actionEvent -> {
                if(selectedExercises.contains(exercise)) {
                    selectedExercises.remove(exercise);
                    button.setStyle(null);
                } else {
                    selectedExercises.add(exercise);
                    button.setStyle("-fx-background-color: #377aff;");
                }
            });
            exerciseList.getChildren().add(button);
        }
    }

    private List<String> createValidator() {
        List<String> errors = new ArrayList<>();

        if(selectedExpedition == null) {
            errors.add("Veuillez sélectionner une expédition");
        }

        if(selectedExercises == null || selectedExercises.size() < 1) {
            errors.add("Veuillez sélectionner au moins un exercice");
        }

        return errors;
    }

    @FXML
    private void assignExercisesToExpedition() {
        if(!validator.showPotentialErrors("Assignation",
                "L'assignation n'est pas possible", createValidator())) {
            return;
        }

        for (Exercise selectedExercise : selectedExercises) {
            createExpeditionExercise(selectedExercise);
        }

        resetFilters();
    }

    private void createExpeditionExercise(Exercise exercise) {
        AdventureExercise expeditionExercise = AdventureExercise
                .builder()
                .adventure(selectedExpedition)
                .exercise(exercise)
                .exerciseId(exercise.getId())
                .adventureId(selectedExpedition.getId())
                .build();

        if(!adventureExerciseService.createExpeditionExercise(expeditionExercise)) {
            dialogUtils.showErrorPopUp("Assignation",
                    String.format("Votre assignation entre l'expédition %s et l'exercice %s a échouée",
                            selectedExpedition.getTitle(), exercise.getName()),
                    null);
        } else {
            dialogUtils.showInformationPopUp("Assignation",
                    String.format("Votre assignation entre l'expédition %s et l'exercice %s s'est bien passée !",
                            selectedExpedition.getTitle(), exercise.getName()));
        }
    }

    private List<Exercise> getUnassignedExercises(Adventure expedition) {
        Set<Integer> assignedExerciseIds = getAssignedExercises(expedition)
                .stream()
                .map(Exercise::getId)
                .collect(Collectors.toSet());

        return leaderExercises
                .stream()
                .filter(exercise -> !assignedExerciseIds.contains(exercise.getId()))
                .collect(Collectors.toList());
    }

    private List<Exercise> getAssignedExercises(Adventure expedition) {
        return leaderExpeditionExercises
                    .stream()
                    .filter(expeditionExercise -> Objects.equals(expeditionExercise.getAdventure().getId(), expedition.getId()))
                    .map(AdventureExercise::getExercise)
                    .collect(Collectors.toList());
    }

    @FXML
    private void resetFilters() {
        expeditionMenu.setText("Aucune");
        expeditionFilter = null;
        setUp();
    }

    private void loadExpeditionExercises() {
        expeditionExerciseList.getChildren().clear();
        List<AdventureExercise> expeditionExercises = getExpeditionExercisesWithFilters();
        for (AdventureExercise expeditionExercise : expeditionExercises) {
            displayOneExpeditionExercise(expeditionExercise);
        }
    }

    private void displayOneExpeditionExercise(AdventureExercise expeditionExercise) {
        HBox hBox = new HBox();
        hBox.setAlignment(Pos.CENTER_LEFT);
        hBox.setSpacing(10);
        hBox.setPadding(new Insets(10, 10, 10, 10));
        Label exp = new Label(expeditionExercise.getAdventure().getTitle());
        exp.setPrefWidth(300);

        Label exo = new Label(expeditionExercise.getExercise().getName());
        exo.setPrefWidth(300);

        hBox.getChildren().add(nodeUtils.createImageView("/images/map.png", 32, 32));
        hBox.getChildren().add(exp);

        hBox.getChildren().add(nodeUtils.createImageView("/images/beast.png", 32, 32));
        hBox.getChildren().add(exo);

        hBox.getChildren().add(nodeUtils.createPanes(1).get(0));
        hBox.getChildren().add(nodeUtils.createIconWithEventAndStyle("/images/trash.png", actionEvent -> deleteExpeditionExercise(expeditionExercise)));
        expeditionExerciseList.getChildren().add(hBox);
    }

    private void deleteExpeditionExercise(AdventureExercise expeditionExercise) {
        String expeditionName = expeditionExercise.getAdventure().getTitle();
        String exerciseName = expeditionExercise.getExercise().getName();
        Alert confirmationDialog = dialogUtils.showConfirmationPopUp("Supprimer un lien",
                String.format("Voulez-vous vraiment supprimer le lien entre l'exercice %s et l'expédition %s ?", exerciseName, expeditionName));

        Optional<ButtonType> result = confirmationDialog.showAndWait();
        if (result.isPresent() && result.get() == ButtonType.OK){
            if(!adventureExerciseService.deleteExpeditionExercise(expeditionExercise.getId())) {
                dialogUtils.showErrorPopUp("Suppression", "La suppression a échouée", null);
            } else {
                dialogUtils.showInformationPopUp("Suppression",
                        String.format("Le lien entre l'exercice %s et l'expédition %s a bien été supprimé",
                                exerciseName, expeditionName));
                setUp();
            }
        }
    }

    private List<AdventureExercise> getExpeditionExercisesWithFilters() {
        List<AdventureExercise> expeditionExercises = leaderExpeditionExercises;

        if(expeditionFilter != null) {
            expeditionExercises = expeditionExercises.stream()
                    .filter(ge -> Objects.equals(ge.getAdventure().getId(), expeditionFilter.getId()))
                    .collect(Collectors.toList());
        }

        return expeditionExercises;
    }

    private void loadExpeditionsMenu() {
        expeditionMenu.getItems().clear();
        leaderExpeditions.forEach(expedition -> {
            expeditionMenu.getItems().add(nodeUtils.createMenuItemWithAction(
                    expedition.getTitle(),
                    actionEvent -> {
                        expeditionFilter = expedition;
                        expeditionMenu.setText(expedition.getTitle());
                        loadExpeditionExercises();
                    }
            ));
        });
    }
}
