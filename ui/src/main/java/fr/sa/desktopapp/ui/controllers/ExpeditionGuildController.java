package fr.sa.desktopapp.ui.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.sa.desktop.core.models.Adventure;
import fr.sa.desktop.core.models.Guild;
import fr.sa.desktop.core.models.GuildAdventure;
import fr.sa.desktop.core.services.GuildAdventureService;
import fr.sa.desktop.core.services.GuildService;
import fr.sa.desktop.core.services.LeaderService;
import fr.sa.desktop.core.services.QuestionService;
import fr.sa.desktop.core.utils.parsers.JsonParser;
import fr.sa.desktop.core.utils.requests.SyncRequest;
import fr.sa.desktopapp.ui.router.Router;
import fr.sa.desktopapp.ui.utils.DialogUtils;
import fr.sa.desktopapp.ui.utils.NodeUtils;
import fr.sa.desktopapp.ui.utils.ValidatorUtils;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import java.net.http.HttpClient;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

import static java.lang.Integer.MAX_VALUE;

public class ExpeditionGuildController {

    private GuildAdventureService guildAdventureService;
    private GuildService guildService;
    private NodeUtils nodeUtils;
    private DialogUtils dialogUtils;
    private ValidatorUtils validator;
    private LeaderService leaderService;

    private List<Adventure> leaderExpeditions;
    private int leaderId;
    private List<Guild> leaderGuilds;
    private List<GuildAdventure> leaderGuildExpeditions;

    private Guild guildFilter;
    private Adventure expeditionFilter;
    private Router router;
    private Guild selectedGuild;
    private List<Adventure> selectedExpeditions;

    @FXML
    private MenuButton guildMenu;

    @FXML
    private MenuButton expeditionMenu;

    @FXML
    private VBox guildExpeditionList;

    @FXML
    private VBox guildList;

    @FXML
    private VBox expeditionList;

    @FXML
    private DatePicker deadlineInput;

    @FXML
    private MenuButton accessibleMenu;

    public void setRouter(Router router) {
        this.router = router;
    }

    public void setNodeUtils(NodeUtils nodeUtils) {
        this.nodeUtils = nodeUtils;
    }

    public void setLeaderExpeditions(List<Adventure> leaderExpeditions) {
        this.leaderExpeditions = leaderExpeditions;
    }

    @FXML
    public void initialize() {
        guildAdventureService = new GuildAdventureService(new JsonParser(new ObjectMapper()),
               new SyncRequest(HttpClient.newHttpClient()));
        guildService = new GuildService(new JsonParser(new ObjectMapper()),
                new SyncRequest(HttpClient.newHttpClient()));
        dialogUtils = new DialogUtils();
        validator = new ValidatorUtils(dialogUtils);
        leaderService = new LeaderService(new JsonParser(new ObjectMapper()),
                new SyncRequest(HttpClient.newHttpClient()));
        leaderId = leaderService.getCurrentLeaderId();
        leaderGuilds = guildService.getGuildsByLeaderId(this.leaderId);
        leaderGuildExpeditions = guildAdventureService.findGuildExpeditionsByLeader(this.leaderId);
    }

    public void setUp() {
        leaderGuildExpeditions = guildAdventureService.findGuildExpeditionsByLeader(this.leaderId);
        loadGuildsMenu();
        loadExpeditionMenu();
        loadGuildExpeditions();
        selectedGuild = null;
        selectedExpeditions = new ArrayList<>();
        expeditionList.getChildren().clear();
        displayGuildsToSelect();
    }

    private void displayGuildsToSelect() {
        guildList.getChildren().clear();
        for (Guild guild : leaderGuilds) {
            guildList.getChildren().add(getSelectedGuild(guild));
        }
    }

    private void resetGuildsColor() {
        for (Node button : guildList.getChildren()) {
            button.setStyle(null);
        }
    }

    private Button getSelectedGuild(Guild guild) {
        Button button = nodeUtils.createListActionButton(guild.getName());
        button.setOnAction(actionEvent -> {
            if(selectedGuild != null) {
                resetGuildsColor();
            }
            button.setStyle("-fx-background-color: #377aff;");
            selectedGuild = guild;
            loadUnassignedExpeditions(guild);
        });
        return button;
    }

    private void loadUnassignedExpeditions(Guild guild) {
        expeditionList.getChildren().clear();
        selectedExpeditions = new ArrayList<>();
        List<Adventure> unassignedExpeditions = getUnassignedExpeditions(guild);

        for (Adventure expedition : unassignedExpeditions) {
            Button button = nodeUtils.createListActionButton(expedition.getTitle());
            button.setOnAction(actionEvent -> {
                if(selectedExpeditions.contains(expedition)) {
                    selectedExpeditions.remove(expedition);
                    button.setStyle(null);
                } else {
                    selectedExpeditions.add(expedition);
                    button.setStyle("-fx-background-color: #377aff;");
                }
            });

            expeditionList.getChildren().add(button);
        }
    }

    private List<String> createValidator() {
        List<String> errors = new ArrayList<>();

        if(selectedGuild == null) {
            errors.add("Veuillez sélectionner une guilde");
        }

        if(selectedExpeditions == null || selectedExpeditions.size() < 1) {
            errors.add("Veuillez sélectionner au moins une expédition");
        }

        if(deadlineInput.getValue() == null || !deadlineInput.getValue().isAfter(LocalDate.now())) {
            errors.add("Veuillez sélectionner une date de rendu postérieure à aujourd'hui");
        }

        return errors;
    }

    @FXML
    private void assignExpeditionsToGuild() {
        if(!validator.showPotentialErrors("Assignation",
                "L'assignation n'est pas possible", createValidator())) {
            return;
        }

        for (Adventure selectedExpedition : selectedExpeditions) {
            createGuildExpedition(selectedExpedition);
        }

        resetFilters();
    }

    private void createGuildExpedition(Adventure expedition) {
        GuildAdventure guildExpedition = GuildAdventure
                .builder()
                .deadline(Date.from(deadlineInput.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant()))
                .accessible(Objects.equals(accessibleMenu.getText(), "Oui"))
                .adventure(expedition)
                .guild(selectedGuild)
                .adventureId(expedition.getId())
                .guildId(selectedGuild.getId())
                .build();

        if(!guildAdventureService.createGuildExpedition(guildExpedition)) {
            dialogUtils.showErrorPopUp("Assignation",
                    String.format("Votre assignation entre l'expédition %s et la guilde %s a échouée",
                            expedition.getTitle(), selectedGuild.getName()),
                    null);
        } else {
            dialogUtils.showInformationPopUp("Assignation",
                    String.format("Votre assignation entre l'expédition %s et la guilde %s s'est bien passée !",
                            expedition.getTitle(), selectedGuild.getName()));
        }
    }

    private List<Adventure> getUnassignedExpeditions(Guild guild) {
        Set<Integer> assignedExpeditionIds = getAssignedExpeditions(guild)
                .stream()
                .map(Adventure::getId)
                .collect(Collectors.toSet());

        return leaderExpeditions
                .stream()
                .filter(expedition -> !assignedExpeditionIds.contains(expedition.getId()))
                .collect(Collectors.toList());
    }

    private List<Adventure> getAssignedExpeditions(Guild guild) {
        return leaderGuildExpeditions
                    .stream()
                    .filter(guildExpedition -> Objects.equals(guildExpedition.getGuild().getId(), guild.getId()))
                    .map(GuildAdventure::getAdventure)
                    .collect(Collectors.toList());
    }

    @FXML
    private void resetFilters() {
        guildMenu.setText("Aucune");
        expeditionMenu.setText("Aucune");
        guildFilter = null;
        expeditionFilter = null;
        setUp();
    }

    private void loadGuildExpeditions() {
        guildExpeditionList.getChildren().clear();
        List<GuildAdventure> guildExpeditions = getGuildExpeditionsWithFilters();
        for (GuildAdventure guildExpedition : guildExpeditions) {
            displayOneGuildExpedition(guildExpedition);
        }
    }

    private void displayOneGuildExpedition(GuildAdventure guildExpedition) {
        HBox hBox = new HBox();
        hBox.setAlignment(Pos.CENTER_LEFT);
        hBox.setSpacing(10);
        hBox.setPadding(new Insets(10, 10, 10, 10));

        LocalDate deadline = guildExpedition.getDeadline().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

        DatePicker datePicker = new DatePicker(deadline);

        datePicker.valueProperty().addListener((obs, old, value) ->
                guildExpedition.setDeadline(Date.from(value.atStartOfDay(ZoneId.systemDefault()).toInstant())));
        datePicker.setPrefWidth(200);
        datePicker.setStyle(null);
        Label guildLabel = new Label(guildExpedition.getGuild().getName());
        guildLabel.setPrefWidth(200);

        Label titleLabel = new Label(guildExpedition.getAdventure().getTitle());
        titleLabel.setPrefWidth(200);

        hBox.getChildren().add(guildLabel);
        hBox.getChildren().add(titleLabel);

        hBox.getChildren().add(datePicker);
        hBox.getChildren().add(createAccessibleMenu(guildExpedition));
        hBox.getChildren().add(nodeUtils.createPanes(1).get(0));
        hBox.getChildren().add(nodeUtils.createIconWithEventAndStyle("/images/check.png", actionEvent -> updateGuildExpedition(guildExpedition)));
        hBox.getChildren().add(nodeUtils.createIconWithEventAndStyle("/images/trash.png", actionEvent -> deleteGuildExpedition(guildExpedition)));
        hBox.setPadding(new Insets(0, 0, 10, 0));
        guildExpeditionList.getChildren().add(hBox);
    }

    private void deleteGuildExpedition(GuildAdventure guildExpedition) {
        String guildName = guildExpedition.getGuild().getName();
        String expeditionName = guildExpedition.getAdventure().getTitle();
        Alert confirmationDialog = dialogUtils.showConfirmationPopUp("Supprimer un lien",
                String.format("Voulez-vous vraiment supprimer le lien entre la guilde %s et l'expédition %s ?", guildName, expeditionName));

        Optional<ButtonType> result = confirmationDialog.showAndWait();
        if (result.isPresent() && result.get() == ButtonType.OK){
            if(!guildAdventureService.deleteGuildExpedition(guildExpedition.getId())) {
                dialogUtils.showErrorPopUp("Suppression", "La suppression a échouée", null);
            } else {
                dialogUtils.showInformationPopUp("Suppression",
                        String.format("Le lien entre la guilde %s et l'expédition %s a bien été supprimé",
                                guildName, expeditionName));
                setUp();
            }
        }
    }

    private void updateGuildExpedition(GuildAdventure guildExpedition) {
        if(guildAdventureService.updateGuildExpedition(guildExpedition)) {
            dialogUtils.showInformationPopUp("Modifier un lien",
                    "Votre lien a bien été mis à jour !");
            resetFilters();
        } else {
            dialogUtils.showErrorPopUp("Modifier un lien",
                    "Votre lien n'a pas pu être mis à jour",
                    null);
        }
    }

    private MenuButton createAccessibleMenu(GuildAdventure guildExpedition) {
        final String isAccessible = guildExpedition.getAccessible() ? "Oui" : "Non";
        MenuButton menu = new MenuButton(isAccessible);
        menu.getItems().addAll(
                nodeUtils.createMenuItemWithAction("Oui", actionEvent -> {
                    guildExpedition.setAccessible(true);
                    menu.setText("Oui");
                }),
                nodeUtils.createMenuItemWithAction("Non", actionEvent -> {
                    guildExpedition.setAccessible(false);
                    menu.setText("Non");
                })
        );
        menu.setPrefWidth(200);
        return menu;
    }

    private List<GuildAdventure> getGuildExpeditionsWithFilters() {
        List<GuildAdventure> guildExpeditions = leaderGuildExpeditions;

        if(guildFilter != null) {
            guildExpeditions = guildExpeditions.stream()
                    .filter(ge -> Objects.equals(ge.getGuild().getId(), guildFilter.getId()))
                    .collect(Collectors.toList());
        }

        if(expeditionFilter != null) {
            guildExpeditions = guildExpeditions.stream()
                    .filter(ge -> Objects.equals(ge.getAdventure().getId(), expeditionFilter.getId()))
                    .collect(Collectors.toList());
        }

        return guildExpeditions;
    }

    private void loadGuildsMenu() {
        guildMenu.getItems().clear();
        leaderGuilds.forEach(guild -> {
            guildMenu.getItems().add(nodeUtils.createMenuItemWithAction(
                    guild.getName(),
                    actionEvent -> {
                        guildFilter = guild;
                        guildMenu.setText(guild.getName());
                        loadGuildExpeditions();
                    }
            ));
        });
    }

    private void loadExpeditionMenu() {
        expeditionMenu.getItems().clear();
        leaderExpeditions.forEach(expedition -> {
            expeditionMenu.getItems().add(nodeUtils.createMenuItemWithAction(
                    expedition.getTitle(),
                    actionEvent -> {
                        expeditionFilter = expedition;
                        expeditionMenu.setText(expedition.getTitle());
                        loadGuildExpeditions();
                    }
            ));
        });
    }

    @FXML
    private void setAccessibleToTrue() {
        accessibleMenu.setText("Oui");
    }

    @FXML
    private void setAccessibleToFalse() {
        accessibleMenu.setText("Non");
    }
}
