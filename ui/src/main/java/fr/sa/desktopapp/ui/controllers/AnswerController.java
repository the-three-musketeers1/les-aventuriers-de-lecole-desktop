package fr.sa.desktopapp.ui.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.sa.desktop.core.models.Answer;
import fr.sa.desktop.core.models.Question;
import fr.sa.desktop.core.services.AnswerService;
import fr.sa.desktop.core.services.QuestionService;
import fr.sa.desktop.core.utils.parsers.JsonParser;
import fr.sa.desktop.core.utils.requests.SyncRequest;
import fr.sa.desktopapp.ui.router.Router;
import fr.sa.desktopapp.ui.utils.DialogUtils;
import fr.sa.desktopapp.ui.utils.NodeUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import org.apache.commons.lang3.StringUtils;

import java.net.http.HttpClient;
import java.util.List;
import java.util.Optional;

public class AnswerController {

    private VBox precedentView;
    private Question question;
    private Boolean correct = false;
    private AnswerService answerService;
    private QuestionService questionService;
    private DialogUtils dialogUtils;
    private NodeUtils nodeUtils;
    private List<Answer> answers;
    private Router router;

    @FXML
    private Button addAnswerBtn;

    @FXML
    private TextArea contentInput;

    @FXML
    private MenuButton correctMenu;

    @FXML
    private MenuItem correctAnswer;

    @FXML
    private MenuItem incorrectAnswer;

    @FXML
    private Button goBackBtn;

    @FXML
    private Label correctLabel;

    @FXML
    private VBox answerList;

    public boolean isQuestionTypeQCM;

    public void setQuestionTypeQCM(boolean questionTypeQCM) {
        isQuestionTypeQCM = questionTypeQCM;
    }

    private Boolean isCorrect() {
        return isQuestionTypeQCM ? correct : true;
    }

    public void setQuestionService(QuestionService questionService) {
        this.questionService = questionService;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public void setRouter(Router router) {
        this.router = router;
    }

    @FXML
    public void goBack() {
        router.getMainWindow().setCenter(precedentView);
    }

    @FXML
    public void setCorrectTrue() {
        this.correct = true;
        correctMenu.setText("Oui");
    }

    @FXML
    public void setCorrectFalse() {
        this.correct = false;
        correctMenu.setText("Non");
    }

    public void setPrecedentView(VBox precedentView) {
        this.precedentView = precedentView;
    }

    @FXML
    public void initialize() {
        answerService = new AnswerService(new JsonParser(new ObjectMapper()),
               new SyncRequest(HttpClient.newHttpClient()));
        dialogUtils = new DialogUtils();
        nodeUtils = new NodeUtils();
    }

    public void setup() {
        displayAnswers();
        if(!isQuestionTypeQCM) {
            hideCorrectMenu();
        }
    }

    private void hideCorrectMenu() {
        correctLabel.setVisible(false);
        correctMenu.setVisible(false);
    }

    @FXML
    public void addAnswer() {
        if(StringUtils.isEmpty(contentInput.getText()) || contentInput.getText().length() > 255) {
            dialogUtils.showErrorPopUp("Ajouter une réponse", "Votre réponse n'a pas pu être ajoutée",
                    "Le contenu de la réponse doit être renseigné et ne pas dépasser 255 caractères");
            return;
        }

        Answer answer = Answer.builder()
                .content(contentInput.getText())
                .correct(isCorrect())
                .question(question)
                .build();

        if(answerService.createAnswer(answer) != null) {
            dialogUtils.showInformationPopUp("Ajouter une réponse", "Votre réponse a bien été ajoutée !");
            displayAnswers();
        } else {
            dialogUtils.showErrorPopUp("Ajouter une réponse", "Votre réponse n'a pas pu être ajoutée", null);
        }
    }

    private void displayAnswers() {
        emptyAnswerList();
        answers = questionService.findQuestionAnswers(question.getId());
        answers.forEach(this::displayOneAnswer);
    }

    private void displayOneAnswer(Answer answer) {
        HBox hBox = new HBox();
        hBox.setAlignment(Pos.CENTER_LEFT);
        hBox.setSpacing(10);
        hBox.setPadding(new Insets(10, 10, 10, 10));
        TextField answerContent = new TextField(answer.getContent());
        List<Pane> panes = nodeUtils.createPanes(1);
        answerContent.textProperty().addListener((observableValue, oldValue, value) -> {
            if(!StringUtils.isEmpty(value) && value.length() <= 255) {
                answer.setContent(value);
            }
        });
        HBox.setMargin(answerContent, new Insets(10, 150, 0, 0));

        hBox.getChildren().add(answerContent);

        if(isQuestionTypeQCM) {
            if(answer.isCorrect()) {
                hBox.getChildren().add(nodeUtils.createImageView("/images/sword.png", 32, 32));
            } else {
                hBox.getChildren().add(nodeUtils.createImageView("/images/potion.png", 32, 32));
            }
            hBox.getChildren().add(createAnswerCorrectMenu(answer));
        }

        hBox.getChildren().add(panes.get(0));
        hBox.getChildren().add(nodeUtils.createIconWithEventAndStyle("/images/check.png", mouseEvent -> updateAnswer(answer)));
        hBox.getChildren().add(nodeUtils.createIconWithEventAndStyle("/images/trash.png", mouseEvent -> deleteAnswer(answer)));
        answerList.getChildren().add(hBox);
    }

    private void emptyAnswerList() {
        answerList.getChildren().clear();
    }

    private void updateAnswer(Answer answer){
        if(answerService.updateAnswer(answer)) {
            dialogUtils.showInformationPopUp("Modifier une réponse", "Votre réponse a bien été modifiée !");
            displayAnswers();
        } else {
            dialogUtils.showErrorPopUp("Modifier une réponse", "Votre réponse n'a pas pu être modifiée", null);
        }
    }

    private void deleteAnswer(Answer answer){
        Alert confirmationDialog = dialogUtils.showConfirmationPopUp("Supprimer une réponse",
                String.format("Voulez-vous vraiment supprimer la réponse %s ?", answer.getContent()));

        Optional<ButtonType> result = confirmationDialog.showAndWait();
        if (result.isPresent() && result.get() == ButtonType.OK){
            if(answerService.deleteAnswer(answer.getId())) {
                dialogUtils.showInformationPopUp("Supprimer une réponse", "Votre réponse a bien été supprimée !");
                displayAnswers();
            } else {
                dialogUtils.showErrorPopUp("Supprimer une réponse", "Votre réponse n'a pas pu être supprimée", null);
            }
        }

    }

    private MenuButton createAnswerCorrectMenu(Answer answer) {
        String isCorrect = answer.isCorrect() ? "Oui" : "Non";
        MenuButton menuButton = new MenuButton(isCorrect);

        menuButton.getItems().addAll(nodeUtils.createMenuItemWithAction("Oui", actionEvent -> {
            answer.setCorrect(true);
            menuButton.setText("Oui");
        }), nodeUtils.createMenuItemWithAction("Non", actionEvent -> {
            answer.setCorrect(false);
            menuButton.setText("Non");
        }));

        return menuButton;
    }

}
