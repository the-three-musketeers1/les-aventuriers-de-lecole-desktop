package fr.sa.desktopapp.ui.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.sa.desktop.core.models.Leader;
import fr.sa.desktop.core.services.AuthService;
import fr.sa.desktop.core.utils.parsers.JsonParser;
import fr.sa.desktop.core.utils.requests.SyncRequest;
import fr.sa.desktopapp.ui.router.StageRouter;
import fr.sa.desktopapp.ui.utils.DialogUtils;
import fr.sa.desktopapp.ui.utils.ValidatorUtils;
import fr.sa.desktopapp.ui.validators.UserValidator;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import org.apache.commons.validator.routines.EmailValidator;

import java.io.FileWriter;
import java.io.IOException;
import java.net.http.HttpClient;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.List;

public class LoginController {

    private StageRouter stageRouter;
    private ValidatorUtils validatorUtils;
    private UserValidator userValidator;
    private AuthService authService;
    private DialogUtils dialogUtils;

    @FXML
    private TextField emailField;

    @FXML
    private PasswordField passwordField;

    @FXML
    private BorderPane loginMainWindow;

    @FXML
    private Button loginBtn;

    @FXML
    public void initialize() {
        this.validatorUtils = new ValidatorUtils(new DialogUtils());
        this.userValidator = new UserValidator(validatorUtils, EmailValidator.getInstance());
        this.authService = new AuthService(new JsonParser(new ObjectMapper()),
                new SyncRequest(HttpClient.newHttpClient(), false));
        this.dialogUtils = new DialogUtils();
    }

    private List<String> checkLoginForm() {
        List<String> errors = new ArrayList<>();
        if (!userValidator.isEmailValid(emailField)) {
            errors.add("Le champ 'email' n'est pas valide");
        }
        if (!userValidator.isPasswordValid(passwordField)) {
            errors.add("Le champ 'password' doit être renseigné");
        }

        return errors;
    }

    @FXML
    private void onLogin(MouseEvent mouseEvent) {
        if (!this.validatorUtils.showPotentialErrors("Connexion",
                "La connexion n'a pas pu être établie", checkLoginForm())) {
            return;
        }
        final var leader = Leader.builder()
                .email(this.emailField.getText())
                .password(this.passwordField.getText())
                .build();
        final var response = this.authService.login(leader);
        if (response == null || response.statusCode() != 201) {
            assert response != null;
            displayErrorByResponse(response);
            return;
        }
        this.saveSessionInFile(response);
        this.stageRouter.goTo(MainController.class, mainController -> {
            mainController.setStageRouter(this.stageRouter);
        });
    }

    private void saveSessionInFile(HttpResponse<String> response) {
        try (final var tokenFileWriter = new FileWriter(AuthService.TOKEN_FILE_PATH)) {
            this.authService.saveSession(response.body(), tokenFileWriter);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void displayErrorByResponse(HttpResponse<String> response) {
        if (response.statusCode() == 403) {
            this.dialogUtils.showErrorPopUp("Connexion", "Problème de connexion dû à une transmission de données non conformes.", "");
        } else if (response.statusCode() == 404) {
            this.dialogUtils.showErrorPopUp("Connexion", "Vous n'êtes pas encore inscrit. Veuillez d'abord vous inscrire avant de vous connecter", "");
        } else {
            this.dialogUtils.showErrorPopUp("Connexion", "Problème de connexion, veuillez réessayer plus tard.", "");
        }
    }

    public void setStageRouter(final StageRouter stageRouter) {
        this.stageRouter = stageRouter;
    }

    public void onSubscribeRoute(MouseEvent mouseEvent) {
        this.stageRouter.goTo(SubscribeController.class, subscribeController -> {
            subscribeController.setStageRouter(this.stageRouter);
        });
    }
}
