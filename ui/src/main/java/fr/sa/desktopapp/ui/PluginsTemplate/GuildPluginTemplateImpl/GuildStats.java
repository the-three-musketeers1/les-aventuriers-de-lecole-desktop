package fr.sa.desktopapp.ui.PluginsTemplate.GuildPluginTemplateImpl;

import fr.sa.desktop.core.plugin.GuildPluginTemplate;
import fr.sa.desktop.core.plugin.models.Chapter;
import fr.sa.desktop.core.plugin.models.ExerciseInformation;
import fr.sa.desktop.core.plugin.models.Discipline;
import fr.sa.desktop.core.plugin.models.StatisticsExercise;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import java.util.List;
import java.util.stream.Collectors;

public class GuildStats implements GuildPluginTemplate {

    private PieChart chart;

    private HBox disciplineBox;
    private HBox chapterBox;
    private HBox exerciseBox;
    private ComboBox<String> disciplineComboBox;
    private ComboBox<String> chapterComboBox;
    private ComboBox<String> exerciseComboBox;

    private String selectedDiscipline;
    private String selectedChapter;
    private String selectedExercise;
    private List<StatisticsExercise> statisticsExercises;
    private List<StatisticsExercise> filteredStatisticsExercises;
    private List<ExerciseInformation> exercises;
    private List<Discipline> subjects;

    @Override
    public String getButtonName() {
        return "Statistique donuts";
    }

    @Override
    public String getTitle() {
        return "Statistique de la guilde";
    }

    @Override
    public Node getMainPanel(List<StatisticsExercise> statisticsExerciseList) {
        this.statisticsExercises = statisticsExerciseList;
        this.filteredStatisticsExercises = statisticsExerciseList;
        ObservableList<PieChart.Data> pieChartData =
                FXCollections.observableArrayList(
                        new PieChart.Data("Bonne réponse", statisticsExerciseList.stream().mapToInt(statisticsExercise -> statisticsExercise.goodAnswers).sum()),
                        new PieChart.Data("Mauvaise Réponse", statisticsExerciseList.stream().mapToInt(statisticsExercise -> statisticsExercise.badAnswers).sum()));
        this.chart = new PieChart(pieChartData);
        chart.setTitle("Statistiques Générale de la Guilde");
        return chart;
    }

    @Override
    public Node getSecondaryPanel(List<Discipline> subjects, List<ExerciseInformation> exercises) {
        VBox vBox = new VBox();
        this.exercises = exercises;
        this.subjects = subjects;
        createDisciplineLine(subjects.stream().map(Discipline::getTitle).collect(Collectors.toList()), vBox);
        createChapterLine(subjects.get(0).getChildSubjects().stream().map(Chapter::getTitle).collect(Collectors.toList()), vBox);
        createExerciseLine(exercises.stream().map(ExerciseInformation::getName).collect(Collectors.toList()), vBox);
        return vBox;
    }
    private void createDisciplineLine(List<String> strings, VBox vBox) {
        ObservableList<String> disciplineOptions =
                FXCollections.observableArrayList(strings);
        disciplineComboBox = new ComboBox<String>(disciplineOptions);
        disciplineComboBox.getSelectionModel().selectedItemProperty().addListener((options, oldValue, newValue) -> {
            this.selectedDiscipline = newValue;
            updateChart();

            ObservableList<String> chapterDatas =
                    FXCollections.observableArrayList(this.subjects.stream()
                            .filter(subject-> subject.getTitle().equals(newValue))
                            .map(Discipline::getChildSubjects)
                            .flatMap(List::stream)
                            .map(Chapter::getTitle).collect(Collectors.toList()));
            chapterComboBox.getItems().clear();
            chapterComboBox.getItems().addAll(chapterDatas);
            chapterBox.setDisable(false);

        });
        final Button button = new Button("X");
        button.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                cleanData("discipline");
            }
        });
        disciplineBox = new HBox();

        disciplineBox.getChildren().add(new Label("Matiere :"));
        disciplineBox.getChildren().add(disciplineComboBox);
        disciplineBox.getChildren().add(button);
        vBox.getChildren().add(disciplineBox);
    }

    private void createChapterLine(List<String> strings, VBox vBox) {
        ObservableList<String> chapterOptions =
                FXCollections.observableArrayList(strings);
        chapterComboBox = new ComboBox<String>(chapterOptions);
        chapterComboBox.getSelectionModel().selectedItemProperty().addListener((options, oldValue, newValue) -> {
            this.selectedChapter = newValue;
            updateChart();
            ObservableList<String> exerciseDatas =
                    FXCollections.observableArrayList(this.exercises.stream()
                            .filter(exercise-> exercise.getChapter().equals(newValue))
                            .map(ExerciseInformation::getName).collect(Collectors.toList()));
            exerciseComboBox.getItems().clear();
            exerciseComboBox.getItems().addAll(exerciseDatas);
            exerciseBox.setDisable(false);
        });
        chapterBox = new HBox();
        final Button button = new Button("X");
        button.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                cleanData("chapter");
            }
        });
        chapterBox.getChildren().add(new Label("Chapitre : "));
        chapterBox.getChildren().add(chapterComboBox);
        chapterBox.getChildren().add(button);
        chapterBox.setDisable(true);
        vBox.getChildren().add(chapterBox);
    }
    private void createExerciseLine(List<String> strings, VBox vBox) {
        ObservableList<String> exerciseOptions =
                FXCollections.observableArrayList(strings);
        exerciseComboBox = new ComboBox<String>(exerciseOptions);
        exerciseComboBox.getSelectionModel().selectedItemProperty().addListener((options, oldValue, newValue) -> {
            this.selectedExercise = newValue;
            updateChart();
        });
        final Button button = new Button("X");
        button.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                cleanData("exercise");
            }
        });
        exerciseBox = new HBox();
        exerciseBox.getChildren().add(new Label("Exercise :"));
        exerciseBox.getChildren().add(exerciseComboBox);
        exerciseBox.getChildren().add(button);
        exerciseBox.setDisable(true);

        vBox.getChildren().add(exerciseBox);
    }

    private void updateChart() {
        filteredStatisticsExercises = this.statisticsExercises.stream()
                .filter(statisticsExercise -> {
                    Integer matchingResult = 0;
                    if (statisticsExercise.discipline.equals(this.selectedDiscipline) || this.selectedDiscipline == null) {
                        matchingResult++;
                    }
                    if (statisticsExercise.chapter.equals(this.selectedChapter) || this.selectedChapter == null) {
                        matchingResult++;
                    }
                    if (this.selectedExercise == null || statisticsExercise.exerciseId == getExerciseId(this.selectedExercise)) {
                        matchingResult++;
                    }
                    return matchingResult == 3;
                }).collect(Collectors.toList());

        ObservableList<PieChart.Data> pieChartData =
                FXCollections.observableArrayList(
                        new PieChart.Data("Bonne réponse", filteredStatisticsExercises.stream().mapToInt(statisticsExercise -> statisticsExercise.goodAnswers).sum()),
                        new PieChart.Data("Mauvaise Réponse", filteredStatisticsExercises.stream().mapToInt(statisticsExercise -> statisticsExercise.badAnswers).sum()));
        this.chart.getData().removeAll();
        this.chart.setData(pieChartData);
    }

    private int getExerciseId(String selectedExercise) {
        return exercises.stream()
                .filter(exercise -> exercise.getName().equals(selectedExercise))
                .map(exercise -> exercise.getId()).collect(Collectors.toList()).get(0);
    }

    private void cleanData(String state) {
        switch (state) {
            case "discipline" :
                selectedDiscipline = null;
                disciplineComboBox.getSelectionModel().clearSelection();
                chapterBox.setDisable(true);
                cleanData("chapter");
                break;
            case "chapter" :
                selectedChapter = null;
                chapterComboBox.getSelectionModel().clearSelection();
                exerciseBox.setDisable(true);
                cleanData("exercise");
                break;
            case "exercise" :
                selectedExercise = null;
                exerciseComboBox.getSelectionModel().clearSelection();
                break;
            default:
                break;
        }
    }

}
