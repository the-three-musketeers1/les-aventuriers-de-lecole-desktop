package fr.sa.desktopapp.ui.controllers;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.sa.desktop.core.models.Guild;
import fr.sa.desktop.core.services.GuildService;
import fr.sa.desktop.core.services.LeaderService;
import fr.sa.desktop.core.utils.parsers.JsonParser;
import fr.sa.desktop.core.utils.requests.SyncRequest;
import fr.sa.desktopapp.ui.router.Router;
import fr.sa.desktopapp.ui.utils.DialogUtils;
import fr.sa.desktopapp.ui.utils.NodeUtils;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.text.Text;
import org.apache.commons.lang3.StringUtils;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.io.IOException;
import java.net.http.HttpClient;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public class GuildListController {

    private GuildService guildService;

    private List<Guild> guildsLeader;

    private LeaderService leaderService;

    private BorderPane mainWindow;

    private DialogUtils dialogUtils;

    private Router router;
    
    private NodeUtils nodeUtils;

    private Clipboard clipboard;

    private int leaderId;

    @FXML
    private TextField nameInput;

    @FXML
    private DatePicker deadlineInput;

    @FXML
    private VBox guildList;

    public void setMainWindow(BorderPane mainWindow) {
        this.mainWindow = mainWindow;
    }

    @FXML
    public void initialize() {
        guildService = new GuildService(new JsonParser(new ObjectMapper()),
                new SyncRequest(HttpClient.newHttpClient()));

        leaderService = new LeaderService(new JsonParser(new ObjectMapper()),
                new SyncRequest(HttpClient.newHttpClient()));
        leaderId = leaderService.getCurrentLeaderId();
        dialogUtils = new DialogUtils();
        nodeUtils = new NodeUtils();

        loadGuildsLeader();
        clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
    }

    private void loadGuildsLeader() {
        guildList.getChildren().clear();
        guildsLeader = guildService.getGuildsByLeaderId(this.leaderId);
        for (Guild guild: guildsLeader) {
            List<Pane> panes = nodeUtils.createPanes(3);
            TextField guildName = nodeUtils.createTextFieldWithBinding(guild.getName(),
                    (obs, old, value) -> {
                        if(!StringUtils.isEmpty(value)) {
                            guild.setName(value);
                        }
                    });
            guildName.setPrefWidth(200);
            DatePicker datePicker = new DatePicker();
            if(guild.getDeadlineAddCharacter() != null) {
                LocalDate deadline = guild.getDeadlineAddCharacter().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                datePicker.valueProperty().setValue(deadline);
            }

            datePicker.valueProperty().addListener((obs, old, value) ->
                    guild.setDeadlineAddCharacter(Date.from(value.atStartOfDay(ZoneId.systemDefault()).toInstant())));
            datePicker.setPrefWidth(200);
            Label characterCountText = new Label(String.valueOf(guild.getCharacters() != null ? guild.getCharacters().length : 0));
            characterCountText.setAlignment(Pos.CENTER);

            StringSelection tokenString = new StringSelection(guild.getToken());
            Label gt = new Label(guild.getToken());
            gt.setPrefWidth(300);
            HBox guildBox = new HBox(guildName,
                    panes.get(0),
                    characterCountText,
                    panes.get(1),
                    nodeUtils.createIconWithEventAndStyle("/images/copy.png", e -> {
                        clipboard.setContents(tokenString, null);
                        dialogUtils.showInformationPopUp("Token d'invitation", "Token copié !");
                    }),
                    gt,
                    datePicker,
                    panes.get(2),
                    nodeUtils.createIconWithEventAndStyle("/images/check.png", e -> updateGuild(guild)),
                    nodeUtils.createIconWithEventAndStyle("/images/members.png", e -> {
                        router.goTo(GuildController.class, controller -> {
                            controller.setRouter(router);
                            controller.setGuild(guild);
                            controller.setMainWindow(mainWindow);
                        });
                    }),
                    nodeUtils.createIconWithEventAndStyle("/images/trash.png", e -> deleteGuild(guild)));
            guildBox.setPadding(new Insets(0, 0, 10, 0));
            guildList.getChildren().add(guildBox);

        }
    }

    private void deleteGuild(Guild guild) {
        Alert confirmationDialog = dialogUtils.showConfirmationPopUp("Supprimer une guilde",
                String.format("Voulez-vous vraiment supprimer la guilde %s ?", guild.getName()));

        Optional<ButtonType> result = confirmationDialog.showAndWait();
        if (result.isPresent() && result.get() == ButtonType.OK){
            if(!guildService.deleteGuild(guild)) {
                dialogUtils.showErrorPopUp("Suppression", "La suppression a échouée", null);
            } else {
                dialogUtils.showInformationPopUp("Suppression",
                        String.format("La guilde %s a bien été supprimée",
                                guild.getName()));
                loadGuildsLeader();
            }
        }
    }

    private void updateGuild(Guild guild) {

        if(guildService.updateGuild(guild)) {
            dialogUtils.showInformationPopUp("Modifier une guilde",
                    "Votre guilde a bien été modifiée");
        } else {
            dialogUtils.showErrorPopUp("Modifier une guilde",
                    "Votre guilde n'a pas pu être modifiée", null);
        }
        loadGuildsLeader();
    }

    private List<String> checkCreateGuild() {
        List<String> errors = new ArrayList<>();
        if(StringUtils.isEmpty(nameInput.getText())) {
            errors.add("Veuillez renseigner un nom");
        }
        if(deadlineInput.getValue() == null || !deadlineInput.getValue().isAfter(LocalDate.now())) {
            errors.add("Veuillez sélectionner une date limite d'invitation postérieure à aujourd'hui");
        }
        return errors;
    }

    @FXML
    private void createGuild() {
        List<String> errors = checkCreateGuild();
        if(errors.size() > 0) {
            dialogUtils.showErrorPopUp("Création de guilde", "La création de guilde a échouée", String.join("\n", errors));
            return;
        }

        Guild guild = Guild.builder()
                .name(nameInput.getText())
                .deadlineAddCharacter(Date.from(deadlineInput.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant()))
                .leader(leaderService.findOneLeader(this.leaderId))
                .build();

        if(!guildService.createGuild(guild)) {
            dialogUtils.showErrorPopUp("Création de guilde",
                    "Votre guilde n'a pas pu être créée", null);
        } else {
            dialogUtils.showInformationPopUp("Création de guilde",
                    "La guilde " + guild.getName() + " a bien été créée !");
            reload();
        }
    }

    public void reload() {
        nameInput.clear();
        loadGuildsLeader();
    }

    public void setRouter(Router router) {
        this.router = router;
    }
}
