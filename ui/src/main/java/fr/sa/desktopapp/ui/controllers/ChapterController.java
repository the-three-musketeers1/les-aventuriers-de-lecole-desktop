package fr.sa.desktopapp.ui.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.sa.desktop.core.models.Subject;
import fr.sa.desktop.core.services.LeaderService;
import fr.sa.desktop.core.services.SubjectService;
import fr.sa.desktop.core.utils.parsers.JsonParser;
import fr.sa.desktop.core.utils.requests.SyncRequest;
import fr.sa.desktopapp.ui.router.Router;
import fr.sa.desktopapp.ui.utils.DialogUtils;
import fr.sa.desktopapp.ui.utils.NodeUtils;
import fr.sa.desktopapp.ui.utils.ValidatorUtils;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import org.jetbrains.annotations.NotNull;

import java.net.http.HttpClient;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ChapterController {
    private Router router;
    private LeaderService leaderService;
    private SubjectService subjectService;
    private Subject discipline;
    private DialogUtils dialogUtils;
    private NodeUtils nodeUtils;
    private ValidatorUtils validatorUtils;
    private List<Subject> listChapters;
    private ObservableList<Subject> chapters = FXCollections.observableArrayList();
    private Subject chapterToEdit;
    private boolean isEdit;

    @FXML
    private Label subjectTitle;
    @FXML
    private Text subjectDescription;
    @FXML
    private TableView<Subject> chaptersTable;
    @FXML
    private TableColumn<Subject, String> titleColumn;
    @FXML
    private TableColumn<Subject, String> descriptionColumn;
    @FXML
    private Button chapterButtonForm;
    @FXML
    private Label formTitle;
    @FXML
    private TextField titleField;
    @FXML
    private TextArea descriptionField;
    @FXML
    private Button buttonChangeForm;

    @FXML
    public void initialize() {
        leaderService = new LeaderService(new JsonParser(new ObjectMapper()),
                new SyncRequest(HttpClient.newHttpClient()));
        subjectService = new SubjectService(new JsonParser(new ObjectMapper()),
                new SyncRequest(HttpClient.newHttpClient()));
        chapterButtonForm.setOnMouseClicked(event -> this.createChapter());
        dialogUtils = new DialogUtils();
        nodeUtils = new NodeUtils();
        validatorUtils = new ValidatorUtils(dialogUtils);

        titleColumn.setCellValueFactory(new PropertyValueFactory<>("title"));
        descriptionColumn.setCellValueFactory(new PropertyValueFactory<>("description"));
    }

    public void setRouter(Router router) {
        this.router = router;
    }

    public void onDeleteChapter(MouseEvent mouseEvent) {
        final var selectedChapter = chaptersTable.getSelectionModel().getSelectedItem();
        if (selectedChapter == null) {
            return;
        }
        Alert confirmDeleteDialog = dialogUtils.showConfirmationPopUp(
                "Suppression d'un chapitre",
                "Voulez-vous supprimer ce chapitre ?"
        );
        Optional<ButtonType> result = confirmDeleteDialog.showAndWait();
        if (result.isPresent() && result.get() == ButtonType.OK) {

            if (this.subjectService.deleteChapter(discipline.getId(), selectedChapter.getId())) {
                dialogUtils.showInformationPopUp("Suppression d'un chapitre", "Votre chapitre a bien été supprimé");
                chapters.remove(selectedChapter);
            } else {
                dialogUtils.showErrorPopUp("Suppression d'un chapitre", "Votre chapitre n'a pas été supprimé", null);
            }
        }
    }

    public void onShowChapter(MouseEvent mouseEvent) {
        final var selectedChapter = chaptersTable.getSelectionModel().getSelectedItem();
        if (selectedChapter == null) {
            return;
        }
        dialogUtils.showInformationPopUp(
                "Information sur le chapitre",
                String.format("Titre : %s\nDescription: %s", selectedChapter.getTitle(), selectedChapter.getDescription())
        );
    }

    public void setDiscipline(Subject discipline) {
        this.discipline = discipline;
        this.subjectTitle.setText(discipline.getTitle());
        this.subjectDescription.setText(discipline.getDescription());
        listChapters = this.subjectService.findChapters(discipline.getId());
        chapters.addAll(listChapters);
        chaptersTable.setItems(chapters);
        chaptersTable.getSelectionModel().selectedItemProperty().addListener(observable -> {
            if (this.isEdit) {
                this.changeToCreateForm();
            }
        });

        chapterButtonForm.setOnMouseClicked(event -> this.createChapter());
        buttonChangeForm.setOnMouseClicked(event -> this.changeToEditForm());
        this.isEdit = false;
    }

    private void changeToEditForm() {
        chapterToEdit = this.chaptersTable.getSelectionModel().getSelectedItem();
        if (chapterToEdit == null) {
            return;
        }
        this.buttonChangeForm.setGraphic(this.nodeUtils.createImageView("/images/add.png", 32, 32));
        this.buttonChangeForm.setOnMouseClicked(event -> this.changeToCreateForm());
        final var tooltip = this.buttonChangeForm.getTooltip();
        tooltip.setText("Créer");
        this.buttonChangeForm.setTooltip(tooltip);
        this.formTitle.setText(String.format("Edition du chapitre '%s'", chapterToEdit.getTitle()));
        this.titleField.setText(chapterToEdit.getTitle());
        this.descriptionField.setText(chapterToEdit.getDescription());
        this.chapterButtonForm.setText("Editer");
        this.chapterButtonForm.setStyle("-fx-background-color: #03ac13");
        this.chapterButtonForm.setOnMouseClicked(event -> this.editChapter());
        this.isEdit = true;
    }

    private void changeToCreateForm() {
        this.cleanInputs();
        this.formTitle.setText("Création d'un chapitre");
        this.buttonChangeForm.setGraphic(this.nodeUtils.createImageView("/images/pencil.png", 32, 32));
        this.buttonChangeForm.setOnMouseClicked(event -> this.changeToEditForm());
        final var tooltip = this.buttonChangeForm.getTooltip();
        tooltip.setText("Editer");
        this.buttonChangeForm.setTooltip(tooltip);
        this.chapterButtonForm.setText("Créer");
        this.chapterButtonForm.setStyle("-fx-background-color: #377aff");
        this.chapterButtonForm.setOnMouseClicked(event -> this.createChapter());
        this.isEdit = false;
    }

    private void createChapter() {
        if (!validatorUtils.showPotentialErrors("Création de chapitre",
                "Votre chapitre n'a pas pu être créée", checkChapterForm())) {
            return;
        }
        final var chapter = Subject.builder()
                .title(this.titleField.getText())
                .description(this.descriptionField.getText())
                .build();
        final var newChapter = this.subjectService.createChapter(chapter, this.discipline.getId());
        if (newChapter == null) {
            final var errorMessage = String.format("Votre chapitre n'a pas pu être créé, vérifier bien si le chapitre %s n'est pas déjà créé", titleField.getText());
            dialogUtils.showErrorPopUp("Création d'une matière", errorMessage, "");
            return;
        }
        chapters.add(newChapter);
        cleanInputs();
    }

    private void editChapter() {
        final var chapter = Subject.builder()
                .id(chapterToEdit.getId())
                .title(this.titleField.getText().trim())
                .description(this.descriptionField.getText().trim())
                .build();
        if (!validatorUtils.showPotentialErrors("Edition d'un chapitre",
                "Votre chapitre n'a pas pu être édité",
                checkChapterForm()
        )) {
            return;
        }
        if (chapter.getTitle().equals(chapterToEdit.getTitle()) && chapter.getDescription().equals(chapterToEdit.getDescription())) {
            dialogUtils.showErrorPopUp("Edition d'un chapitre",
                    "Votre chapitre édité doit avoir des valeurs différents de sa valeur d'origine. Changer au moins une valeur", ""
            );
            return;
        }
        if (!subjectService.updateChapter(discipline.getId(), chapter)) {
            dialogUtils.showErrorPopUp("Edition d'un chaptre",
                    "Votre chapitre n'a pas pu être édité", "");
            return;
        }
        this.dialogUtils.showInformationPopUp(
                "Edition d'un chapitre",
                "Votre chapitre a bien été édité"
        );
        this.chapters.set(this.chapters.indexOf(chapterToEdit), chapter);
        this.changeToCreateForm();
    }

    private void cleanInputs() {
        this.titleField.clear();
        this.descriptionField.clear();
    }

    @NotNull
    private List<String> checkChapterForm() {
        List<String> errors = new ArrayList<>();

        if (!validatorUtils.hasLengthBetween(this.titleField.getText().trim(), 1, 255)) {
            errors.add("Le titre doit être renseigné");
        }

        if (!validatorUtils.hasLengthBetween(this.descriptionField.getText().trim(), 1, 255)) {
            errors.add("La description doit être renseignée");
        }

        return errors;
    }
}
