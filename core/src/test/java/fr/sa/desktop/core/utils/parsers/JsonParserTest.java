package fr.sa.desktop.core.utils.parsers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import fr.sa.desktop.core.models.Leader;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class JsonParserTest {
    Leader toParse;
    Leader.LeaderBuilder builder = Leader.builder();

    @Mock
    ObjectMapper mapper;

    @InjectMocks
    JsonParser jsonParser;

    @Before
    public void setUp() {
        toParse = builder
                .id(1)
                .email("iujn@ijo.com")
                .firstname("gf")
                .lastname("fg")
                .build();
    }

    @Test
    public void shouldThrowExceptionWhenParsingNullObject() {
        assertThrows(NullPointerException.class, () -> jsonParser.toRaw(null));
    }

    @Test
    public void shouldCallMapperWriteAsStringValueOnce() throws JsonProcessingException {
        jsonParser.toRaw(toParse);
        verify(mapper, times(1)).writeValueAsString(anyObject());
    }

    @Test
    public void shouldReturnJsonRaw() throws JsonProcessingException {
        when(mapper.writeValueAsString(toParse)).thenReturn("Parsing worked fine");
        String raw = jsonParser.toRaw(toParse);
        assertEquals("Parsing worked fine", raw);
    }

    @Test
    public void shouldReturnNullIfRawNull() {
        Leader leader = jsonParser.toObject(null, Leader.class);
        Assert.assertNull(leader);
    }

    /*@Test
    public void shouldCallMapperReadValue() throws IOException {
        jsonParser.toObject("works", Leader.class);
        verify(mapper, times(1)).readValue(anyString(), eq(Leader.class));
    }*/

    @Test
    public void shouldReturnNullIfRawEmpty() {
        Leader leader = jsonParser.toObject("", Leader.class);
        Assert.assertNull(leader);
    }

    @Test
    public void shouldReturnNullIfRawWrongFormat() {
        Leader leader = jsonParser.toObject("bonjour à tous", Leader.class);
        Assert.assertNull(leader);
    }

    /*@Test
    public void shouldReturnObjectIfRawIsCorrect() throws IOException {
        when(mapper.readValue(anyString(), eq(Leader.class))).thenReturn(toParse);
        Leader leader = jsonParser.toObject("Hi everyone my name is Josh", Leader.class);
        assertNotNull(leader);
    }*/





}
