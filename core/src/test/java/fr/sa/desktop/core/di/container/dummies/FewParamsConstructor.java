package fr.sa.desktop.core.di.container.dummies;

import fr.sa.desktop.core.di.annotations.Injectable;

@Injectable
public class FewParamsConstructor {
    private NoConstructorAndParamsClass noConstructorAndParamsClass;
    private EmptyConstructorClass emptyConstructorClass;

    public FewParamsConstructor(NoConstructorAndParamsClass noConstructorAndParamsClass, EmptyConstructorClass emptyConstructorClass) {
        this.noConstructorAndParamsClass = noConstructorAndParamsClass;
        this.emptyConstructorClass = emptyConstructorClass;
    }
}
