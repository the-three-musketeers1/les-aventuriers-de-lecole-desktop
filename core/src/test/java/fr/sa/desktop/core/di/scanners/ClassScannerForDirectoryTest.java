package fr.sa.desktop.core.di.scanners;

import fr.sa.desktop.core.di.annotations.Injectable;
import fr.sa.desktop.core.di.container.dummies.EmptyConstructorClass;
import fr.sa.desktop.core.di.container.dummies.NoConstructorAndParamsClass;
import fr.sa.desktop.core.di.container.dummies.testinterface.TestDummiImpl;
import fr.sa.desktop.core.di.container.dummies.testinterface.TestDummiImpl2;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertTrue;

public class ClassScannerForDirectoryTest {
//    ClassScannerForDirectory classScannerForDirectory;
//
//    @Before
//    public void setUp() {
//        classScannerForDirectory = new ClassScannerForDirectory();
//    }
//
//
//    @Test
//    public void findAllClassesHavingAnnotation_shouldReturnClassWithAnnoation() {
//        List<Class<?>> classArrayList = new ArrayList<>();
//        classArrayList.add(NoConstructorAndParamsClass.class);
//        classArrayList.add(EmptyConstructorClass.class);
//        classScannerForDirectory.setRootPackage("fr.sa.desktop.core.di.container.dummies");
//
//        var classes = classScannerForDirectory.findAllClassHavingAnnotation(Injectable.class);
//
//        var result = classArrayList.stream().anyMatch(classes::containsKey);
//
//        assertThat(classes.size()).isGreaterThanOrEqualTo(2);
//        assertTrue(result);
//    }
//
//    @Test
//    public void findAllClassesHavingAnnotation_whenPackageWithNoInjectableClass_shouldReturnEmptyList() {
//        classScannerForDirectory.setRootPackage("not.injectable.class.in.this.package");
//
//        var classes = classScannerForDirectory.findAllClassHavingAnnotation(Injectable.class);
//
//        assertThat(classes.size()).isEqualTo(0);
//    }
//
//    @Test
//    public void findAllClassesHavingAnnotation_whenInjectableAnnotationInterface_shouldReturnAllClassesImplement() {
//        List<Class<?>> classArrayList = new ArrayList<>();
//        classArrayList.add(TestDummiImpl.class);
//        classArrayList.add(TestDummiImpl2.class);
//        classScannerForDirectory.setRootPackage("fr.sa.desktop.core.di.container.dummies.testinterface");
//
//        var classes = classScannerForDirectory.findAllClassHavingAnnotation(Injectable.class);
//
//        var haveKeys = classArrayList.stream().anyMatch(classes::containsKey);
//        classes.forEach((k, v) -> {
//            assertThat(v).isNotNull();
//        });
//
//        assertTrue(haveKeys);
//    }
}
