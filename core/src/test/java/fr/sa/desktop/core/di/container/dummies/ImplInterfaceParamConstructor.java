package fr.sa.desktop.core.di.container.dummies;

import fr.sa.desktop.core.di.annotations.Injectable;
import fr.sa.desktop.core.di.container.dummies.testinterface.DummiInterface;

@Injectable
public class ImplInterfaceParamConstructor {

    private DummiInterface dummiInterface;
    public ImplInterfaceParamConstructor(DummiInterface dummiInterface) {
        this.dummiInterface = dummiInterface;
    }

    public DummiInterface getDummiInterface() {
        return this.dummiInterface;
    }

    @Override
    public String toString() {
        return "ImplInterfaceParamConstructor{" +
                "dummiInterface=" + dummiInterface +
                '}';
    }
}
