package fr.sa.desktop.core.di.container.dummies;

import fr.sa.desktop.core.di.annotations.Injectable;
import fr.sa.desktop.core.di.container.dummies.testinterface.NotInjectableInterface;

@Injectable
public class NotInjectableParamInterface {

    private NotInjectableInterface notInjectableInterface;
    public NotInjectableParamInterface(NotInjectableInterface notInjectableInterface) {
        this.notInjectableInterface = notInjectableInterface;
    }

    public NotInjectableInterface getNotInjectableInterface() {
        return notInjectableInterface;
    }
}
