package fr.sa.desktop.core.di.container.dummies.deep;

import fr.sa.desktop.core.di.annotations.Injectable;
import fr.sa.desktop.core.di.container.dummies.ImplInterfaceParamConstructor;

@Injectable
public class DeepTestClass {

    private ImplInterfaceParamConstructor interfaceParamConstructor;
    private Deep2TestClass deep2TestClass;

    public DeepTestClass(ImplInterfaceParamConstructor implInterfaceParamConstructor, Deep2TestClass deep2TestClass) {
        this.interfaceParamConstructor = implInterfaceParamConstructor;
        this.deep2TestClass = deep2TestClass;
    }

    public ImplInterfaceParamConstructor getInterfaceParamConstructor() {
        return interfaceParamConstructor;
    }

    public Deep2TestClass getDeep2TestClass() {
        return deep2TestClass;
    }
}
