package fr.sa.desktop.core.di.container.dummies;

import fr.sa.desktop.core.di.annotations.Injectable;

@Injectable
public class OneParamConstructor {
    private NoConstructorAndParamsClass noConstructorAndParamsClass;

    public OneParamConstructor(NoConstructorAndParamsClass noConstructorAndParamsClass) {
        this.noConstructorAndParamsClass = noConstructorAndParamsClass;
    }

    public NoConstructorAndParamsClass getNoConstructorAndParamsClass() {
        return noConstructorAndParamsClass;
    }

    @Override
    public String toString() {
        return "OneParamConstructor{" +
                "noConstructorAndParamsClass=" + noConstructorAndParamsClass +
                '}';
    }
}
