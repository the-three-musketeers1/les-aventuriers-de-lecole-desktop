package fr.sa.desktop.core.di.container;

import fr.sa.desktop.core.di.container.dummies.*;
import fr.sa.desktop.core.di.container.dummies.deep.Deep2TestClass;
import fr.sa.desktop.core.di.container.dummies.deep.DeepTestClass;
import fr.sa.desktop.core.di.container.dummies.testinterface.*;
import fr.sa.desktop.core.di.container.outrange.NotInContainerClass;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class DIContainerTest {
//    DIContainer diContainer;
//
//    @Before
//    public void setup() {
//        diContainer = DIContainer.getInstance();
//        diContainer.implementInjectableClassByPackage("fr.sa.desktop.core.di.container.dummies");
//    }
//
//    @After
//    public void tearDown() {
//        diContainer.clearDiContainer();
//    }
//
//    @Test
//    public void DIContainer_shouldBeSingleton() {
//        var checkDiContainer = DIContainer.getInstance();
//        assertThat(checkDiContainer).isEqualTo(diContainer);
//    }
//
//    @Test
//    public void getClassInstance_whenInjectableClassWithoutConstructorAndParam_shouldReturnInstance() {
//        NoConstructorAndParamsClass result = diContainer.getClassInstance(NoConstructorAndParamsClass.class);
//        var expect = new NoConstructorAndParamsClass();
//
//        assertThat(result.toString()).isEqualTo(expect.toString());
//    }
//
//    @Test
//    public void getClassInstance_whenCall2Times_shouldReturnSameInstance() {
//        var result = diContainer.getClassInstance(NoConstructorAndParamsClass.class);
//        var result2 = diContainer.getClassInstance(NoConstructorAndParamsClass.class);
//
//        assertThat(result).isEqualTo(result2);
//    }
//
//    @Test
//    public void getClassInstance_whenNotInjectableClassButImplementInjectableInterface_shouldReturnInstance() {
//        TestDummiImpl result = diContainer.getClassInstance(TestDummiImpl.class);
//        TestDummiImpl2 result2 = diContainer.getClassInstance(TestDummiImpl2.class);
//
//        assertThat(result).isNotNull();
//        assertThat(result2).isNotNull();
//    }
//
//    @Test
//    public void getClassInstance_whenNotInjectableClass_shouldReturnNull() {
//        NotInjectableClass result = diContainer.getClassInstance(NotInjectableClass.class);
//
//        assertThat(result).isNull();
//    }
//
//    @Test
//    public void getClassInstance_whenClassNotInPackage_shouldReturnNull() {
//        NotInContainerClass result = diContainer.getClassInstance(NotInContainerClass.class);
//
//        assertThat(result).isNull();
//    }
//
//    @Test
//    public void getClassInstance_whenClassHasOneParamConstructor_shouldReturnInstance() {
//        OneParamConstructor result = diContainer.getClassInstance(OneParamConstructor.class);
//
//        assertThat(result).isNotNull();
//    }
//
//    @Test
//    public void getClassInstance_whenClassHasOneParamConstructor_shouldInstanceParam() {
//        OneParamConstructor result = diContainer.getClassInstance(OneParamConstructor.class);
//
//        assertThat(result.getNoConstructorAndParamsClass()).isNotNull();
//    }
//
//    @Test
//    public void getClassInstance_whenClassHasFewParamsConstructor_shouldReturnInstance() {
//        FewParamsConstructor result = diContainer.getClassInstance(FewParamsConstructor.class);
//
//        assertThat(result).isNotNull();
//    }
//
//    @Test(expected = IllegalStateException.class)
//    public void getClassInstance_whenClassHasParamImplementInjectableInterfaceWithoutIndicate_shouldThrowError() {
//        ImplInterfaceParamConstructor result = diContainer.getClassInstance(ImplInterfaceParamConstructor.class);
//    }
//
//    @Test(expected = IllegalStateException.class)
//    public void getClassInstance_whenClassHasParamImplementNotInjectableInterfaceIndicate_shouldThrowError() {
//        diContainer.indicateImplementedParamClass(NotInjectableParamInterface.class, NotInjectableInterface.class, TestNotInjectableImpl.class);
//        var check = diContainer.getClassInstance(NotInjectableParamInterface.class);
//    }
//
//    @Test
//    public void getClassInstance_whenClassHasParamImplementInjectableInterfaceIndicate_shouldInstanceParam() {
//        diContainer.indicateImplementedParamClass(ImplInterfaceParamConstructor.class, DummiInterface.class, TestDummiImpl.class);
//        ImplInterfaceParamConstructor result = diContainer.getClassInstance(ImplInterfaceParamConstructor.class);
//
//        assertThat(result.getDummiInterface()).isNotNull();
//    }
//
//    @Test
//    public void getClassInstance_whenClasshaveDeeplyInjactableClasses_shouldReturnInstanceWithAllParamsInstances() {
//        diContainer.indicateImplementedParamClass(ImplInterfaceParamConstructor.class, DummiInterface.class, TestDummiImpl.class);
//        diContainer.indicateImplementedParamClass(Deep2TestClass.class, DummiInterface.class, TestDummiImpl2.class);
//
//        DeepTestClass result = diContainer.getClassInstance(DeepTestClass.class);
//
//        assertThat(result).isNotNull();
//        assertThat(result.getDeep2TestClass()).isNotNull();
//        assertThat(result.getDeep2TestClass().getClass()).isEqualTo(Deep2TestClass.class);
//        assertThat(result.getInterfaceParamConstructor()).isNotNull();
//        assertThat(result.getInterfaceParamConstructor().getClass()).isEqualTo(ImplInterfaceParamConstructor.class);
//
//        var deep2TestInstance = result.getDeep2TestClass();
//        assertThat(deep2TestInstance.getDummiInterface()).isNotNull();
//        assertThat(deep2TestInstance.getDummiInterface().getClass()).isEqualTo(TestDummiImpl2.class);
//        assertThat(deep2TestInstance.getOneParamConstructor()).isNotNull();
//        assertThat(deep2TestInstance.getOneParamConstructor().getClass()).isEqualTo(OneParamConstructor.class);
//
//        var interfaceParamConstructor = result.getInterfaceParamConstructor();
//        assertThat(interfaceParamConstructor.getDummiInterface()).isNotNull();
//        assertThat(interfaceParamConstructor.getDummiInterface().getClass()).isEqualTo(TestDummiImpl.class);
//
//        var oneParamConstructor = deep2TestInstance.getOneParamConstructor();
//        assertThat(oneParamConstructor.getNoConstructorAndParamsClass()).isNotNull();
//        assertThat(oneParamConstructor.getNoConstructorAndParamsClass().getClass()).isEqualTo(NoConstructorAndParamsClass.class);
//    }

}
