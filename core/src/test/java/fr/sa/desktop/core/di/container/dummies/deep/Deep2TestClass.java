package fr.sa.desktop.core.di.container.dummies.deep;

import fr.sa.desktop.core.di.annotations.Injectable;
import fr.sa.desktop.core.di.container.dummies.OneParamConstructor;
import fr.sa.desktop.core.di.container.dummies.testinterface.DummiInterface;

@Injectable
public class Deep2TestClass {
    private DummiInterface dummiInterface;
    private OneParamConstructor oneParamConstructor;

    public Deep2TestClass(DummiInterface dummiInterface, OneParamConstructor oneParamConstructor) {
        this.dummiInterface = dummiInterface;
        this.oneParamConstructor = oneParamConstructor;
    }

    public DummiInterface getDummiInterface() {
        return dummiInterface;
    }

    public OneParamConstructor getOneParamConstructor() {
        return oneParamConstructor;
    }
}
