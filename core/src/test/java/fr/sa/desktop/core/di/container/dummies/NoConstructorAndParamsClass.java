package fr.sa.desktop.core.di.container.dummies;

import fr.sa.desktop.core.di.annotations.Injectable;

@Injectable
public class NoConstructorAndParamsClass {

    @Override
    public String toString() {
        return "ClassNoConstructorAndParams{}";
    }
}
