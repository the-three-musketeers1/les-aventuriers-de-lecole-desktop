package fr.sa.desktop.core.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import java.time.LocalDate;
import java.util.Date;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString(of = {"id", "current_mana", "current_life", "guild", "adventure", "accessible", "deadline"})
@JsonIgnoreProperties({ "createdAt", "updatedAt", "current_mana", "current_life" })
public class GuildAdventure {

    private Integer id;
    private Integer current_mana;
    private Integer current_life;
    private int guildId;
    private int adventureId;
    private Guild guild;
    private Adventure adventure;
    private Boolean accessible;
    private Date deadline;
}
