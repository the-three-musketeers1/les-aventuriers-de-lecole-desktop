package fr.sa.desktop.core.utils.requests;

import fr.sa.desktop.core.services.AuthService;
import io.github.cdimascio.dotenv.Dotenv;

import javax.inject.Inject;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

public class SyncRequest implements Request {

    private final Dotenv dotenv = Dotenv.load();
    private final String URL = dotenv.get("URL") + ':' + dotenv.get("PORT") + '/';
    private final HttpClient httpClient;
    private String token = "";

    /**
     * constructor to read the authToken file and set token to the request
     * @param httpClient : Http client to send request
     */
    public SyncRequest(HttpClient httpClient) {
        this.httpClient = httpClient;
        this.setTokenBySessionFile();
    }

    /**
     * constructor to read authToken file and set token only if mustSetToken is true
     * @param httpClient : http client to send request
     * @param mustSetToken : read authToken file and set token when is true
     */
    public SyncRequest(HttpClient httpClient, boolean mustSetToken) {
        this.httpClient = httpClient;
        if (mustSetToken) {
            this.setTokenBySessionFile();
        }
    }

    public HttpResponse<String> get(String url) {

        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(this.URL + url))
                .GET()
                .header("Content-Type", "application/json")
                .header("authorization", String.format("Bearer %s", this.token))
                .build();

        return this.sendRequest(request);
    }

    public HttpResponse<String> post(String url, String json) {

        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(this.URL + url))
                .POST(HttpRequest.BodyPublishers.ofString(json))
                .header("Content-Type", "application/json")
                .header("authorization", String.format("Bearer %s", this.token))
                .build();

        return this.sendRequest(request);
    }

    public HttpResponse<String> put(String url, String json) {
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(this.URL + url))
                .PUT(HttpRequest.BodyPublishers.ofString(json))
                .header("Content-Type", "application/json")
                .header("authorization", String.format("Bearer %s", this.token))
                .build();

        return this.sendRequest(request);
    }

    @Override
    public void setToken(String token) {
        if (token != null) {
            this.token = token;
        }
    }

    @Override
    public void setTokenBySessionFile() {
        try {
            this.token = Files.readString(Paths.get(AuthService.TOKEN_FILE_PATH), StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getToken() {
        return this.token;
    }

    public HttpResponse<String> delete(String url) {

        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(this.URL + url))
                .DELETE()
                .header("Content-Type", "application/json")
                .header("authorization", String.format("Bearer %s", this.token))
                .build();

        return this.sendRequest(request);
    }

    private HttpResponse<String> sendRequest(HttpRequest request) {
        try {
            return httpClient.send(request, HttpResponse.BodyHandlers.ofString());
        } catch (InterruptedException | IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
