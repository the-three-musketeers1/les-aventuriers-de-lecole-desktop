package fr.sa.desktop.core.services;

import fr.sa.desktop.core.models.Leader;
import fr.sa.desktop.core.models.Session;
import fr.sa.desktop.core.utils.parsers.Parser;
import fr.sa.desktop.core.utils.requests.Request;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

public class AuthService
{
    public static String TOKEN_FILE_PATH = "authToken.txt";
    private final Parser parser;
    private final Request request;

    public AuthService(Parser parser, Request request) {
        this.parser = parser;
        this.request = request;
    }

    public HttpResponse<String> login(Leader leader) {
        String expressionJson = this.parser.toRaw(leader);

        return this.request.post("auth/login/teacher", expressionJson);
    }

    public void saveSession(String sessionString, FileWriter tokenFileWriter) throws IOException {
        var session = this.parser.toObject(sessionString, Session.class);
        tokenFileWriter.write(session.getToken());
    }

    public boolean logout() {
        this.request.setTokenBySessionFile();
        final var response = this.request.delete("auth/logout");
        return response != null && response.statusCode() == 204;
    }

    public String getTokenByFile() throws IOException {
        return Files.readString(Paths.get(AuthService.TOKEN_FILE_PATH), StandardCharsets.UTF_8);
    }

    public boolean hasAuthTokenFile() {
        File checkAuthTokenFile = new File(AuthService.TOKEN_FILE_PATH);
        return checkAuthTokenFile.exists();
    }

    public boolean deleteAuthTokenFile() throws IOException {
        return Files.deleteIfExists(Paths.get(AuthService.TOKEN_FILE_PATH));
    }

    public HttpResponse<String> subscribe(Leader leader) {
        String leaderJson = this.parser.toRaw(leader);
        String expressionJson;
        if (leaderJson.contains("\"id\":null,")) {
            expressionJson = leaderJson.replace("\"id\":null,", "");
        } else {
            expressionJson = leaderJson;
        }
        return this.request.post("auth/subscribe/", expressionJson);
    }
}
