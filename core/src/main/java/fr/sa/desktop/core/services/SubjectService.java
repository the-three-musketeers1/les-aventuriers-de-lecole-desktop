package fr.sa.desktop.core.services;

import fr.sa.desktop.core.models.Guild;
import fr.sa.desktop.core.models.Subject;
import fr.sa.desktop.core.plugin.models.Discipline;
import fr.sa.desktop.core.utils.parsers.Parser;
import fr.sa.desktop.core.utils.requests.Request;

import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.List;

public class SubjectService {

    private Parser parser;
    private Request request;

    public SubjectService(Parser parser, Request request) {
        this.parser = parser;
        this.request = request;
    }

    public List<Subject> findSubjects() {
        HttpResponse<String> response = this.request.get("subjects");

        if(response != null && response.statusCode() == 200) {
            return this.parser.toObjectList(response.body(), Subject.class);
        }

        return null;
    }

    public Subject findOneSubject(Integer id) {
        HttpResponse<String> response = this.request.get("subjects/" + id);

        return getSubjectIfStatusCodeIsCorrect(response, 200);
    }

    public Subject createSubject(Subject subject) {
        String subjectJson = this.parser.toRaw(subject);

        HttpResponse<String> response = this.request.post("subjects", subjectJson);

        return getSubjectIfStatusCodeIsCorrect(response, 201);
    }

    public boolean updateSubject(Subject subject) {
        String subjectJson = this.parser.toRaw(subject);

        HttpResponse<String> response = this.request.put("subjects/" + subject.getId(), subjectJson);

        return response != null && response.statusCode() == 204;
    }

    public boolean deleteSubject(Integer id) {
        HttpResponse<String> response = this.request.delete("subjects/" + id);
        return response != null && response.statusCode() == 204;
    }

    private Subject getSubjectIfStatusCodeIsCorrect(HttpResponse<String> response, Integer statusCode) {
        if(response != null && response.statusCode() == statusCode) {
            return this.parser.toObject(response.body(), Subject.class);
        }
        return null;
    }

    public List<Discipline> findSubjectsByGuild(Guild guild) {
        HttpResponse<String> response = this.request.get("subjects/guild/" + guild.getId());
        if(response != null && response.statusCode() == 200) {
            return this.parser.toObjectList(response.body(), Discipline.class);
        }

        return new ArrayList<Discipline>();
    }

    public Subject createChapter(Subject chapter, Integer id) {
        String url = String.format("subjects/disciplines/%d/chapters", id);
        String chapterJson = this.parser.toRaw(chapter);
        HttpResponse<String> response = this.request.post(url, chapterJson);

        return getSubjectIfStatusCodeIsCorrect(response, 201);
    }

    public List<Subject> findChapters(Integer id) {
        String url = String.format("subjects/disciplines/%d/chapters", id);
        HttpResponse<String> response = this.request.get(url);
        if(response != null && response.statusCode() == 200) {
            return this.parser.toObjectList(response.body(), Subject.class);
        }
        return new ArrayList<Subject>();
    }

    public boolean deleteChapter(Integer disciplineId, Integer chapterId) {
        String url = String.format("subjects/disciplines/%d/chapters/%d", disciplineId, chapterId);
        HttpResponse<String> response = this.request.delete(url);
        return response != null && response.statusCode() == 204;
    }

    public boolean updateChapter(Integer disciplineId, Subject chapter) {
        String url = String.format("subjects/disciplines/%d/chapters/%d", disciplineId, chapter.getId());
        String chapterJson = parser.toRaw(chapter);
        HttpResponse<String> response = this.request.put(url, chapterJson);
        return response != null && response.statusCode() == 204;
    }
}
