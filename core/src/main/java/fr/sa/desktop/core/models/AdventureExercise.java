package fr.sa.desktop.core.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString(of = {"id", "adventure", "exercise"})
@JsonIgnoreProperties({ "createdAt", "updatedAt" })
public class AdventureExercise {
    private Integer id;
    private Adventure adventure;
    private Exercise exercise;
    private int adventureId;
    private int exerciseId;
}
