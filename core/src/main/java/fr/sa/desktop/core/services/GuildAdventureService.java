package fr.sa.desktop.core.services;

import fr.sa.desktop.core.models.Adventure;
import fr.sa.desktop.core.models.GuildAdventure;
import fr.sa.desktop.core.models.Leader;
import fr.sa.desktop.core.models.Question;
import fr.sa.desktop.core.utils.parsers.Parser;
import fr.sa.desktop.core.utils.requests.Request;

import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.List;

public class GuildAdventureService {

    private final Parser parser;
    private final Request request;

    public GuildAdventureService(Parser parser, Request request) {
        this.parser = parser;
        this.request = request;
    }

    public List<GuildAdventure> findGuildExpeditionsByLeader(int leaderId) {
        HttpResponse<String> response = this.request.get("guild-adventure/expeditions/leader/" + leaderId);

        if(response != null && response.statusCode() == 200) {
            return this.parser.toObjectList(response.body(), GuildAdventure.class);
        }

        return new ArrayList<GuildAdventure>();
    }

    public boolean createGuildExpedition(GuildAdventure guildExpedition) {
        String guildExpeditionJson = this.parser.toRaw(guildExpedition);

        HttpResponse<String> response = this.request.post("guild-adventure/expeditions", guildExpeditionJson);

        return response != null && response.statusCode() == 201;
    }

    public boolean deleteGuildExpedition(int id) {
        HttpResponse<String> response = this.request.delete("guild-adventure/" + id);
        return response != null && response.statusCode() == 204;
    }

    public boolean updateGuildExpedition(GuildAdventure guildExpedition) {
        String guildExpeditionJson = this.parser.toRaw(guildExpedition);

        HttpResponse<String> response = this.request.put("guild-adventure/expedition/" + guildExpedition.getId(), guildExpeditionJson);

        return response != null && response.statusCode() == 204;
    }


}
