package fr.sa.desktop.core.services;

import fr.sa.desktop.core.models.Adventure;
import fr.sa.desktop.core.models.Exercise;
import fr.sa.desktop.core.utils.parsers.Parser;
import fr.sa.desktop.core.utils.requests.Request;

import java.net.http.HttpResponse;

public class AdventureService {

    private final Parser parser;
    private final Request request;

    public AdventureService(Parser parser, Request request) {
        this.parser = parser;
        this.request = request;
    }

    public boolean createExpedition(Adventure expedition) {
        String expeditionJson = this.parser.toRaw(expedition);

        HttpResponse<String> response = this.request.post("adventures", expeditionJson);

        return response != null && response.statusCode() == 201;
    }

    public boolean deleteExpedition(Integer id) {
        HttpResponse<String> response = this.request.delete("adventures/" + id);

        return response != null && response.statusCode() == 204;
    }

    public boolean updateExpedition(Adventure expedition) {
        String expeditionJson = this.parser.toRaw(expedition);

        HttpResponse<String> response = this.request.put("adventures/" + expedition.getId(), expeditionJson);

        return response != null && response.statusCode() == 204;
    }


}
