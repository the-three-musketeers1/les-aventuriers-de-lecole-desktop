package fr.sa.desktop.core.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.util.Objects;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString(of = {"id", "name", "description", "life", "experience", "subject", "leader"})
@JsonIgnoreProperties({ "createdAt", "updatedAt" })
public class Exercise {

    private Integer id;
    private String name;
    private String description;
    private Integer life;
    private Integer experience;
    private Subject subject;
    private Leader leader;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Exercise exercise = (Exercise) o;
        EqualsBuilder builder = new EqualsBuilder();
        builder.append(getId(), exercise.getId());
        builder.append(getName(), exercise.getName());
        builder.append(getDescription(), exercise.getDescription());
        builder.append(getLife(), exercise.getLife());
        builder.append(getExperience(), exercise.getExperience());
        return builder.isEquals();
    }

    @Override
    public int hashCode() {
        HashCodeBuilder builder = new HashCodeBuilder(17, 37);
        builder.append(getId());
        builder.append(getName());
        builder.append(getDescription());
        builder.append(getLife());
        builder.append(getExperience());
        return builder.toHashCode();
    }
}
