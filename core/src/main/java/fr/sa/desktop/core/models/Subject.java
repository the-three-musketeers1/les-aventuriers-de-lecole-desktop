package fr.sa.desktop.core.models;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString(of = {"id", "title", "description", "parentSubject", "childSubjects", "leaders"})
public class Subject {

    private Integer id;
    private String title;
    private String description;
    private Subject parentSubject;
    private Subject[] childSubjects;
    private Leader[] leaders;
}
