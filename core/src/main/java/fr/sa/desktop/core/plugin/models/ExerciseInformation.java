package fr.sa.desktop.core.plugin.models;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ExerciseInformation {
    private Integer id;
    private Integer life;
    private String name;
    private String chapter;
    private String discipline;
}
