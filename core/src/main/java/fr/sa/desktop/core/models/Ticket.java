package fr.sa.desktop.core.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import java.util.Date;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString(of = {"id", "title", "character", "leader", "messages", "date", "updatedAt", "status"})
public class Ticket {
    private Integer id;
    private String title;
    private CharacterUser character;
    private Leader leader;
    private Message[] messages;
    private Date date;
    private Date updatedAt;
    private Status status;
}
