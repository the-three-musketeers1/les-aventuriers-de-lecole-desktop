package fr.sa.desktop.core.services;

import fr.sa.desktop.core.models.Answer;
import fr.sa.desktop.core.models.Question;
import fr.sa.desktop.core.utils.parsers.Parser;
import fr.sa.desktop.core.utils.requests.Request;

import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.List;

public class QuestionService {
    
    private Parser parser;
    private Request request;

    public QuestionService(Parser parser, Request request) {
        this.parser = parser;
        this.request = request;
    }

    public List<Question> findQuestions() {
        HttpResponse<String> response = this.request.get("questions");

        if(response != null && response.statusCode() == 200) {
            return this.parser.toObjectList(response.body(), Question.class);
        }

        return null;
    }

    public List<Answer> findQuestionAnswers(Integer id) {
        HttpResponse<String> response = this.request.get("questions/" + id + "/answers");

        if(response != null && response.statusCode() == 200) {
            return this.parser.toObjectList(response.body(), Answer.class);
        }

        return new ArrayList<Answer>();
    }

    public Question findQuestionBySequenceNumber(Integer sequenceNumber) {
        HttpResponse<String> response = this.request.get("questions/sequence-number/" + sequenceNumber);
        return getQuestionIfStatusCodeIsCorrect(response, 200);
    }

    public Question findOneQuestion(Integer id) {
        HttpResponse<String> response = this.request.get("questions/" + id);

        return getQuestionIfStatusCodeIsCorrect(response, 200);
    }

    public Question createQuestion(Question question) {
        String questionJson = this.parser.toRaw(question);

        HttpResponse<String> response = this.request.post("questions", questionJson);

        return getQuestionIfStatusCodeIsCorrect(response, 201);
    }

    public boolean updateQuestion(Question question) {
        String questionJson = this.parser.toRaw(question);

        HttpResponse<String> response = this.request.put("questions/" + question.getId(), questionJson);

        return response != null && response.statusCode() == 204;
    }

    public boolean deleteQuestion(Integer id) {
        HttpResponse<String> response = this.request.delete("questions/" + id);

        return response != null && response.statusCode() == 204;
    }

    private Question getQuestionIfStatusCodeIsCorrect(HttpResponse<String> response, Integer statusCode) {
        if(response != null && response.statusCode() == statusCode) {
            return this.parser.toObject(response.body(), Question.class);
        }
        return null;
    }
}
