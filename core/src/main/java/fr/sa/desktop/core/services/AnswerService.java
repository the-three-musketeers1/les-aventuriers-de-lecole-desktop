package fr.sa.desktop.core.services;

import fr.sa.desktop.core.models.Answer;
import fr.sa.desktop.core.models.Question;
import fr.sa.desktop.core.utils.parsers.Parser;
import fr.sa.desktop.core.utils.requests.Request;

import java.net.http.HttpResponse;
import java.util.List;

public class AnswerService {
    
    private Parser parser;
    private Request request;

    public AnswerService(Parser parser, Request request) {
        this.parser = parser;
        this.request = request;
    }

    public Answer createAnswer(Answer answer) {
        String answerJson = this.parser.toRaw(answer);

        HttpResponse<String> response = this.request.post("answers", answerJson);

        return getAnswerIfStatusCodeIsCorrect(response, 201);
    }

    public boolean updateAnswer(Answer answer) {
        String answerJson = this.parser.toRaw(answer);

        HttpResponse<String> response = this.request.put("answers/" + answer.getId(), answerJson);

        return response != null && response.statusCode() == 204;
    }

    private Answer getAnswerIfStatusCodeIsCorrect(HttpResponse<String> response, Integer statusCode) {
        if (response != null && response.statusCode() == statusCode) {
            return this.parser.toObject(response.body(), Answer.class);
        }
        return null;
    }

    public List<Answer> findAnswersByQuestion(Question question) {
        HttpResponse<String> response = this.request.get("answers/question/" + question.getId());

        if(response != null && response.statusCode() == 200) {
            return this.parser.toObjectList(response.body(), Answer.class);
        }
        return null;
    }

    public Answer findOneAnswer(Integer id) {
        HttpResponse<String> response = this.request.get("answers/" + id);

        return getAnswerIfStatusCodeIsCorrect(response, 200);
    }

    public boolean deleteAnswer(Integer id) {
        HttpResponse<String> response = this.request.delete("answers/" + id);
        return response != null && response.statusCode() == 204;
    }
}
