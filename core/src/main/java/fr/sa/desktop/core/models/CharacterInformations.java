package fr.sa.desktop.core.models;


import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString(of = {"id", "pseudo"})
public class CharacterInformations {
    private Integer id;
    private String  pseudo;
    private Integer level;
    private Integer action_points;
    private Integer experience;
    private String role;
    private String guild_bonus;
    private String guild_bonus_cost;
    private String board_bonus;
    private String board_bonus_cost;
    private String first_name;
    private String last_name;
    private String email;
    private String guild_name;
}
