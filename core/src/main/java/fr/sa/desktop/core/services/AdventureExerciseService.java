package fr.sa.desktop.core.services;

import fr.sa.desktop.core.models.Adventure;
import fr.sa.desktop.core.models.AdventureExercise;
import fr.sa.desktop.core.models.GuildAdventure;
import fr.sa.desktop.core.models.Question;
import fr.sa.desktop.core.utils.parsers.Parser;
import fr.sa.desktop.core.utils.requests.Request;

import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.List;

public class AdventureExerciseService {

    private final Parser parser;
    private final Request request;

    public AdventureExerciseService(Parser parser, Request request) {
        this.parser = parser;
        this.request = request;
    }

    public boolean deleteExpeditionExercise(int id) {
        HttpResponse<String> response = this.request.delete("adventures-exercises/" + id);
        return response != null && response.statusCode() == 204;
    }

    public List<AdventureExercise> findExpeditionExercisesByLeader(int id) {
        HttpResponse<String> response = this.request.get("adventures-exercises/" + id);

        if(response != null && response.statusCode() == 200) {
            return this.parser.toObjectList(response.body(), AdventureExercise.class);
        }

        return new ArrayList<>();
    }

    public boolean createExpeditionExercise(AdventureExercise expeditionExercise) {
        String expeditionExerciseJson = this.parser.toRaw(expeditionExercise);

        HttpResponse<String> response = this.request.post("adventures-exercises", expeditionExerciseJson);

        return response != null && response.statusCode() == 201;
    }
}
