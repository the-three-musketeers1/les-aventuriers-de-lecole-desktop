package fr.sa.desktop.core.services;

import fr.sa.desktop.core.models.Status;
import fr.sa.desktop.core.models.Ticket;
import fr.sa.desktop.core.utils.parsers.Parser;
import fr.sa.desktop.core.utils.requests.Request;

import java.net.http.HttpResponse;
import java.util.List;

public class StatusService {
    private Parser parser;
    private Request request;

    public StatusService(Parser parser, Request request) {
        this.parser = parser;
        this.request = request;
    }

    public List<Status> findStatus() {
        HttpResponse<String> response = this.request.get("status");

        if (response != null && response.statusCode() == 200) {
            return this.parser.toObjectList(response.body(), Status.class);
        }
        return null;
    }
}
