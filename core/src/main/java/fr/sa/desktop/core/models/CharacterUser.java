package fr.sa.desktop.core.models;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString(of = {"id", "name", "user"})
public class CharacterUser {
    private Integer id;
    private String  name;
    private Integer level;
    private Integer action_points;
    private Integer experience;
    private Ticket[] tickets;
    private User user;
}
