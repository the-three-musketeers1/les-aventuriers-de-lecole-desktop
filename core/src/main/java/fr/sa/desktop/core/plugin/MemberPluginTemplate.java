package fr.sa.desktop.core.plugin;

import fr.sa.desktop.core.plugin.models.CharacterWithStatistics;
import fr.sa.desktop.core.plugin.models.StatisticsExercise;
import javafx.scene.Node;

import java.util.HashMap;
import java.util.List;

public interface MemberPluginTemplate {
    public String setButtonName();
    public String setTitle();
    public Node setPluginContent(List<StatisticsExercise> exercises, HashMap<String, String> characterInfos);
    public String setAllMemberActionButtonName();
    public void setAllMemberActionButtonAction(List<CharacterWithStatistics> charactersStatistics);
}
