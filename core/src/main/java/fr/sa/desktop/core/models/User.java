package fr.sa.desktop.core.models;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString(of = {"id", "email", "firstname", "lastname", "is_teacher"})
public class User {
    private Integer id;
    private String email;
    private String firstname;
    private String lastname;
    private String password;
    private Boolean is_teacher;
}
