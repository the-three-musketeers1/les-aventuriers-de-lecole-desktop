package fr.sa.desktop.core.plugin.models;

import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CharacterWithStatistics {
    public Integer id;
    public String pseudo;
    public String firstName;
    public String lastName;
    public List<StatisticsExercise> exercisesAnswer;
}
