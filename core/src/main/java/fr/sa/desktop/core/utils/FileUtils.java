package fr.sa.desktop.core.utils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileUtils {

    public void createDirectoryIfNotExists(String path) {
        Path dir = Paths.get(path);
        if(Files.notExists(dir)) {
            try {
                Files.createDirectory(dir);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void createPluginDirs() {
        createDirectoryIfNotExists("plugin");
        createDirectoryIfNotExists("plugin/enable");
        createDirectoryIfNotExists("plugin/disable");
    }
}
