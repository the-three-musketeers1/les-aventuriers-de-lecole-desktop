package fr.sa.desktop.core.models;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString(of = {"id", "name"})
public class QuestionType {

    private Integer id;
    private String name;
}
