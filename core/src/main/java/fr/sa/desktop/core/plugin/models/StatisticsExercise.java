package fr.sa.desktop.core.plugin.models;

import lombok.*;

import java.util.Date;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class StatisticsExercise {
    public Integer characterExerciseId;
    public Integer exerciseId;
    public Integer exerciseLife;
    public String discipline;
    public String chapter;
    public Integer goodAnswers;
    public Integer badAnswers;
    public Integer damage;
    public Date createAt;

}
