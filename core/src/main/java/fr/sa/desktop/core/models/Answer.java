package fr.sa.desktop.core.models;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString()
public class Answer {

    private Integer id;
    private String content;
    private boolean correct;
    private Question question;
}
