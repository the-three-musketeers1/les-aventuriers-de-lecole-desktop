package fr.sa.desktop.core.plugin.models;


import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Chapter {
    private Integer id;
    private String title;
    private String description;
}
