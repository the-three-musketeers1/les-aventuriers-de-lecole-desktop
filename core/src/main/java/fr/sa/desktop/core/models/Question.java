package fr.sa.desktop.core.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import java.util.Date;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString(of = {"id", "wording", "sequence_number", "damage", "time", "questionType", "exercise"})
@JsonIgnoreProperties({ "createdAt", "updatedAt" })
public class Question {

    private Integer id;
    private String wording;
    private Integer sequence_number;
    private Integer damage;
    private Integer time;
    private Exercise exercise;
    private QuestionType questionType;
}
