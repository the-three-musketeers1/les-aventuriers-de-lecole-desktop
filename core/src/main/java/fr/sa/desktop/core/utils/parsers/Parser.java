package fr.sa.desktop.core.utils.parsers;

import com.fasterxml.jackson.core.JsonProcessingException;

import java.io.IOException;
import java.util.List;

public interface Parser {
    <T> List<T> toObjectList(final String raw, final Class<T> type);

    <T> T toObject(final String raw, Class<T> type);

    <T> String toRaw(T objects);
}
