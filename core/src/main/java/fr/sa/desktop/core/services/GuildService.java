package fr.sa.desktop.core.services;

import fr.sa.desktop.core.models.CharacterInformations;
import fr.sa.desktop.core.models.CharacterUser;
import fr.sa.desktop.core.models.Guild;
import fr.sa.desktop.core.models.Leader;
import fr.sa.desktop.core.utils.parsers.Parser;
import fr.sa.desktop.core.utils.requests.Request;

import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.List;

public class GuildService {

    private final Parser parser;
    private final Request request;

    public GuildService(Parser parser, Request request) {
        this.parser = parser;
        this.request = request;
    }

    public List<Guild> getGuildsByLeaderId(final Integer leaderId) {
        HttpResponse<String> response = this.request.get("guild/leader/" + leaderId);

        if(response != null && response.statusCode() == 200) {
            return this.parser.toObjectList(response.body(), Guild.class);
        }

        return new ArrayList<Guild>();
    }

    public boolean createGuild(final Guild guild) {
        String guildJson = this.parser.toRaw(guild);

        HttpResponse<String> response = this.request.post("guild", guildJson);

        return response != null && response.statusCode() == 201;
    }

    public boolean updateGuild(final Guild guild) {
        String guildJson = this.parser.toRaw(guild);

        HttpResponse<String> response = this.request.put("guild/" + guild.getId(), guildJson);

        return response != null && response.statusCode() == 204;
    }

    public List<CharacterInformations> getGuildMemberByGuildId(final Integer guildId) {
        HttpResponse<String> response = this.request.get("guild/" + guildId + "/members");

        if(response != null && response.statusCode() == 200) {
            return this.parser.toObjectList(response.body(), CharacterInformations.class);
        }

        return new ArrayList<>();
    }

    public boolean deleteGuild(Guild guild) {
        HttpResponse<String> response = this.request.delete("guild/" + guild.getId());
        return response != null && response.statusCode() == 204;
    }

    public boolean deleteMember(Integer id) {
        HttpResponse<String> response = this.request.delete("character/" + id);
        return response != null && response.statusCode() == 204;
    }
}
