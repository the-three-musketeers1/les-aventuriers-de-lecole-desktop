package fr.sa.desktop.core.services;

import fr.sa.desktop.core.utils.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PluginsGestionService {

    private FileUtils fileUtils;

    public PluginsGestionService(FileUtils fileUtils) {
        this.fileUtils = fileUtils;
    }

    public List<String> findPluginsByState(String state) {
        fileUtils.createPluginDirs();
        try (Stream<Path> walk = Files.walk(Paths.get("plugin/" + state))) {
            List<String> result = walk.filter(Files::isRegularFile)
                    .map(Path::toString).collect(Collectors.toList());
            return result;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }

    public String getPluginName(String enabledPluginStr) {
        File f = new File(enabledPluginStr);
        String filename = f.getName();
        filename = filename.substring(0, filename.lastIndexOf('.'));
        return filename;
    }

    public boolean movePlugin(String pluginPath, Boolean enable) {
        File file = new File(pluginPath);
        String destinationPath;
        if (enable) {
            destinationPath = pluginPath.replace("disable","enable");
        } else {
            destinationPath = pluginPath.replace("enable","disable");
        }
        return file.renameTo(new File(destinationPath));
    }
}
