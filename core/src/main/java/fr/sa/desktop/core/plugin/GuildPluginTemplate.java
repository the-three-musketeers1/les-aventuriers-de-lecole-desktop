package fr.sa.desktop.core.plugin;

import fr.sa.desktop.core.plugin.models.ExerciseInformation;
import fr.sa.desktop.core.plugin.models.Discipline;
import fr.sa.desktop.core.plugin.models.StatisticsExercise;
import javafx.scene.Node;

import java.util.List;

public interface GuildPluginTemplate {
    public String getButtonName();
    public String getTitle();
    public Node getMainPanel(List<StatisticsExercise> statisticsExerciseList);
    public Node getSecondaryPanel(List<Discipline> subjects, List<ExerciseInformation> exercises);
}
