package fr.sa.desktop.core.di.scanners;

import java.io.File;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.*;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public class ClassScannerForJarFile implements ClassScanner {
    private String rootPackage;
    private String jarFilePath;

    public ClassScannerForJarFile(String jarFilePath) {
        this.jarFilePath = jarFilePath;
    }

    @Override
    public void setRootPackage(String rootPackage) {
        this.rootPackage = rootPackage;
    }

    @Override
    public Map<Class<?>, Class<?>> findAllClassHavingAnnotation(Class<? extends Annotation> annotation) {
        final Map<Class<?>, Class<?>> implementations = new HashMap<>();

        try {

            JarFile jarfile = new JarFile(new File(jarFilePath));
            Enumeration<JarEntry> entries = jarfile.entries();
            while(entries.hasMoreElements()) {
                JarEntry jarEntry = entries.nextElement();

                if (!jarEntry.getName().endsWith(".class")) {
                    continue;
                }
                final String className = jarEntry.getName().replace(".class", "")
                        .replaceAll("\\\\", ".")
                        .replaceAll("/", ".");
                int indexOfPackageRoot = className.indexOf(this.rootPackage);

                if (indexOfPackageRoot != -1) {
                    this.putClasses(Class.forName(className, true,new URLClassLoader(new URL[]{new File(jarFilePath).toURI().toURL()})), implementations, annotation);
                }
            }
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        return implementations;
    }

    public Map<Class<?>, Class<?>> findAllClassHavingAnnotationAndExpectedInterface(Class<? extends Annotation> annotation, Class<?> pluginInterface) {
        final Map<Class<?>, Class<?>> implementations = new HashMap<>();

        try {
            JarFile jarfile = new JarFile(new File(jarFilePath));
            Enumeration<JarEntry> entries = jarfile.entries();
            while(entries.hasMoreElements()) {
                JarEntry jarEntry = entries.nextElement();
                if (!jarEntry.getName().endsWith(".class")) {
                    continue;
                }
                final String className = jarEntry.getName().replace(".class", "")
                        .replaceAll("\\\\", ".")
                        .replaceAll("/", ".");
                int indexOfPackageRoot = className.indexOf(this.rootPackage);
                if (indexOfPackageRoot != -1) {
                    this.putClassesExtendingExpectedInterface(Class.forName(className, true,new URLClassLoader(new URL[]{new File(jarFilePath).toURI().toURL()})), implementations, annotation, pluginInterface);
                }
            }
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        return implementations;
    }

    private void putClasses(Class<?> curClass, Map<Class<?>, Class<?>> implementations, Class<? extends Annotation> annotation) {
        Optional<Class<?>> concernedInterface;
        if (!curClass.isInterface() && curClass.getInterfaces().length == 1) {
            implementations.put(curClass, null);
        } else if (!curClass.isAnnotationPresent(annotation)) {
            Class<?>[] interfaces = curClass.getInterfaces();
            concernedInterface = Arrays.stream(interfaces)
                    .filter(i -> i.isAnnotationPresent(annotation))
                    .findFirst();
            concernedInterface.ifPresent(aClass -> implementations.put(curClass, aClass));
        }
    }

    private void putClassesExtendingExpectedInterface(Class<?> curClass, Map<Class<?>, Class<?>> implementations, Class<? extends Annotation> annotation,Class<?> pluginInterface) {
        Optional<Class<?>> concernedInterface;
        if (!curClass.isInterface() && pluginInterface.isAssignableFrom(curClass)) {
            implementations.put(curClass, null);
        } else if (!curClass.isAnnotationPresent(annotation)) {
            Class<?>[] interfaces = curClass.getInterfaces();
            concernedInterface = Arrays.stream(interfaces)
                    .filter(i -> i.isAnnotationPresent(annotation))
                    .findFirst();
            concernedInterface.ifPresent(aClass -> implementations.put(curClass, aClass));
        }
    }
}
