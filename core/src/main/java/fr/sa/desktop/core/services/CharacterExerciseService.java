package fr.sa.desktop.core.services;

import fr.sa.desktop.core.models.Guild;
import fr.sa.desktop.core.plugin.models.CharacterWithStatistics;
import fr.sa.desktop.core.plugin.models.StatisticsExercise;
import fr.sa.desktop.core.utils.parsers.Parser;
import fr.sa.desktop.core.utils.requests.Request;

import java.net.http.HttpResponse;
import java.util.List;

public class CharacterExerciseService {
    private Parser parser;
    private Request request;

    public CharacterExerciseService(Parser parser, Request request) {
        this.parser = parser;
        this.request = request;
    }

    public List<StatisticsExercise> findStatisticsExerciseByGuild(Guild guild) {
        HttpResponse<String> response = this.request.get("characterExercise/guild/" + guild.getId());

        if(response != null && response.statusCode() == 200) {
            return this.parser.toObjectList(response.body(), StatisticsExercise.class);
        }
        return null;
    }

    public List<StatisticsExercise> findStatisticsExerciseByCharacter(Integer characterId) {
        HttpResponse<String> response = this.request.get("characterExercise/character/" + characterId);
        if(response != null && response.statusCode() == 200) {
            return this.parser.toObjectList(response.body(), StatisticsExercise.class);
        }
        return null;
    }

    public List<CharacterWithStatistics> findStatisticsByCharactersByGuildId(Integer id) {
        HttpResponse<String> response = this.request.get("characterExercise/guild/" + id + "/byCharacter");
        if(response != null && response.statusCode() == 200) {
            return this.parser.toObjectList(response.body(), CharacterWithStatistics.class);
        }
        return null;
    }
}
