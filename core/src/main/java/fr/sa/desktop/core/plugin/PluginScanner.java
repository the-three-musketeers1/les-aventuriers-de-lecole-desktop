package fr.sa.desktop.core.plugin;

import fr.sa.desktop.core.di.annotations.Injectable;
import fr.sa.desktop.core.di.scanners.ClassScannerForJarFile;
import fr.sa.desktop.core.utils.FileUtils;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.*;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PluginScanner {
    @SuppressWarnings("unchecked")
   public static <T> List<T> scanPlugin(Class<T> pluginInterface) {
        FileUtils fileUtils = new FileUtils();
        fileUtils.createPluginDirs();
       try (Stream<Path> walk = Files.walk(Paths.get("plugin/enable"))) {
           List<T> listPlugin = new ArrayList<>();
           List<String> result = walk.filter(Files::isRegularFile)
                   .map(x -> x.toString()).collect(Collectors.toList());

           result.forEach(res -> {
               ClassScannerForJarFile scann = new ClassScannerForJarFile(res);
               scann.setRootPackage("fr.sa.desktop.core.impl");
               Map<Class<?>, Class<?>> allClassHavingAnnotation = scann.findAllClassHavingAnnotationAndExpectedInterface(Injectable.class, pluginInterface);
               allClassHavingAnnotation.forEach((i, v) -> {
                   try {
                       Constructor<?> constructor = i.getConstructor();
                       constructor.setAccessible(true);
                       T instance = (T) constructor.newInstance();
                       listPlugin.add(instance);
                   } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
                       e.printStackTrace();
                   }
               });
           });
           return listPlugin;
       } catch (IOException e) {
           e.printStackTrace();
           throw new IllegalStateException("Error with plugin implementation");
       }
   }

}
