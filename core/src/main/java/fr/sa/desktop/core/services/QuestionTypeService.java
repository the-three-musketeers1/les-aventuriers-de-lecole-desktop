package fr.sa.desktop.core.services;

import fr.sa.desktop.core.models.QuestionType;
import fr.sa.desktop.core.utils.parsers.Parser;
import fr.sa.desktop.core.utils.requests.Request;

import java.net.http.HttpResponse;
import java.util.List;

public class QuestionTypeService {

    private Parser parser;
    private Request request;

    public QuestionTypeService(Parser parser, Request request) {
        this.parser = parser;
        this.request = request;
    }

    public List<QuestionType> findQuestionTypes() {
        HttpResponse<String> response = this.request.get("question-types");

        if(response != null && response.statusCode() == 200) {
            return this.parser.toObjectList(response.body(), QuestionType.class);
        }
        return null;
    }

    public QuestionType findOneQuestionType(Integer id) {
        HttpResponse<String> response = this.request.get("question-types/" + id);

        if(response != null && response.statusCode() == 200) {
            return this.parser.toObject(response.body(), QuestionType.class);
        }
        return null;
    }


}
