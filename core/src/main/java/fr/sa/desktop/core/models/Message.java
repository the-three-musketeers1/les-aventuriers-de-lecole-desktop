package fr.sa.desktop.core.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import java.util.Date;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString(of = {"id", "content", "sentByCharacter", "ticket"})
@JsonIgnoreProperties({"date", "updatedAt"})
public class Message {
    private Integer id;
    private String content;
    private Boolean sentByCharacter;
    private Ticket ticket;
    private Date sentDate;
    private Date updatedAt;
}
