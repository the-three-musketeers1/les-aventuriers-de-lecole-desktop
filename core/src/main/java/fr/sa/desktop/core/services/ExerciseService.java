package fr.sa.desktop.core.services;

import fr.sa.desktop.core.models.*;
import fr.sa.desktop.core.plugin.models.ExerciseInformation;
import fr.sa.desktop.core.utils.parsers.Parser;
import fr.sa.desktop.core.utils.requests.Request;

import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.List;

public class ExerciseService {

    private final Parser parser;
    private final Request request;

    public ExerciseService(Parser parser, Request request) {
        this.parser = parser;
        this.request = request;
    }

    public List<Exercise> findExercises() {
        HttpResponse<String> response = this.request.get("exercise");

        if(response != null && response.statusCode() == 200) {
            return this.parser.toObjectList(response.body(), Exercise.class);
        }

        return new ArrayList<Exercise>();
    }

    public Exercise findOneExercise(Integer id) {
        HttpResponse<String> response = this.request.get("exercise/" + id);

        return getExerciseIfStatusCodeIsCorrect(response, 200);
    }

    public List<Question> findExerciseQuestions(Integer id) {
        HttpResponse<String> response = this.request.get("exercise/" + id + "/only-questions");

        if(response != null && response.statusCode() == 200) {
            return this.parser.toObjectList(response.body(), Question.class);
        }

        return new ArrayList<Question>();
    }

    public boolean createExercise(Exercise exercise) {
        String exerciseJson = this.parser.toRaw(exercise);

        HttpResponse<String> response = this.request.post("exercise", exerciseJson);
        return response != null && response.statusCode() == 201;
    }

    public boolean updateExercise(Exercise exercise) {
        String exerciseJson = this.parser.toRaw(exercise);

        HttpResponse<String> response = this.request.put("exercise/" + exercise.getId(), exerciseJson);

        return response != null && response.statusCode() == 204;
    }

    public boolean deleteExercise(Integer id) {
        HttpResponse<String> response = this.request.delete("exercise/" + id);
        return response != null && response.statusCode() == 204;
    }

    private Exercise getExerciseIfStatusCodeIsCorrect(HttpResponse<String> response, Integer statusCode) {
        if(response != null && response.statusCode() == statusCode) {
            return this.parser.toObject(response.body(), Exercise.class);
        }
        return null;
    }

    public List<ExerciseInformation> findExercisesByGuild(Guild guild) {
        HttpResponse<String> response = this.request.get("exercise/guild/" + guild.getId());

        if(response != null && response.statusCode() == 200) {
            return this.parser.toObjectList(response.body(), ExerciseInformation.class);
        }

        return new ArrayList<ExerciseInformation>();
    }
}
