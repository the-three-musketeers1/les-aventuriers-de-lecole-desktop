package fr.sa.desktop.core.models;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString(of = {"token", "user"})
public class Session {
    private Integer id;
    private String token;
    private Leader user;
}
