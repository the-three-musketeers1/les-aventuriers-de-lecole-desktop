package fr.sa.desktop.core.models;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString(of = {"id", "state", "tickets"})
public class Status {
    private Integer id;
    private String state;
    private Ticket[] tickets;
}
