package fr.sa.desktop.core.di.scanners;

import java.io.File;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.net.URL;
import java.util.*;

public class ClassScannerForDirectory implements ClassScanner{
    private String rootPackage;

    public ClassScannerForDirectory() {
    }

    public void setRootPackage(String rootPackage) {
        this.rootPackage = rootPackage;
    }

    /**
     * Find all class that have annotation, depend to the root package
     * @param annotation {Class} : annotation to check for all concerned classes
     * @return implementations {Map} : map that have for each occurence the annotated class in key and its implemented interface or null in value
     */
    public Map<Class<?>, Class<?>> findAllClassHavingAnnotation(Class<? extends Annotation> annotation) {
        Map<Class<?>, Class<?>> implementations = new HashMap<>();
        try {
            ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
            Enumeration<URL> urls = classLoader.getResources("");
            while (urls.hasMoreElements()) {
                URL next = urls.nextElement();
                File curDir = new File(next.getFile());
                for (Class<?> curClass : this.findClassesInPackage(curDir)) {
                    this.putClasses(curClass, implementations, annotation);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return implementations;
    }

    private void putClasses(Class<?> curClass, Map<Class<?>, Class<?>> returned, Class<? extends Annotation> annotation) {
        Class<?>[] interfaces;
        Optional<Class<?>> concernedInterface;
        if (curClass.isAnnotationPresent(annotation) && !curClass.isInterface()) {
            returned.put(curClass, null);
        } else if (!curClass.isAnnotationPresent(annotation)) {
            interfaces = curClass.getInterfaces();
            concernedInterface = Arrays.stream(interfaces)
                    .filter(i -> i.isAnnotationPresent(annotation))
                    .findFirst();
            concernedInterface.ifPresent(aClass -> returned.put(curClass, aClass));
        }
    }

    private List<Class<?>> findClassesInPackage(File curDir) {
        List<Class<?>> classes = new LinkedList<>();

        if (curDir.exists()) {
            File[] content = curDir.listFiles();
            assert content != null;
            for (File curFile : content) {
                if (curFile.isDirectory()) {
                    classes.addAll(this.findClassesInPackage(curFile));
                } else {
                    try {
                        String path = curFile.getAbsolutePath();
                        String asPackageFormat = path.replaceAll("\\\\", ".");
                        int indexOfPackageRoot = asPackageFormat.indexOf(this.rootPackage);
                        if (indexOfPackageRoot != -1) {
                            String fullClassName = asPackageFormat.substring(indexOfPackageRoot, asPackageFormat.length() - 6);
                            classes.add(Class.forName(fullClassName));
                        }
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return classes;
    }
}

