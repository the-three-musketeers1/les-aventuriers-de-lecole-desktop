package fr.sa.desktop.core.services;

import fr.sa.desktop.core.models.Adventure;
import fr.sa.desktop.core.models.Exercise;
import fr.sa.desktop.core.models.Leader;
import fr.sa.desktop.core.models.Subject;
import fr.sa.desktop.core.utils.parsers.Parser;
import fr.sa.desktop.core.utils.requests.Request;

import java.net.http.HttpResponse;
import java.util.*;
import java.util.stream.Collectors;

public class LeaderService {
    
    private final Parser parser;
    private final Request request;
    private final Integer currentLeaderId;

    public LeaderService(Parser parser, Request request) {
        this.parser = parser;
        this.request = request;

        var result = this.parser.toObject(this.request.getToken(), HashMap.class);
        this.currentLeaderId = (Integer) result.get("userId");
    }

    public List<Leader> findLeaders() {
        HttpResponse<String> response = this.request.get("users/leaders");

        if(response != null && response.statusCode() == 200) {
            return this.parser.toObjectList(response.body(), Leader.class);
        }

        return null;
    }

    public Leader findOneLeader(final Integer id) {
        HttpResponse<String> response = this.request.get("users/leaders/one/" + id);
        return this.getLeaderIfStatusCodeIsCorrect(response, 200);
    }

    public List<Exercise> findLeaderExercises(final Integer id) {
        HttpResponse<String> response = this.request.get("users/leaders/" + id + "/exercises");

        if(response != null && response.statusCode() == 200) {
            return this.parser.toObjectList(response.body(), Exercise.class);
        }

        return new ArrayList<Exercise>();
    }

    public List<Exercise> findLeaderExercisesBySubject(final Integer leaderId, final Integer subjectId) {
        HttpResponse<String> response = this.request.get("users/leaders/" + leaderId + "/subject/" + subjectId + "/exercises");

        if(response != null && response.statusCode() == 200) {
            return this.parser.toObjectList(response.body(), Exercise.class);
        }

        return new ArrayList<Exercise>();
    }

    public List<Exercise> findLeaderExercisesByAdventure(final Integer leaderId, final Integer adventureId) {
        HttpResponse<String> response = this.request.get("users/leaders/" + leaderId + "/adventure/" + adventureId + "/exercises");

        if(response != null && response.statusCode() == 200) {
            return this.parser.toObjectList(response.body(), Exercise.class);
        }

        return new ArrayList<Exercise>();
    }

    public List<Exercise> findLeaderExercisesByExpedition(final Integer leaderId, final Integer expeditionId) {
        HttpResponse<String> response = this.request.get("users/leaders/" + leaderId + "/expedition/" + expeditionId + "/exercises");

        if(response != null && response.statusCode() == 200) {
            return this.parser.toObjectList(response.body(), Exercise.class);
        }

        return new ArrayList<Exercise>();
    }

    public List<Adventure> findLeaderAdventures(final Integer id) {
        HttpResponse<String> response = this.request.get("users/leaders/" + id + "/adventures");

        if(response != null && response.statusCode() == 200) {
            return this.parser.toObjectList(response.body(), Adventure.class);
        }

        return new ArrayList<Adventure>();
    }

    public List<Adventure> findLeaderExpeditions(final Integer id) {
        HttpResponse<String> response = this.request.get("users/leaders/" + id + "/expeditions");

        if(response != null && response.statusCode() == 200) {
            return this.parser.toObjectList(response.body(), Adventure.class);
        }

        return new ArrayList<Adventure>();
    }

    public Leader createLeader(final Leader leader) {
        String leaderJson;
        try {
            leaderJson = this.parser.toRaw(leader);
        } catch (IllegalArgumentException e) {
            return null;
        }


        HttpResponse<String> response = this.request.post("users", leaderJson);
        return getLeaderIfStatusCodeIsCorrect(response, 201);
    }

    public boolean updateLeader(final Leader leader) {
        String leaderJson = this.parser.toRaw(leader);

        HttpResponse<String> response = this.request.put("users" + leader.getId(), leaderJson);

        return response != null && response.statusCode() == 204;
    }

    public boolean deleteLeader(final Integer id) {
        HttpResponse<String> response = this.request.delete("users/" + id);
        return response != null && response.statusCode() == 204;
    }

    private Leader getLeaderIfStatusCodeIsCorrect(final HttpResponse<String> response,final Integer statusCode) {
        System.out.println(response.statusCode());
        System.out.println(response.body());
        if(response != null && response.statusCode() == statusCode) {
            return this.parser.toObject(response.body(), Leader.class);
        }
        return null;
    }

    public List<Subject> findLeaderSubjects(Integer id) {
        HttpResponse<String> response = this.request.get("users/leaders/" + id + "/subjects");

        if(response != null && response.statusCode() == 200) {
            return this.parser.toObjectList(response.body(), Subject.class);
        }

        return new ArrayList<>();
    }

    public List<Exercise> findLeaderExercisesWithFilters(Integer id, Subject subjectFilter, Adventure expeditionFilter) {
        List<Exercise> leaderExercises = findLeaderExercises(id);
        if(subjectFilter != null) {
            List<Subject> childSubjects = Arrays.asList(subjectFilter.getChildSubjects());
            Set<Integer> childSubjectIds = childSubjects.stream().map(Subject::getId).collect(Collectors.toSet());
            leaderExercises = leaderExercises.stream()
                    .filter(exercise -> childSubjectIds.contains(exercise.getSubject().getId()))
                    .collect(Collectors.toList());
        }

        if(expeditionFilter != null) {
            leaderExercises.retainAll(findLeaderExercisesByExpedition(id, expeditionFilter.getId()));
        }
        return leaderExercises;
    }


    public Integer getCurrentLeaderId() {
        return currentLeaderId;
    }
}
