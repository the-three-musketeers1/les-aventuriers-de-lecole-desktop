package fr.sa.desktop.core.di.scanners;

import java.lang.annotation.Annotation;
import java.util.Map;

public interface ClassScanner {
    public void setRootPackage(String rootPackage);

    public Map<Class<?>, Class<?>> findAllClassHavingAnnotation(Class<? extends Annotation> annotation);
}
