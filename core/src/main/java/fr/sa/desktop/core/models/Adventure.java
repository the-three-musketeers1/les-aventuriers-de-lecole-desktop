package fr.sa.desktop.core.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import java.time.LocalDate;
import java.util.Date;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString(of = {"id", "title", "life", "mana", "experienceBonus", "leader"})
@JsonIgnoreProperties({ "createdAt", "updatedAt"})
public class Adventure {

    private Integer id;
    private String title;
    private String description;
    private Integer life;
    private Integer mana;
    private Boolean isExpedition;
    private Integer experienceBonus;

    //@JsonFormat(pattern = "dd/MM/YYYY")
    //private LocalDate deadline;

    private Leader leader;
}
