package fr.sa.desktop.core.services;

import fr.sa.desktop.core.models.Leader;
import fr.sa.desktop.core.models.Message;
import fr.sa.desktop.core.models.Status;
import fr.sa.desktop.core.models.Ticket;
import fr.sa.desktop.core.utils.parsers.Parser;
import fr.sa.desktop.core.utils.requests.Request;

import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.List;

public class TicketService {
    private Parser parser;
    private Request request;

    public TicketService(Parser parser, Request request) {
        this.parser = parser;
        this.request = request;
    }

    public List<Ticket> findTickets() {
        HttpResponse<String> response = this.request.get("ticket/leader");

        if (response != null && response.statusCode() == 200) {
            return this.parser.toObjectList(response.body(), Ticket.class);
        }
        return null;
    }

    public boolean updateStatus(Ticket selectedTicket, Status status) {
        String jsonToSend = String.format("{\"status\":%s}", this.parser.toRaw(status));
        String url = String.format("ticket/%d/status", selectedTicket.getId());
        HttpResponse<String> response = this.request.put(url, jsonToSend);
        return response != null && response.statusCode() == 204;
    }

    public Ticket findTicketById(Ticket selectedTicket) {
        String url = String.format("ticket/%d/leader", selectedTicket.getId());
        HttpResponse<String> response = this.request.get(url);

        if (response != null && response.statusCode() == 200) {
            return this.parser.toObject(response.body(), Ticket.class);
        }
        return null;
    }

    public Message createMessage(Message message, Ticket ticket) {
        String messageJson = parser.toRaw(message);
        String url = String.format("ticket/%d/leader/message", ticket.getId());
        HttpResponse<String> response = this.request.post(url, messageJson);
        if (response != null && response.statusCode() == 201) {
            return parser.toObject(response.body(), Message.class);
        }
        return null;
    }
}
