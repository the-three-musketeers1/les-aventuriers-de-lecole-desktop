package fr.sa.desktop.core.utils.parsers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.NonNull;

import javax.inject.Inject;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class JsonParser implements Parser {

    private final ObjectMapper mapper;

    public JsonParser(final ObjectMapper mapper) {
        this.mapper = mapper;
        mapper.registerModule(new JavaTimeModule());
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
    }


    @Override
    public <T> List<T> toObjectList(final String raw, final Class<T> type) {
        try {
            return this.mapper.readValue(raw, this.mapper.getTypeFactory().constructCollectionType(ArrayList.class, type));
        } catch (IOException e) {
            e.printStackTrace();
            return new ArrayList<T>();
        }
    }

    @Override
    public <T> T toObject(final String raw, final Class<T> type) {
        try {
            return this.mapper.readValue(raw, this.mapper.getTypeFactory().constructType(type));
        } catch (IOException | NullPointerException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public <T> String toRaw(@NonNull final T objects) {
        try {
            return this.mapper.writeValueAsString(objects);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return "";
        }
    }
}
