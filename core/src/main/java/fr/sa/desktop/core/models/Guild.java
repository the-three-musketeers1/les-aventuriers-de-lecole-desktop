package fr.sa.desktop.core.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import java.util.Date;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString(of = {"id", "name", "leader", "deadlineAddCharacter", "token"})
@JsonIgnoreProperties({"createdDate", "updatedDate"})
public class Guild {
    private Integer id;
    private String name;
    private CharacterUser[] characters;
    private String token;
    private Date deadlineAddCharacter;
    private Date createdDate;
    private Date updatedDate;
    private Leader leader;
}
