package fr.sa.desktop.core;
import fr.sa.desktop.core.di.annotations.Injectable;
import fr.sa.desktop.core.models.*;
import fr.sa.desktop.core.services.*;

@Injectable
public class SchoolAdventurers {
    public void run() {
        System.out.println("Bienvenue aux aventuriers de l'école !");
    }
}
