package fr.sa.desktop.core.di.container;

import fr.sa.desktop.core.di.annotations.Injectable;
import fr.sa.desktop.core.di.scanners.ClassScanner;
import fr.sa.desktop.core.di.scanners.ClassScannerForDirectory;
import fr.sa.desktop.core.di.scanners.ClassScannerForJarFile;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.util.*;

public class DIContainer {
    private static DIContainer diContainer = new DIContainer();
    private ClassScanner classScanner;
    private Map<Class<?>, Class<?>> implementations;
    private Map<Class<?>, Object> registry;
    private Map<Class<?>, Class<?>[]> classInterfaceParam;

    /**
     * Get the DIContainer singleton
     */
    public static DIContainer getInstance() {
        return diContainer;
    }

    /**
     * Clear all DIContainer data
     */
    public void clearDiContainer() {
        this.implementations = new HashMap<>();
        this.registry = new Hashtable<>();
        this.classInterfaceParam = new Hashtable<>();
    }

    private DIContainer() {
        this.classScanner = this.resolveScanner();
        this.implementations = new HashMap<>();
        this.registry = new Hashtable<>();
        this.classInterfaceParam = new Hashtable<>();
    }

    private ClassScanner resolveScanner() {
        String filePath = this.getClass().getProtectionDomain().getCodeSource().getLocation().getFile();
        File file = new File(filePath);
        if (!file.isDirectory() && filePath.endsWith(".jar")) {
            return new ClassScannerForJarFile(filePath.replaceAll("%20", ""));
        }

        return new ClassScannerForDirectory();
    }

    /**
     * Scan all classes that is injectable depend to package path
     * @param packagePath {String} : package path to search injectable classes
     */
    public void implementInjectableClassByPackage(String packagePath) {
        this.classScanner.setRootPackage(packagePath);

        if (this.implementations.size() == 0) {
            this.implementations = this.classScanner.findAllClassHavingAnnotation(Injectable.class);
            return;
        }
        this.implementations.putAll(this.classScanner.findAllClassHavingAnnotation(Injectable.class));
    }


    /**
     * Get instance recursivly
     * @param appClass {Class}: class to instance
     * @return {T} : instance of class
     */
    @SuppressWarnings("unchecked")
    public <T> T getClassInstance(Class<T> appClass) {
        T instance = null;

        if(this.implementations.containsKey(appClass)) {
            try {
                if (this.registry.containsKey(appClass)) {
                    return (T) this.registry.get(appClass);
                }
                instance = this.createInstanceOfClass(appClass);

                this.registry.put(appClass, instance);
            } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
                e.printStackTrace();
            }
        }

        return instance;
    }


    @SuppressWarnings("unchecked")
    private <T> T createInstanceOfClass(Class<T> appClass) throws IllegalAccessException, InvocationTargetException, InstantiationException, NoSuchMethodException {
        T instance;

        List<Object> parameters = new ArrayList<>();
        var constructors = appClass.getConstructors();
        if (constructors.length > 1) {
            throw new IllegalStateException(DIContainer.class + " : Multiple constructors not managed");
        }

        for (Class<?> parameterClass: constructors[0].getParameterTypes()) {
            var parameter = this.getParameter(appClass, parameterClass);
            parameters.add(parameter);
        }
        if (parameters.size() > 0) {
            instance = (T) constructors[0].newInstance(parameters.toArray());
        } else {
            instance = appClass.getDeclaredConstructor().newInstance();
        }
        return instance;
    }

    private Object getParameter(Class<?> appClass, Class<?> parameterClass) {
        if (parameterClass.isInterface()) {
            if (!this.classInterfaceParam.containsKey(appClass)) {
                throw new IllegalStateException(DIContainer.class + " : Should indicate parent class and concernd class implements " + parameterClass.getName());
            }
            var checkClass = this.classInterfaceParam.get(appClass)[1];
            if (!this.implementations.containsKey(checkClass)) {
                throw new IllegalStateException(DIContainer.class + " : interface not injectable");
            }
            return this.getClassInstance(checkClass);
        }

        return this.getClassInstance(parameterClass);
    }

    /**
     * Fill map for DI Container to know the implemented parameter real type to load
     * @param aClass {Class} : class that is concerned
     * @param aParamInterface {Class} : parameter interface type
     * @param aParamClass {Class} : parameter concrete type
     */
    public void indicateImplementedParamClass(Class<?> aClass, Class<?> aParamInterface, Class<?> aParamClass) {
        this.classInterfaceParam.put(aClass, new Class<?>[]{aParamInterface, aParamClass});
    }
}
