package fr.sa.desktop.core.plugin.models;

import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString(of = {"id", "title", "description"})
public class Discipline {

    private Integer id;
    private String title;
    private String description;
    private List<Chapter> childSubjects;
}
