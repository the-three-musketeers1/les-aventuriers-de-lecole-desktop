package fr.sa.desktop.core.utils.requests;

import io.github.cdimascio.dotenv.Dotenv;

import java.net.http.HttpClient;
import java.net.http.HttpResponse;

public interface Request {
    HttpResponse<String> delete(final String url);
    HttpResponse<String> get(final String url);
    HttpResponse<String> post(final String url, final String raw);
    HttpResponse<String> put(final String url, final String raw);
    void setToken(final String token);
    void setTokenBySessionFile();
    String getToken();
}
