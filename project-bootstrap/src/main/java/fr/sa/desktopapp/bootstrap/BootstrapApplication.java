package fr.sa.desktopapp.bootstrap;

import org.update4j.Configuration;
import org.update4j.FileMetadata;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static java.lang.String.format;

public class BootstrapApplication {
    public static void main(String[] args) throws IOException {
        final var applicationVersion = args[0];
        final var ciJobId = System.getenv("CI_JOB_ID");


        final var configuration = Configuration.builder()
                .baseUri(format("https://gitlab.com/the-three-musketeers1/les-aventuriers-de-lecole-desktop/-/jobs/%s/artifacts/raw/", ciJobId))
                .basePath("${user.home}/.les-aventuriers-de-lecole-desktop/")
                .property("default.launcher.main.class", "fr.sa.desktopapp.ui.Main")
                .file(FileMetadata
                        .readFrom(format("build/target/School_Adventurers.jar", applicationVersion))
                        .path("desktop-ui.jar")
                        .classpath()
                )
                .build();

        Files.writeString(Path.of("project-bootstrap/target/desktopapp.xml"), configuration.toString());
    }
}
