package fr.sa.desktopapp.cli;

import fr.sa.desktop.core.SchoolAdventurers;
import fr.sa.desktop.core.di.annotations.Injectable;
import fr.sa.desktop.core.di.container.DIContainer;
import fr.sa.desktop.core.di.scanners.ClassScannerForJarFile;
import fr.sa.desktop.core.plugin.GuildPluginTemplate;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Application {
    public static void main(String[] args) {
        DIContainer container = DIContainer.getInstance();

        container.implementInjectableClassByPackage("fr.sa.desktop.core");
        var schoolAdventurers = container.getClassInstance(SchoolAdventurers.class);
        try (Stream<Path> walk = Files.walk(Paths.get("plugin/enable"))) {

            List<String> result = walk.filter(Files::isRegularFile)
                    .map(x -> x.toString()).collect(Collectors.toList());

            result.forEach(res -> {
                ClassScannerForJarFile scann = new ClassScannerForJarFile(res);
                scann.setRootPackage("fr.sa.desktop.core.impl");
                Map<Class<?>, Class<?>> allClassHavingAnnotation = scann.findAllClassHavingAnnotation(Injectable.class);
                allClassHavingAnnotation.forEach((i, v) -> {
                    try {
                        Constructor<?> constructor = i.getConstructor();
                        constructor.setAccessible(true);
                        GuildPluginTemplate test = (GuildPluginTemplate) constructor.newInstance();
                    } catch (InstantiationException e) {
                        e.printStackTrace();
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    } catch (InvocationTargetException e) {
                        e.printStackTrace();
                    } catch (NoSuchMethodException e) {
                        e.printStackTrace();
                    }
                });
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
